/*
 *  sbb_nonSelective.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in sbb_nonSelective.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Jan/2015
 *
 */

#ifndef sbb_nonSelective_h
#define sbb_nonSelective_h

/*
 * @global functions
 */

#include "mplx_rfpulse.h"


/*rf excitation sequence building block */
typedef struct sbb_nonSelective{
        mplx_pulse* prfpulse;
        WF_PULSE rf;
        WF_PULSE rf_theta;

        /*WF_PULSE gzrfa;
        WF_PULSE gzrf;
        WF_PULSE gzrfd;*/

        WF_PULSE gzcrushla;
        WF_PULSE gzcrushl;
        WF_PULSE gzcrushld;

        WF_PULSE gzcrushra;
        WF_PULSE gzcrushr;
        WF_PULSE gzcrushrd;


        WF_PULSE gzcrushlXa;
        WF_PULSE gzcrushlX;
        WF_PULSE gzcrushlXd;

        WF_PULSE gzcrushrXa;
        WF_PULSE gzcrushrX;
        WF_PULSE gzcrushrXd;

        WF_PULSE gzcrushlYa;
        WF_PULSE gzcrushlY;
        WF_PULSE gzcrushlYd;

        WF_PULSE gzcrushrYa;
        WF_PULSE gzcrushrY;
        WF_PULSE gzcrushrYd;


        /*control variables */
        int ia_rfExt;
        float a_rfExt;
        int ia_rfExtTheta;
        float a_rfExtTheta;
        /*float a_gzrf;
        int ia_gzrf;*/
        float a_gcrush;
        int ia_gcrush;
        float slcThickness;
        int axis; /* 0=X;1=Y;2=Z*/

        /*SAR */
        float alpha; /*flip angle in deg */
        int RF_SLOT; /*slot in rfpulse[] structure*/
        int exciter; /*which exciter RHO1 or RHO2*/
        int Ninstances; /*how often do we employ that pulse per entry point */
        float B1scaling[MAX_MPLX_SLICES];
        float stretchFactor; /*multiple nominal pulse width by this factor */

        /*timing*/
        int start_time; /*all other timing is relative to the sbb start time */
        int total_time; /*duration of sbb */
        int time_from_RFcenter; /*sbb duration measured from peak of rf pulse */
        int psd_rf_wait;
        int psd_grd_wait;
        int pw_rfExt;


        int pw_gcrushla;
        int pw_gcrushl;
        int pw_gcrushld;
        int pw_gcrushra;
        int pw_gcrushr;
        int pw_gcrushrd;
} sbb_nonSelective;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_nonSelective_cveval(sbb_nonSelective* psbb);
STATUS sbb_nonSelective_predownload(void);
STATUS sbb_nonSelective_link_rfpulse(sbb_nonSelective* psbb,int slot);

/*
 * @pg functions
 */
STATUS sbb_nonSelective_pulsegen(sbb_nonSelective* psbb,CHAR *sbb_name);


/*************************/
/* @rsp functions */
/*************************/
void sbb_nonSelective_psdinit(sbb_nonSelective* psbb);
void sbb_nonSelective_setfrequency(sbb_nonSelective* psbb,long freq);


#endif
