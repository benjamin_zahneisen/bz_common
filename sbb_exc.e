/*
 *  exc_sbb.e
 *
 *  This file contains all the external functions
 *  defined in exc_sbb.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include "mplx_rfpulse.h"
#include "sbb_exc.h"




/**********************************************/
@host _ipgexport
/* this is a possible set of ipgexport variables */
mplx_pulse mplxRFPulse;
sbb_exc excSBB;
#ifdef POMP
mplx_pulse mplxRFPulse_pomp[MAX_MPLX_SLICES];
#endif


/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
#include "sbb_exc.h"

void sbb_exc_init(sbb_exc* p){
    p->prfpulse = NULL;
    int i;
    for(i=0;i<MAX_MPLX_SLICES;i++){
    p->prfpulse_pomp[i] = NULL;}
    initWF_PULSE(&(p->rf));
    initWF_PULSE(&p->rf_theta);
    initWF_PULSE(&p->gzrfa);
    initWF_PULSE(&p->gzrf);
    initWF_PULSE(&p->gzrfd);
    initWF_PULSE(&p->gzrfdepa);
    initWF_PULSE(&p->gzrfdep);
    initWF_PULSE(&p->gzrfdepd);
    initWF_PULSE(&p->gzrfrepa);
    initWF_PULSE(&p->gzrfrep);
    initWF_PULSE(&p->gzrfrepd);

    /*control variables */
    p->ia_rfExt        = MAX_PG_IAMP;
    p->ia_rfExtTheta   = MAX_PG_IAMP;
    p->ia_gzrf        = MAX_PG_IAMP;
    p->a_gzrf         = 0;
    p->slcThickness    = 0;
    p->playSlcReph     = 1;
    
    /*pomp and single band waves*/
    p->Nwaves		= 1;

    /*SAR */
    p->alpha              = 90.0;
    p->RF_SLOT            = 0;
    p->exciter            = TYPRHO1; /*which exciter RHO1 or RHO2*/

    /*timing*/
    p->start_time           = 0;
    p->total_time           = 0;
    p->time_from_RFcenter   = 0;
    p->time_to_RFcenter     = 0;
    p->psd_rf_wait          = 0; /*(long) get_rf_dly();*/
    p->psd_grd_wait         = 0; /*(long) get_grad_dly(); 52 seems reasonable*/
    p->pw_rfExt             = 0;
    p->pw_gzrf              = 0;
    p->pw_gzrfa             = 0;
    p->pw_gzrfd             = 0;

    p->instance             = 0;
    p->debugstate           = 0;
};

/*helpfer function to initialize WF_PULSE*/
#ifndef initWF_PULSE_h
#define initWF_PULSE_h
void initWF_PULSE(WF_PULSE* pwf){
    pwf->pulsename = "<uninitialized>";
    pwf->ninsts = 0;
    pwf->wave_addr = -1;
    pwf->board_type = PSDCERD;
    pwf->reusep =  PSDREUSEP;
    pwf->tag =  SSPUNKN;
    pwf->addtag =  0;
    pwf->id =  0;
    pwf->ctrlfield =  0;
    pwf->inst_hdr_tail =  NULL;
    pwf->inst_hdr_head =  NULL;
    pwf->wavegen_type =TYPXGRAD;
    pwf->type =  TYPBITS;
    pwf->resolution = 0;
    pwf->assoc_pulse = NULL;
    pwf->ext =  NULL;
};
#endif

STATUS sbb_exc_cveval(sbb_exc* psbb,mplx_pulse* prf)
{

    /*pass on and store rf-pulse*/
    psbb->prfpulse = prf;

    /*store slice thickness from rf-pulse*/
    psbb->slcThickness = psbb->prfpulse->slcThickness;

    /*get slice selection amplitude from rf-pulse*/
    psbb->a_gzrf = psbb->prfpulse->slcSelectAmp;

    /*set the pulse width*/
    psbb->pw_rfExt = psbb->prfpulse->pw;

    /*flat part equals pulse width */
    psbb->pw_gzrf = psbb->pw_rfExt;

    /*get optimal ramps*/
    if (optramp(&psbb->pw_gzrfa, psbb->a_gzrf, loggrd.tz, loggrd.zrt, TYPDEF) == FAILURE){
        fprintf(stderr, "optramp attack in exc_sbb_cveval() failed.\n");
        return FAILURE;
    }

    /*get optimal ramps*/
    if (optramp(&psbb->pw_gzrfd, psbb->a_gzrf, loggrd.tz, loggrd.zrt, TYPDEF) == FAILURE){
        fprintf(stderr, "optramp decay in exc_sbb_cveval() failed.\n");
        return FAILURE;
    }

    /*calculate area of slice select gradient */ /*Verify which loggrd we need !!!!!!!!!!!*/
    psbb->ia_gzrf = psbb->a_gzrf * MAX_PG_IAMP / loggrd.tz;
    float arearf = psbb->a_gzrf * ((float)psbb->pw_rfExt + (float)psbb->pw_gzrfa);


    /* slice select dephaser & rephaser instruction amplitudes.
     amppwgrad() calculates fastest trapezoid for desired area. */
    float target;
    int rtime,ftime;
    gettarget(&target, ZGRAD, &loggrd);
    getramptime(&rtime, &ftime, ZGRAD, &loggrd);
    /* rephaser */
    if( amppwgrad(-0.5 * arearf, target, 0.0, 0.0, rtime, MIN_PLATEAU_TIME, &psbb->a_grfrep, &psbb->pw_grfrepa, &psbb->pw_grfrep,
                  &psbb->pw_grfrepd)
        == FAILURE)
        return FAILURE;
    psbb->ia_grfrep = (psbb->a_grfrep / target) * MAX_PG_IAMP;


    /*set timing of sbb */
    psbb->total_time =  psbb->pw_gzrfa
                        + psbb->pw_gzrf
                        + psbb->pw_gzrfd;

    if(psbb->playSlcReph)
        psbb->total_time += (psbb->pw_grfrepa + psbb->pw_grfrep + psbb->pw_grfrepd);

    psbb->time_from_RFcenter =  psbb->pw_gzrf/2
                                + psbb->pw_gzrfd;

    if(psbb->playSlcReph)
        psbb->time_from_RFcenter += (psbb->pw_grfrepa + psbb->pw_grfrep + psbb->pw_grfrepd) ;

    psbb->time_to_RFcenter = psbb->pw_grfrepa + psbb->pw_gzrf/2;

    if(psbb->debugstate > 1){
        fprintf(stderr, "exc_sbb_cveval() results: \n");
        fprintf(stderr, "a_gzrf1: %f [G/cm] \n",psbb->a_gzrf);
        fprintf(stderr, "pw_gzrf1a: %i [micros] \n",psbb->pw_gzrfa);
        fprintf(stderr, "pw_gzrf1a: %i [micros] \n",psbb->pw_gzrf);
        fprintf(stderr, "pw_gzrf1d: %i [micros] \n",psbb->pw_gzrfd);
    }

  return SUCCESS;
}

STATUS exc_sbb_predownload(void){


    return SUCCESS;
};

STATUS sbb_exc_link_rfpulse(sbb_exc* psbb,int slot){

    /*do we rely on rfpulse as a global variable ?? */

    /*set rf-slot */
    psbb->RF_SLOT = slot;

    rfpulse[slot].pw =(int *)  &(psbb->pw_rfExt);   /* int *pw;  pulse width */
    rfpulse[slot].amp = (float *) &(psbb->a_rfExt);  /* float *amp;      amplitude */
    rfpulse[slot].act_fa = (float *) &(psbb->alpha);  /*float *act_fa;   desired flip angle */
    rfpulse[slot].res = (int *) &(psbb->prfpulse->resolution); /* pulse resolution vmx 3/13/95 YI changed from short to int */
    rfpulse[slot].exciter = (int *) &(psbb->exciter); /* waveform generator (e.g., TYPRHO1 or TYPRHO2) */

    /*SAR relevant parameters from rf-pulse */
    rfpulse[slot].abswidth      = psbb->prfpulse->SAR.abs_width; /* calculated RF absolute width */
    rfpulse[slot].effwidth      = psbb->prfpulse->SAR.eff_width; /* calculated RF effective width */
    rfpulse[slot].area          = psbb->prfpulse->SAR.area;     /* zero momemt of pulse */
    rfpulse[slot].dtycyc        = psbb->prfpulse->SAR.dty_cycle;   /* max % of pulse width above 23% and in any 1  lobe */
    rfpulse[slot].maxpw         = psbb->prfpulse->SAR.max_pw;    /* max % of time rf pulse is above 0 amp */
    rfpulse[slot].num           = 1;        /* quantity of this type of RF pulse */
    rfpulse[slot].max_b1        = psbb->prfpulse->SAR.max_b1;   /* peak B1 in G under standard conditions */
    rfpulse[slot].max_int_b1_sq = psbb->prfpulse->SAR.max_integral_b1_sqr;   /* peak integral B1^2 in GG-msec under standard conditions */
    rfpulse[slot].max_rms_b1    = psbb->prfpulse->SAR.max_rms_b1;      /* peak RMS B1 in G under standard conditions */
    rfpulse[slot].nom_fa        = psbb->prfpulse->SAR.nom_fa;   /* flip angle on resonance under standard conditions. */

    rfpulse[slot].nom_pw = (psbb->prfpulse->dwell)*(psbb->prfpulse->resolution);   /* pulse duration under standard conditions */
    rfpulse[slot].nom_bw =psbb->prfpulse->bw;   /* pulse bandwidth under standard conditions */

    rfpulse[slot].activity  = PSD_MPS2_ON+PSD_APS2_ON+PSD_SCAN_ON; /* bitmask showing when pulse is used  PSD_PULSE_OFF*/
    rfpulse[slot].reference = (char ) 0; /* flag for pulse used in TG setting */
    rfpulse[slot].isodelay  = 0; /* time in usec from the zero phase reference of pulse to its end*/
    rfpulse[slot].scale     = 0.0;    /* duty cycle scale factor */
    rfpulse[slot].extgradfile = TRUE; /* TRUE = rfpulse uses external gradient waveform file */


    return SUCCESS;
};


/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg



STATUS sbb_exc_pulsegen(sbb_exc* psbb,CHAR *sbb_name,mplx_pulse* prf,int start_time)
{

    /*store pointers to RF array*/
    psbb->prfpulse =  &(prf[0]);
	/*for(k=0;k<Nwaves;k++){
        psbb->prfpulse_pomp[k] =  &(prf[k]);
    }*/

    /*store start time */
    psbb->start_time = start_time;

    if(psbb->prfpulse == NULL){
        fprintf(stderr, "sbb_exc_pulsegen() error: No rfpulse specified! \n");
        return FAILURE;
     }

    /*start with slice selection gradients first */
    int ia_start = 0;
    int ia_end = 0;
    int posRep;
    char name[128];

    sprintf(name,"%s_%s",sbb_name,"gz");
    trapezoid(ZGRAD,name, &psbb->gzrf, &psbb->gzrfa, &psbb->gzrfd,
              psbb->pw_gzrf, psbb->pw_gzrfa, psbb->pw_gzrfd, psbb->ia_gzrf,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    posRep = psbb->pw_gzrfa +psbb->pw_gzrf +psbb->pw_gzrfd;
    sprintf(name,"%s_%s",sbb_name,"gzreph");
    if(psbb->playSlcReph){
        trapezoid(ZGRAD,name, &psbb->gzrfrep, &psbb->gzrfrepa, &psbb->gzrfrepd,
              psbb->pw_grfrep, psbb->pw_grfrepa, psbb->pw_grfrepd, psbb->ia_grfrep,
              ia_start, ia_end,0,0 ,posRep + psbb->start_time, TRAP_ALL, &loggrd);
    }


    sprintf(name,"%s_%s",sbb_name,"rfmag");
    pulsename(&psbb->rf,name);
    sprintf(name,"%s_%s",sbb_name,"rftheta");
    pulsename(&psbb->rf_theta,name);

    /*reserves space in instruction memory */
    createreserve(&psbb->rf,RHO,psbb->prfpulse->resolution);
    createreserve(&psbb->rf_theta,THETA,psbb->prfpulse->resolution);

    /*convert waveforms in psbb from int to short*/
    short* wave_mag;
    short* wave_phase;
    wave_mag = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    wave_phase = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    int t;
    for(t=0;t<psbb->prfpulse->resolution;t++){
        wave_mag[t] = (short) psbb->prfpulse->iwave_mag[t];
        wave_phase[t] = (short) psbb->prfpulse->iwave_phase[t];
    }

    /*assign local wave to hardware */
    movewaveimm(wave_mag,&psbb->rf,0,psbb->prfpulse->resolution,TOHARDWARE);
    movewaveimm(wave_phase,&psbb->rf_theta,0,psbb->prfpulse->resolution,TOHARDWARE);

    /*create instruction */
    psbb->ia_rfExt = MAX_PG_IAMP*psbb->prfpulse->a_RF; /*get instruction amplitude after B1 scaling*/
    psbb->ia_rfExtTheta = MAX_PG_IAMP;
    long start_rf;
    start_rf =  psbb->start_time
                + psbb->pw_gzrfa;

    createinstr( &psbb->rf,start_rf,psbb->pw_rfExt,psbb->ia_rfExt);
    createinstr( &psbb->rf_theta,start_rf,psbb->pw_rfExt,psbb->ia_rfExtTheta);

    /*this ensures that the last point of the waveform is odd, I guess */
    setweos(EOS_DEAD,&psbb->rf,psbb->prfpulse->resolution-1);
    setweos(EOS_DEAD,&psbb->rf_theta,psbb->prfpulse->resolution-1);


    addrfbits(&psbb->rf,0,start_rf, psbb->pw_rfExt);


    return SUCCESS;
};

#ifdef POMP
STATUS sbb_exc_pulsegen_pomp(sbb_exc* psbb){

    if(psbb->debugstate)
    fprintf(stderr, "sbb_exc_pulsegen_pomp() entered. \n" );

    /* allocate memory for pomp waveforms */
    psbb->pomp_wf = (WF_PULSE *) AllocNode(psbb->Nwaves*sizeof(WF_PULSE));
    psbb->pomp_wf_theta = (WF_PULSE *) AllocNode(psbb->Nwaves*sizeof(WF_PULSE));
    psbb->wave_ptr = (WF_HW_WAVEFORM_PTR *) AllocNode(psbb->Nwaves*sizeof(WF_HW_WAVEFORM_PTR));
    psbb->wave_theta_ptr = (WF_HW_WAVEFORM_PTR *) AllocNode(psbb->Nwaves*sizeof(WF_HW_WAVEFORM_PTR));
    char pnstr[80];
    short* wave_mag;
    short* wave_phase;
    float maxB1Pomp = 0.0;
    int k,t;

    /*convert waveforms in psbb from int to short*/
     wave_mag = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
     wave_phase = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
 
    for (k = 0; k < psbb->Nwaves; k++){

        for(t=0;t<psbb->prfpulse->resolution;t++){
	    /*fprintf(stderr, "wave = %i \n" ,psbb->prfpulse_pomp[k]->iwave_mag[t]);*/
            wave_mag[t] = (short) psbb->prfpulse_pomp[k]->iwave_mag[t];
            wave_phase[t] = (short) psbb->prfpulse_pomp[k]->iwave_phase[t];

        }

        /*magnitude */
        sprintf(pnstr,"thrf1sl%d",k);
        pulsename(&psbb->pomp_wf[k], pnstr);
        createreserve(&psbb->pomp_wf[k],TYPRHO1,psbb->prfpulse->resolution);
        movewaveimm(wave_mag,&psbb->pomp_wf[k],0,psbb->prfpulse->resolution,TOHARDWARE);
        psbb->wave_ptr[k] = psbb->pomp_wf[k].wave_addr;

        /*phase */
        sprintf(pnstr,"thrf2sl%d",k);
        pulsename(&psbb->pomp_wf_theta[k], pnstr);
        createreserve(&psbb->pomp_wf_theta[k],THETA,psbb->prfpulse->resolution);
        movewaveimm(wave_phase,&psbb->pomp_wf_theta[k],0,psbb->prfpulse->resolution,TOHARDWARE);
        psbb->wave_theta_ptr[k] = psbb->pomp_wf_theta[k].wave_addr;

        /*for each wave we have to keep track of small changes in actual max B1 value */
        psbb->B1scaling[k] = psbb->prfpulse_pomp[k]->maxB1;
        if(psbb->B1scaling[k] > maxB1Pomp)
            maxB1Pomp = psbb->B1scaling[k];
    }
  
  
    for (k = 0; k < psbb->Nwaves; k++){
        psbb->B1scaling[k] /= maxB1Pomp;
        /*if(psbb->debugstate)*/
        /*fprintf(stderr,"B1scaling 90 = %f \n",psbb->B1scaling[k]);*/
    }

    if(psbb->debugstate)
    fprintf(stderr, "sbb_exc_pulsegen_pomp() done. \n" );

    FreeNode(wave_mag);
    FreeNode(wave_phase);

    return SUCCESS;
};
#endif

/* end @pg rfov_funcs.e **********************/


/***********************************/
@rsp _rsp
void sbb_exc_psdinit(sbb_exc* psbb){

    /*set frequency to zero */
    setfrequency(0.0, &psbb->rf, 0);

    /*set amplitudes */
    setiamp(psbb->ia_rfExt, &psbb->rf, 0);
    setiamp(MAX_PG_IAMP, &psbb->rf_theta, 0);

    /*slice selection gradient*/
    setiamp(psbb->ia_gzrf, &psbb->gzrf,0);
    if(psbb->playSlcReph)
    setiamp(psbb->ia_grfrep, &psbb->gzrfrep,0);

};

void sbb_exc_setfrequency(sbb_exc* psbb,long freq){

    setfrequency(freq, &psbb->rf, 0);

};

void sbb_exc_setphase(sbb_exc* psbb,double phase){
    /*phase value in radians  (e.g. pi/2)*/

    setphase(phase, &psbb->rf, 0);
};

#ifdef POMP
/* Sets different waveforms  */
void sbb_exc_setwave(sbb_exc* psbb,int num)
{

    if((num > (MAX_MPLX_SLICES-1)) || (num>=psbb->Nwaves )){
        fprintf(stderr,"No wave available for n=%i.\n",num);
        return;
    }

    if(num < 0){
        /*set wave to default single wave pointer*/
        return;
    }

    /*magnitude */
    int ia;
    setwave(psbb->wave_ptr[num],&(psbb->rf),INSTRALL);
    setweos(EOS_DEAD,&(psbb->rf),psbb->prfpulse->resolution-1);
    ia = psbb->ia_rfExt * (psbb->B1scaling[num]);
    setiamp(ia, &(psbb->rf), 0); /*where do we get the correct ampliutde ?*/

    /*phase */
    setwave(psbb->wave_theta_ptr[num],&(psbb->rf_theta),INSTRALL);
    setweos(EOS_DEAD,&(psbb->rf_theta),psbb->prfpulse->resolution-1);
    setiamp(MAX_PG_IAMP, &(psbb->rf_theta), 0); /*where do we get the correct ampliutde ?*/

}
#endif
/* end @rsp rfov_funcs.e ********************/
