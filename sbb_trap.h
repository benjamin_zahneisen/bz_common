/*
 *  sbb_trap.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in sbb_trap.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Jan/2015
 *
 */
#ifndef sbb_trap_h
#define sbb_trap_h

#define SBBtrap_DEBUG
/*
 * @global functions
 */

/*trapezoidal gradient */
typedef struct sbb_trap{

        /*control variables */
        int ia_flat;
        int ia_attack;
        int ia_decay;
        float a_flat;
        float a_attack;
        float a_decay;
        int momentum; /*gradient moment [G/cm s]*/

        /*SAR */
        int GRAD_SLOT; /*slot in rfpulse[] structure*/
        int axis; /*GRADX, GRADY, GRADZ */
        int type; /*TRAP_ALL, TRAP_ALL_SLOPED*/
        int Ninstances;

        /*timing*/
        int start_time; /*all other timing is relative to the sbb start time */
        int total_time; /*duration of sbb */
        int psd_grd_wait;
        int pw_attack;
        int pw_decay;
        int pw_flat;

        WF_PULSE wf_attack;
        WF_PULSE wf_decay;
        WF_PULSE wf_flat;

} sbb_trap;


/*
 * @host functions
 */
STATUS sbb_trap_cveval(sbb_trap* psbb,float momentum);
STATUS sbb_trap_predownload(void);
STATUS sbb_trap_init(sbb_trap* psbb);

/*
 * @pg functions
 */
STATUS sbb_trap_pulsegen(sbb_trap* psbb,CHAR *sbb_name);


/*************************/
/* @rsp functions */
/*************************/
STATUS sbb_trap_psdinit(sbb_trap* psbb);
STATUS sbb_trap_setAmplitude(sbb_trap* psbb,int amplitude,long index);


#endif /*exc_sbb_h */
