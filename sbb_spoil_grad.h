/*
 *  sbb_diff_grad.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in sbb_diff_grad.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sep/2016
 *
 */

#ifndef sbb_spoil_grad_h
#define sbb_spoil_grad_h

#define sbb_spoil_grad_DEBUG
/*
 * @global functions
 */



/*spoiler sequence building block */
typedef struct sbb_spoil_grad{


    WF_PULSE gxa;
    WF_PULSE gx;
    WF_PULSE gxd;

    WF_PULSE gya;
    WF_PULSE gy;
    WF_PULSE gyd;

    WF_PULSE gza;
    WF_PULSE gz;
    WF_PULSE gzd;
    
    /*blank wf_pulses on non-gradient axis*/
    /*WF_PULSE blank_rho;
    WF_PULSE blank_theta;
    WF_PULSE blank_omega;
    WF_PULSE blank_ssp;*/
      
    /*control variables */
    float a_gx;
    float a_gy;
    float a_gz;
    int ia_gx;
    int ia_gy;
    int ia_gz;
    int axis; /* 0=X;1=Y;2=Z*/
    float gmax; /*max gradient amplitude*/

    /*timing*/
    int start_time; /*all other timing is relative to the sbb start time */
    int total_time; /*duration of sbb */
    int psd_rf_wait;
    int psd_grd_wait;
    int pw_gxa;
    int pw_gx;
    int pw_gxd;
    int pw_gya;
    int pw_gy;
    int pw_gyd;
    int pw_gza;
    int pw_gz;
    int pw_gzd;
    
    /*misc*/
    int Ninstances;
    int use_maxloggrad; 
    int debug;

} sbb_spoil_grad;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_spoil_grad_cveval(sbb_spoil_grad* psbb,float revolutions);
STATUS sbb_spoil_grad_predownload(void);

/*
 * @pg functions
 */
STATUS sbb_spoil_grad_pulsegen(sbb_spoil_grad* psbb,CHAR *sbb_name,int start_time);


/*************************/
/* @rsp functions */
/*************************/
void sbb_spoil_grad_psdinit(sbb_spoil_grad* psbb);

#endif /*sbb_dw_dpfg_h */
