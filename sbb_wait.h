/*
 *  sbb_wait.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in exc_sbb.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Jan/2015
 *
 */

#ifndef sbb_wait_h
#define sbb_wait_h

/*
 * @global functions
 */


/*wait pulse on all boards */
typedef struct sbb_wait{

        WF_PULSE rho;
        WF_PULSE theta;
        WF_PULSE omega;
        WF_PULSE ssp;
        WF_PULSE gx;
        WF_PULSE gy;
        WF_PULSE gz;



        /*timing*/
        int start_time; /*all other timing is relative to the sbb start time */
        int total_time; /*duration of sbb */
        int max_duration;


        int instance; /*add number to base name*/
        int debugstate;

} sbb_wait;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_wait_cveval(sbb_wait* psbb);
STATUS sbb_wait_predownload(void);


/*
 * @pg functions
 */
STATUS sbb_wait_pulsegen(sbb_wait* psbb,CHAR *sbb_name,int start_time,int duration);


/*************************/
/* @rsp functions */
/*************************/
void sbb_wait_psdinit(sbb_wait* psbb);
void sbb_wait_setperiod(sbb_wait* psbb,long period);


#endif 
