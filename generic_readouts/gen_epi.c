/*
 * gen_epi.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */

int gen_epi(generic_ro * w)
{
    int i,j,k;
    float gamma = 2.0*M_PI*4.257e3;
    float dkx; /* [rad/mm] as seen by the ADC */
    float dkx_grad; /* [rad/mm] with 4microsec GRAD_UPDATE_TIME */
    float dky;
    float gampx;
    float kxmax; /* rad / mm */
    float kymax;
    float tr,tr1,tr2; /* transition times between flat regions */
    float * ramp;
    float * ramp2;
    int ramp_res;
    float * blip_y;
    float * blip_z;
    float * blip_z_rew;
    int blip_res;
    int blip_y_res;
    int blip_z_res;
    int blip_z_rew_res;
    int blip_z_flag; /* flag for z-gradient blips 0=no blips */
    int numPlanes;
    int blipCounter;
    int flat_res;
    int nlines1,nlines2; /* number of total lines (i.e. trapezoids) in EPI train per interleave*/
    /* nlines1 is without any fractional echo, nlines2 is with fractional echo*/
    int ctr;
    float area_ramp, area_ramp2;
    float zblipMoment;
    float zRewinderBlipMoment;
    float a,b,c;
    float T_blip_up_down[12];
    int nominalIlvs; /*store nominal number of interleaves before adding "virtual" blip down interleaves */
    int downIlv; /*which interleave do we select for the blip down scan */ 
    float zBlipIlv; /*extra z-blip moment for blip down shift along z (=different CAIPI blips */

    /* blip up & down */
    if (w->epi_blip_up_down==1){
        identity(T_blip_up_down,0);
        T_blip_up_down[5]=-1.0;
    }

    /* x direction */
    dkx=MIN(2.0*M_PI/w->fovx*0.1,w->gmax*gamma*w->dts_adc*0.1);
    gampx=dkx/gamma/w->dts_adc*10.0;
    dkx_grad = gampx*gamma*w->dts*0.1;      
    kxmax=2.0*M_PI*(float)w->nx/w->fovx*0.1/2.0; /* rad / mm */
    
    fprintf(stderr,"gampx = %f \n",gampx);
    fprintf(stderr,"w->gslew = %f \n",w->gslew);
    fprintf(stderr,"dkx_grad = %f \n",dkx_grad);

    flat_res=(int)(2.0*kxmax/dkx_grad+0.5);
    if (flat_res%2) flat_res++;

    fprintf(stderr,"flat_res = %i \n",flat_res);
    
    /* y direction */
    dky=2.0*M_PI/w->fovy/10.0*w->ni;
    nlines1=(int)((float)w->ny/(float)w->ni+0.5);
    /* number of lines per interleaf has to be even */
    nlines1=2*(int)((float)nlines1/2.0+0.5);
    /* going for higher resolution than initially asked for */
    w->ny=nlines1*w->ni;
    kymax=dky*(float)w->ny/2.0/w->ni;
    /* adding the effect of fractional echo */
    nlines2=(int)((float)w->ny/(float)w->ni*w->fn+0.5);
    /* has to be even*/
    nlines2=2*(int)((float)nlines2/2.0+0.5);


    /* blips and ramps */
    /* ramp from 0 to gampx*/
    ramp=(float*)malloc(MAXPTSPERINT*sizeof(float));
    /* ramp from -gampx to gampx */
    ramp2=(float*)malloc(MAXPTSPERINT*sizeof(float));
    blip_y =(float*)malloc(MAXPTSPERINT*sizeof(float));
    blip_z =(float*)malloc(MAXPTSPERINT*sizeof(float));
    blip_z_rew =(float*)malloc(MAXPTSPERINT*sizeof(float));
    /* transition time from 0 to gampx */
    gen_ramp(0, gampx, 0.0, w->gslew, w->dts,ramp, &ramp_res,NULL);
    tr1=ramp_res*w->dts*2; /* 2 is because the ramp goes -gampx to gampx */
    /* phase encoding blip duration */
    gen_trap(dky,w->gmax,w->gslew,w->dts,blip_y,&blip_y_res);

    
    /*BZ check if we need caipi blips */
    if((w->epi_multiband_factor == (int)w->epi_multiband_R) || w->epi_multiband_fov < 0.0 || w->epi_multiband_factor == 1)
        blip_z_flag = 0;
    else
        blip_z_flag = 1;
		

    /*blip moment as dk */
    if(blip_z_flag){

        /*that's the multiband k-space increment*/
        zblipMoment = 2.0*M_PI/(w->epi_multiband_fov);
        
        /*how many kz-planes(=positive blips-1)*/
        numPlanes = (int)w->epi_multiband_factor/w->epi_multiband_R;
        
        /*rewinder blip moment*/
        zRewinderBlipMoment = (numPlanes-1)*(zblipMoment*w->epi_multiband_R);
        
        /*calculate blip and rewinder gradient pulse*/
        gen_trap(zblipMoment*w->epi_multiband_R,w->gmax,w->gslew,w->dts,blip_z,&blip_z_res);
        gen_trap(zRewinderBlipMoment,w->gmax,w->gslew,w->dts,blip_z_rew,&blip_z_rew_res);
        
        blip_res=MAX(blip_z_rew_res,blip_z_res);
        blip_res=MAX(blip_res,blip_y_res);       
        
    }
    else{
        blip_res=blip_y_res;
    }

    tr2=blip_res*w->dts;
    /* comparing them and getting the blip and ramp duration */
    /* for now, we don't know whether the ramp or the blip is longer*/
    tr=MAX(tr1,tr2);


    /* this is the part where the effect of ramp sampling is put
     the usable area of the ramp is the part where it doesn't
     coincide with the blip. First, let's see if the resolution
     of the ramp is larger than the resolution of the blip. Then,
     we calculate the area of the ramp that doesn't coincide
     with the blip. Then we subtract this area from the area
     spanned by the flat portion of the readout. It is also
     possible that the area of the ramp is larger. In this
     case, we need to reduce gampx and make flat_res==1
     */

    /* no ramp sampling is possible if the blip is longer than the ramp */
    if (blip_res/2 >= ramp_res )
        w->epi_rampsamp=0;


    if (w->epi_rampsamp==1)
    {
        area_ramp=calc_g_area(ramp,ramp_res,w->dts,w->tempmem);
        /* this is the area that concides with the blip, unusable area */
        area_ramp2=calc_g_area(ramp,blip_res/2,w->dts,w->tempmem);

        /* the difference is the area that is usable for phase encoding */
        area_ramp=area_ramp-area_ramp2;

        /* if this area is already larger than the kxmax required,
     we lower gampx and also ramp time. This means that the
     whole data acquisition takes place on the ramp, except
     for the part of the ramp that concides with the blip

     Math:
     G/cm * G/cm * m*ms/mT * rad*Hz/G * 0.1 mT/G * 0.001 sec/ms * 100 cm/m * 0.1 cm/mm
     area_ramp=gampx*gampx/gslew*gamma*0.01/2.0+flat_res*dts*gamma*0.1*gampx=kxmax+area_ramp2  (rad/mm)
     gampx=sqrt(kxmax*gslew/gamma/0.01*2.0/(2.0*M_PI))
         */
        if (area_ramp > kxmax)
        {
            flat_res=2;
            a=gamma*0.001/2.0/w->gslew;
            b=flat_res*w->dts*0.1/2.0;
            c=-(kxmax+area_ramp2);
            gampx=(-b+sqrt(b*b-4*a*c))/(2*a);


            /* what if this is shorter than blip_res???? */
        }

        /* else, we go up to gampx, but the resolution of the flat
     region is shorter
         */
        else
        {
            flat_res=(int)(2.0*(kxmax-area_ramp)/dkx_grad+0.5);
            /*fprintf(stderr,"gen_epi >> flat_res=%d\n",flat_res);fflush(stderr);*/
            /* making even */
            if (flat_res%2) flat_res++;
        }

        /* re-create the ramp */
        gen_ramp(0, gampx, 0.0, w->gslew, w->dts,ramp, &ramp_res,NULL);
        tr1=ramp_res*w->dts*2; /* 2 is because the ramp goes -gampx to gampx */
        /* comparing them and getting the blip and ramp duration
         * for now, we don't know whether the ramp or the blip
         * is longer
         */
        tr=MAX(tr1,tr2);
    }


    /* recreating the ramp by stretching it if necessary*/
    if (tr != tr1)
        gen_ramp(0, gampx, tr/2, w->gslew, w->dts,ramp, &ramp_res,NULL);


    /* recreating the y blip by centering it*/
    if ((int)(tr/w->dts+0.5)>blip_y_res)
    {
        ctr=(int)(((int)(tr/w->dts+0.5)-blip_y_res)/2.0+0.5);
        for (i=(int)(tr/w->dts+0.5)-1;i>=(int)(tr/w->dts+0.5)-ctr;i--)
            blip_y[i]=0.0;
        for (i=(int)(tr/w->dts+0.5)-ctr-1;i>=ctr;i--)
            blip_y[i]=blip_y[i-ctr];
        for (i=ctr-1;i>=0;i--)
            blip_y[i]=0.0;
    }
     /* recreating the z blip by centering it*/
    if (blip_z_flag)
    {
        if ((int)(tr/w->dts+0.5)>blip_z_res)
        {
            ctr=(int)(((int)(tr/w->dts+0.5)-blip_z_res)/2.0+0.5);
            for (i=(int)(tr/w->dts+0.5)-1;i>=(int)(tr/w->dts+0.5)-ctr;i--)
                blip_z[i]=0.0;
            for (i=(int)(tr/w->dts+0.5)-ctr-1;i>=ctr;i--)
                blip_z[i]=blip_z[i-ctr];
            for (i=ctr-1;i>=0;i--)
                blip_z[i]=0.0;
        }
        
        /*rewinder blip */
        if ((int)(tr/w->dts+0.5)>blip_z_rew_res)
        {
            ctr=(int)(((int)(tr/w->dts+0.5)-blip_z_rew_res)/2.0+0.5);
            for (i=(int)(tr/w->dts+0.5)-1;i>=(int)(tr/w->dts+0.5)-ctr;i--)
                blip_z_rew[i]=0.0;
            for (i=(int)(tr/w->dts+0.5)-ctr-1;i>=ctr;i--)
                blip_z_rew[i]=blip_z_rew[i-ctr];
            for (i=ctr-1;i>=0;i--)
                blip_z_rew[i]=0.0;
        }
    }

    /* creating a ramp from -gampx to gampx */
    for (i=0;i<ramp_res;i++)
    {
        ramp2[i]=ramp[i]-gampx;
        ramp2[i+ramp_res]=ramp[i];
    }

    w->gx_res=(flat_res+2*ramp_res)*nlines2;
    w->gy_res=w->gx_res;
    if (blip_z_flag)
        w->gz_res=w->gx_res;
    else
        w->gz_res=0;
    w->gx_start=0;
    w->gy_start=0;
    w->gz_start=0;
    w->gx_end=w->gx_res-1;
    w->gy_end=w->gy_res-1;
    w->gz_end=w->gx_end;
    w->gx_ctr= (flat_res+2*ramp_res)*(nlines2-nlines1/2);
    w->gy_ctr=w->gx_ctr;
    w->gz_ctr=w->gx_ctr;

    /* error check for maximum frame size*/
    if (w->gx_res >= MAXPTSPERINT)
    {
        w->gx_res=0;
        w->gy_res=0;
        w->gz_res=0;
        w->nseg=0;
        w->frsize=0;
        w->gx_ctr=0;
        w->gy_ctr=0;
        w->gz_ctr=0;
        w->gx_start=0;
        w->gy_start=0;
        w->gz_start=0;
        w->gx_end=0;
        w->gy_end=0;
        w->gz_end=0;
        w->epi_blip_res=0;
        w->epi_flat_res=0;
        w->epi_ramp_res=0;
        w->i_ctr=0;
        return 0;
    }


    /* start with half ramp*/
    for (k=0;k<ramp_res;k++)
    {
        w->gx[k]=ramp[k];
        w->gy[k]=0.0;
        if (blip_z_flag)
            w->gz[k]=0.0;
    }
    
    blipCounter = 1;
    for (j=0;j<nlines2/2;j++)
    {
        /* flat region*/
        for (k=0;k<flat_res;k++)
        {
            w->gx[2*j*(flat_res+2*ramp_res)+k+ramp_res]=gampx;
            w->gy[2*j*(flat_res+2*ramp_res)+k+ramp_res]=0.0;
            if (blip_z_flag)
                w->gz[2*j*(flat_res+2*ramp_res)+k+ramp_res]=0.0;
        }
        /* blip and ramp down region */
        for (k=0;k<2*ramp_res;k++)
        {
            w->gx[2*j*(flat_res+2*ramp_res)+flat_res+k+ramp_res]=-ramp2[k];
            w->gy[2*j*(flat_res+2*ramp_res)+flat_res+k+ramp_res]=blip_y[k];
            if (blip_z_flag){
                if(blipCounter%numPlanes == 0)
                    w->gz[2*j*(flat_res+2*ramp_res)+flat_res+k+ramp_res]=-blip_z_rew[k];
                else
                   w->gz[2*j*(flat_res+2*ramp_res)+flat_res+k+ramp_res]=blip_z[k]; 
            }
        }
        blipCounter++;
        /* flat region*/
        for (k=0;k<flat_res;k++)
        {
            w->gx[2*j*(flat_res+2*ramp_res)+flat_res+3*ramp_res+k]=-gampx;
            w->gy[2*j*(flat_res+2*ramp_res)+flat_res+3*ramp_res+k]=0.0;
            if (blip_z_flag)
                w->gz[2*j*(flat_res+2*ramp_res)+flat_res+3*ramp_res+k]=0.0;
        }
        /* blip and ramp up region only if this is not the last line */
        if (j!=(nlines2/2-1))
        {
            for (k=0;k<2*ramp_res;k++)
            {
                w->gx[2*j*(flat_res+2*ramp_res)+2*flat_res+3*ramp_res+k]=ramp2[k];
                w->gy[2*j*(flat_res+2*ramp_res)+2*flat_res+3*ramp_res+k]=blip_y[k];
                if (blip_z_flag){
                    if(blipCounter%numPlanes == 0){
                        w->gz[2*j*(flat_res+2*ramp_res)+2*flat_res+3*ramp_res+k]=-blip_z_rew[k];
                    }
                    else{
                        w->gz[2*j*(flat_res+2*ramp_res)+2*flat_res+3*ramp_res+k]=blip_z[k];
                    }
                }
            }
            blipCounter++;
        }
    }

    /* end with half ramp */
    for (k=0;k<ramp_res;k++)
    {
        w->gx[w->gx_res-ramp_res+k]=ramp[k]-gampx;
        w->gy[w->gx_res-ramp_res+k]=0.0;
        if (blip_z_flag)
            w->gz[w->gx_res-ramp_res+k]=0.0;

    }

    if (w->epi_rampsamp ==0)
    {
        area_ramp=calc_g_area(ramp,ramp_res,w->dts,w->tempmem);
        gen_k_from_g(w->gx, w->gx_res, w->dts, -kxmax-area_ramp, w->kx);
    }
    else if (w->epi_rampsamp ==1)
    {
        /*!!!!! */
        area_ramp2=calc_g_area(ramp,blip_res/2,w->dts,w->tempmem);
        gen_k_from_g(w->gx, w->gx_res, w->dts, -kxmax-area_ramp2, w->kx);
    }

    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;


    w->epi_blip_res=blip_res;
    w->epi_flat_res=flat_res;
    w->epi_ramp_res=ramp_res;

    /* frame size*/
    w->frsize=(w->gx_end-w->gx_start+1);

    /* be careful with ky : in fractional echo, the echo time must be shorter */
    gen_k_from_g(w->gy, w->gy_res, w->dts, -kymax+dky*(0.5/(float)(w->ni)+nlines1-nlines2), w->ky);
    if (blip_z_flag)
    {
        /* again, carefully check the multiband dephasing */
        gen_k_from_g(w->gz, w->gz_res, w->dts,-zRewinderBlipMoment/2.0 , w->kz);

    }

    identity(w->T,0);

    /* shift along y */
    i=w->ni;
    w->ni=1;
    shift(w,i,dky/(float)i,0.0,1);


    nominalIlvs = w->ni;
    /* blip up down for distortion correction realized as proper interleaves */
    if (w->epi_blip_up_down==1)
    {
        rep_w(w,2);
        for (i=w->ni/2;i<w->ni;i++)
        {
            multiply_w(w,T_blip_up_down,i);
        }
    }
    
    /*shift CAIPI blips for blip down interleave*/
    if((w->epi_blip_up_down==1) && (blip_z_flag)){
        
        switch(w->epi_multiband_factor){
            
            case 2:
                downIlv = w->ni-1;
                zBlipIlv = 0.0;
                break;
            
            case 4:
                downIlv = w->ni-1;
                
                if(w->epi_multiband_R == 2)
                zBlipIlv = (zblipMoment*w->epi_multiband_R)/2.0;
                
                /*no in-plane acceleration -> shift last interleave by dky*/
                if(nominalIlvs == 1){
                    shift_ilv(w,downIlv,dky,1);                
                }
                
                break;
            
            case 6:
                downIlv = w->ni-1;
                 /*corresponds to 3 kz-planes*/   
                 if(w->epi_multiband_R == 2){
                     zBlipIlv = (zblipMoment*w->epi_multiband_R)/2.0;
                    if(nominalIlvs == 1){
                    shift_ilv(w,downIlv,dky,1);              
                    }
                 }
                 /*corresponds to 2 kz-planes*/
                 if(w->epi_multiband_R == 3){
                     zBlipIlv = (zblipMoment*w->epi_multiband_R)/2.0;
                     if(nominalIlvs == 1){
                        shift_ilv(w,downIlv,dky,1);                
                    }
                 }                

                 
                break;
            
            default:
                downIlv = w->ni-1;
                zBlipIlv = 0.0;
                
                break;
        
        }
        
        shift_ilv(w,downIlv,zBlipIlv,2);/*shift CAIPI-blips by half a plane (or not) */
    }


    free(ramp);
    free(ramp2);
    free(blip_y);
    free(blip_z);

    return 1;

};


int gen_stack_of_epi(generic_ro * w)
{

    float dkz;

    w->ni=w->ni/w->nz;

    /* generating the EPI part */
    gen_epi(w);


    /* shifting along z */
    dkz=2.0*M_PI/w->fovz/10.0;
    shift(w,w->nz,dkz,-(int)((float)w->nz/2.0),2);

    w->i_ctr=w->ni/2; /*!!!! actually, this must be a function of pevious i_ctr since there can be pf along the blade dim
     * take care of this later
     */
    return 1;

}
