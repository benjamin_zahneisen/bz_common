int gen_spiral(generic_ro * w)
{
    float gamma = 2.0*M_PI*4.257e3;
    float dt;
    float S0;
    int i, j, n;
    float Ts, T, ts, t, theta, dthdt, thetas, x, y;
    float a1, a2, beta, Gmax, c, s;
    float gabs, gmax;
    float q;


    /* getting a copy of all variables necessary */
    float D = w->fovx;
    int N = w->nx;
    float dts=w->dts;
    int nl=w->ni;
    float gamp=w->gmax;
    float gslew = w->gslew;
    float Tmax = w->Tmax;



    q = 5;
    S0 = gslew*100;
    dt = dts*0.5;

    /*  slew-rate limited approximation  */

    Ts = 0.666667/(float)nl*sqrt(pow((double)(M_PI*N), (double)3.0)/(gamma*D*S0));
    if(Ts > Tmax) {
        printf("slew limited readout too long\n");
        return -1;
    }
    a2 = N*M_PI/(nl*pow(Ts, 0.666667));
    a1 = 1.5*S0/a2;
    beta = S0*gamma*D/nl;
    Gmax = a1*pow(Ts, 0.333333);
    gmax = 0;
    i = 0;
    for (t = 0; t<=Ts; t+=dt) {
        x = pow(t, 1.333333);
        theta = .5*beta*t*t/(q + .5*beta/a2*x);
        y = q+.5*beta/a2*x;
        dthdt = beta*t*(q+0.166667*beta/a2*x)/(y*y);
        c = cos(theta);
        s = sin(theta);
        w->gx[i] = nl/(D*gamma)*dthdt*(c - theta*s);
        w->gy[i] = nl/(D*gamma)*dthdt*(s + theta*c);
        gabs = hypot(w->gx[i], w->gy[i]);
        if(gabs>=gamp)  {
            if(gmax==0)  gmax = hypot(gamp/theta, gamp);
            if(gabs>gmax) break;
        }
        ts = t;
        thetas = theta;
        i++;
    }

    /*gmax limited approximation  */

    if(Gmax > gamp)  {
        T = ((M_PI*N/(float)nl)*(M_PI*N/(float)nl) - thetas*thetas)/(2*gamma*gamp*D/(float)nl) + ts;
        if(T > Tmax)  {
            printf("gmax limited readout too long\n");
            return -1;
        }
        for (t=ts+dt; t<=T; t+=dt)  {
            theta = sqrt(thetas*thetas + 2*gamma*gamp*D*(t-ts)/nl);
            c = cos(theta);
            s = sin(theta);
            w->gx[i] = gamp*(c/theta - s);
            w->gy[i++] = gamp*(s/theta + c);
        }
    }


    /* making sure that the resolution is even */
    if ((int)((float)i/2.0+0.5) % 2)
    {
        w->gx[0]=0.0;
        w->gy[0]=0.0;
        n=1;
    }
    else
    {
        n=0;
    }


    /*decimate by 2 to get back to 4us sampling */
    for (j=0; j<i; j+=2) {
        w->gx[n] = LIMIT(w->gx[j], -gamp, gamp);
        w->gy[n++] = LIMIT(w->gy[j], -gamp, gamp);
    }



    w->gx_ctr=0;
    w->gy_ctr=0;
    w->gx_res=n;
    w->gy_res=n;
    w->gz_res=0;
    w->gx_start=0;
    w->gy_start=0;
    w->gx_end=n-1;
    w->gy_end=n-1;



    /* going from gradients to k-space, assuming spiral starts at 0 */
    gen_k_from_g(w->gx,w->gx_res,w->dts,0.0,w->kx);
    gen_k_from_g(w->gy,w->gy_res,w->dts,0.0,w->ky);

    /* constructing all interleaves */
    for (i=0;i<nl;i++) {
        theta = i*2.0*M_PI/(float)nl;
        c = cos(theta);
        s = sin(theta);
        w->T[12*i+0] = c;
        w->T[12*i+1] = -s;
        w->T[12*i+2] = 0.0;
        w->T[12*i+3] = 0.0;
        w->T[12*i+4] = s;
        w->T[12*i+5] = c;
        w->T[12*i+6] = 0.0;
        w->T[12*i+7] = 0.0;
        w->T[12*i+8] = 0.0;
        w->T[12*i+9] = 0.0;
        w->T[12*i+10]= 1.0;
        w->T[12*i+11]= 0.0;
    }

    /* number of segments with different transformation matrices*/
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;
    /* number of segments with different WF_pulses */
    /*   w->n_psd_seg_gx=1; */
    /*   w->psd_segs_gx[0]=0; */
    /*   w->psd_segs_gx[1]=w->gx_res; */
    /*   w->n_psd_seg_gy=1; */
    /*   w->psd_segs_gy[0]=0; */
    /*   w->psd_segs_gy[1]=w->gy_res; */
    /*   w->n_psd_seg_gz=0; */
    /* number of acqq segments */
    /* rhfrsize */
    w->frsize=w->gx_end-w->gx_start+1;

    return 1;
};


int gen_spiral_radial(generic_ro * w)
{

    generic_ro * wsp=(generic_ro*)malloc(sizeof(generic_ro));
    float c1,s1,c2,s2,zbeta,zalpha,czbeta,szbeta,czalpha,szalpha,phi,sphi, cphi, zeta,czeta, szeta;
    int i,j,k;
    double xx,yy;

    /* generating the Spiral part */
    copy_w(w,wsp);
    wsp->ni=w->ni/w->spiral_radial_nspokes;


    if(w->vdspiral_alpha==1){
        /*  fprintf(stderr,"\t alpha=1 -> gen_spiral...\n");fflush(stderr);*/
        gen_spiral(wsp);
    }else{
        /* fprintf(stderr,"\t alpha!=1 -> gen_vd_spiral...\n");fflush(stderr);*/
        gen_vd_spiral(wsp);
    }

    fill_gz_with_zeros(wsp);


    add_ramp_prewinder_to_g(wsp);

    /* add the rewinder if needed */
    if(w->rewind_readout){
        add_rewinder_to_g(wsp);
    }
    /* put some zeros */
    add_zeros_before_g(wsp, 6);

    /* generating the pepi pulse */
    w->gx_res=wsp->gx_res;
    w->gy_res=wsp->gx_res;
    w->gz_res=wsp->gx_res;
    w->gx_start=wsp->gx_start;
    w->gx_end=wsp->gx_end;
    w->gy_start=wsp->gy_start;
    w->gy_end=wsp->gy_end;
    w->gz_start=wsp->gx_start;
    w->gz_end=wsp->gx_end;
    w->gx_ctr=wsp->gx_ctr;
    w->gy_ctr=wsp->gy_ctr;
    w->gz_ctr=wsp->gx_ctr;





    for (j=0;j<w->gx_res*wsp->ni;j++)
    {
        w->gx[j]= wsp->gx[j];
        w->gy[j]= wsp->gy[j];
        w->gz[j]= 0.0;
    }
    for (j=0;j<(w->gx_res+1)*wsp->ni;j++)
    {
        w->kx[j]= wsp->kx[j];
        w->ky[j]= wsp->ky[j];
        w->kz[j]= 0.0;
    }

    /* fprintf(stderr,"\t generating the interleaves...\n");fflush(stderr);
     */




    /* for (i=0;i<w->spiral_radial_nspokes;i++)
    {
      for (j=0;j<wsp->ni;j++)
    {*/

    /* rafael reversed the order of these loops for SPI */
    k=0;
    for (j=0;j<wsp->ni;j++)
    {
        for (i=0;i<w->spiral_radial_nspokes;i++)
        {


            switch (w->golden_angle){

                case 0:
                    /* rotation of spiral arms */
                    c1=cos((float)j*2.0/(float)wsp->ni*M_PI);
                    s1=sin((float)j*2.0/(float)wsp->ni*M_PI);

                    /* rotation of radial arms */
                    c2=cos((float)i/(float)w->spiral_radial_nspokes*M_PI);
                    s2=sin((float)i/(float)w->spiral_radial_nspokes*M_PI);
                    w->T[12*k+0]  = c1;
                    w->T[12*k+1]  = s1;
                    w->T[12*k+2]  = 0;
                    w->T[12*k+3]  = 0;
                    w->T[12*k+4]  = -c2*s1;
                    w->T[12*k+5]  = c1*c2;
                    w->T[12*k+6]  = s2;
                    w->T[12*k+7]  = 0;
                    w->T[12*k+8]  = s1*s2;
                    w->T[12*k+9]  = -c1*s2;
                    w->T[12*k+10] = c2;
                    w->T[12*k+11] = 0;
                    break;

                case 1:
                    /* rotation of spiral arms */
                    c1=cos((float)j*2.0/(float)wsp->ni*M_PI);
                    s1=sin((float)j*2.0/(float)wsp->ni*M_PI);

                    /* GOLDEN rotation of radial arms */
                    c2=cos(golden_angle_coef(i,1,1)*M_PI);
                    s2=sin(golden_angle_coef(i,1,1)*M_PI);
                    w->T[12*k+0]  = c1;
                    w->T[12*k+1]  = s1;
                    w->T[12*k+2]  = 0;
                    w->T[12*k+3]  = 0;
                    w->T[12*k+4]  = -c2*s1;
                    w->T[12*k+5]  = c1*c2;
                    w->T[12*k+6]  = s2;
                    w->T[12*k+7]  = 0;
                    w->T[12*k+8]  = s1*s2;
                    w->T[12*k+9]  = -c1*s2;
                    w->T[12*k+10] = c2;
                    w->T[12*k+11] = 0;
                    break;

                case 2:
                    /* GOLDEN rotation of spiral arms */
                    c1=cos(golden_angle_coef(j,1,1)*2.0*M_PI);
                    s1=sin(golden_angle_coef(j,1,1)*2.0*M_PI);

                    /* GOLDEN rotation of radial arms */
                    c2=cos(golden_angle_coef(i,1,1)*M_PI);
                    s2=sin(golden_angle_coef(i,1,1)*M_PI);
                    w->T[12*k+0]  = c1;
                    w->T[12*k+1]  = s1;
                    w->T[12*k+2]  = 0;
                    w->T[12*k+3]  = 0;
                    w->T[12*k+4]  = -c2*s1;
                    w->T[12*k+5]  = c1*c2;
                    w->T[12*k+6]  = s2;
                    w->T[12*k+7]  = 0;
                    w->T[12*k+8]  = s1*s2;
                    w->T[12*k+9]  = -c1*s2;
                    w->T[12*k+10] = c2;
                    w->T[12*k+11] = 0;
                    break;

                case 3:
                    /* GOLDEN rotation of spiral arms */
                    c1=cos(golden_angle_coef(k,1,1)*2.0*M_PI);
                    s1=sin(golden_angle_coef(k,1,1)*2.0*M_PI);

                    /* GOLDEN rotation of radial arms */
                    c2=cos(golden_angle_coef(k,1,1)*M_PI);
                    s2=sin(golden_angle_coef(k,1,1)*M_PI);
                    w->T[12*k+0]  = c1;
                    w->T[12*k+1]  = s1;
                    w->T[12*k+2]  = 0;
                    w->T[12*k+3]  = 0;
                    w->T[12*k+4]  = -c2*s1;
                    w->T[12*k+5]  = c1*c2;
                    w->T[12*k+6]  = s2;
                    w->T[12*k+7]  = 0;
                    w->T[12*k+8]  = s1*s2;
                    w->T[12*k+9]  = -c1*s2;
                    w->T[12*k+10] = c2;
                    w->T[12*k+11] = 0;
                    break;

                case 4:
                    phi = golden_angle_coef(k,1,1)*2.0*M_PI;
                    cphi  = cos(phi);
                    sphi  = sin(phi);

                    xx = golden_angle_coef(k,2,1);

                    yy = golden_angle_coef(k,2,2);

                    zbeta  = acos(xx);
                    czeta = cos(zeta);
                    szeta = sin(zeta);

                    zalpha = 2.0*3.14159265 * yy;

                    czbeta = cos(zbeta);
                    szbeta = sin(zbeta);

                    czalpha = cos(zalpha);
                    szalpha = sin(zalpha);


                    w->T[12*k+0] = czalpha*czbeta*cphi-szalpha*sphi;
                    w->T[12*k+1] = czalpha*czbeta*sphi+szalpha*cphi;
                    w->T[12*k+2] = 0;
                    w->T[12*k+3] = 0;
                    w->T[12*k+4] = -szalpha*czbeta*cphi-czalpha*sphi;
                    w->T[12*k+5] = -szalpha* czbeta*sphi+czalpha*cphi;
                    w->T[12*k+6] = 0;
                    w->T[12*k+7] = 0;
                    w->T[12*k+8] = -szbeta*cphi;
                    w->T[12*k+9] = -szbeta*sphi;
                    w->T[12*k+10] = 0;
                    w->T[12*k+11] = 0;


                    break;


            }

            /*w->T[12*(i*wsp->ni+j)+0]  = c1;
    w->T[12*(i*wsp->ni+j)+1]  = s1;
    w->T[12*(i*wsp->ni+j)+2]  = 0;
    w->T[12*(i*wsp->ni+j)+3]  = 0;
    w->T[12*(i*wsp->ni+j)+4]  = -c2*s1;
    w->T[12*(i*wsp->ni+j)+5]  = c1*c2;
    w->T[12*(i*wsp->ni+j)+6]  = s2;
    w->T[12*(i*wsp->ni+j)+7]  = 0;
    w->T[12*(i*wsp->ni+j)+8]  = s1*s2;
    w->T[12*(i*wsp->ni+j)+9]  = -c1*s2;
    w->T[12*(i*wsp->ni+j)+10] = c2;
    w->T[12*(i*wsp->ni+j)+11] = 0;  */


            k=k+1;



        }
    }

    /* fprintf(stderr,"\t done generating the interleaves...\n");fflush(stderr);
     */
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;

    /*   w->n_psd_seg_gx=1; */
    /*   w->psd_segs_gx[0]=0; */
    /*   w->psd_segs_gx[1]=w->gx_res; */
    /*   w->n_psd_seg_gy=1; */
    /*   w->psd_segs_gy[0]=0; */
    /*   w->psd_segs_gy[1]=w->gy_res; */
    /*   w->n_psd_seg_gz=1; */
    /*   w->psd_segs_gz[0]=0; */
    /*   w->psd_segs_gz[1]=w->gz_res; */

    /* rhfrsize */
    w->frsize=w->gx_end-w->gx_start+1;

    free(wsp);


    /* fprintf(stderr,"\tend\n");fflush(stderr);*/

    return 1;

};


int gen_vd_spiral(generic_ro * w)
{
    float gammavd = 4.25759e3;

    /*float alpha = 3.;*/
    float km;
    float chi;
    float omega;
    int nturn;
    float dt;
    float g_over;
    float S0;

    int i, j, n;
    double Te, Tr, T1, tm;
    float Ts, ts, t, theta;
    float c1, s1, c2, s2;
    float bx, by, c, s;
    float tau[2*MAXPTSPERINT];
    float q;

    /* getting a copy of all variables necessary */
    float D = w->fovx;
    int N = w->nx;
    float dts=w->dts;
    int nl=w->ni;
    float gamp=w->gmax;
    float gslew = w->gslew;
    float alpha=w->vdspiral_alpha;




    q = 5;      /* nice number  */
    S0 = gslew*100;
    dt = dts;
    /* find chi and nturn */
    km = .5*(float)N/D;
    chi = km;
    nturn = floor(1./(1.-pow(1.-2./((float)N/(float)nl), 1./alpha)));
    omega = 2*M_PI*nturn;

    /* some variables for time */
    Te = sqrt(S0*gammavd/(chi*omega*omega));
    Tr = (gammavd*gamp)/(chi*omega);
    Ts = 1./(alpha/2.+1.)/Te;
    T1 = (alpha/2.+1.)*Te;
    ts = pow((Tr*(alpha/2.+1.)/pow(T1, (alpha+1.)/(alpha*.5+1.))), (1.+2./alpha));

    n = 0;
    if(ts < Ts) {
        Ts = 1./(Tr*(alpha+1.));

        /* slew-rate limited approximation  */

        for(i=0; i<=floor(ts/dt); i++) {
            t=(float)i*dt;
            tau[n++]=pow((alpha/2.+1.)*Te*t, 1./(alpha/2.+1.));
        }

        ts = floor(ts/4e-6)*4e-6;
        tm = pow(Te*(alpha/2.+1.)*ts, (alpha+1.)/(alpha*.5+1.)) / (Tr*(alpha+1.));

        /* gamp limited approximation */
        for(i=0;i<=floor((Ts-tm)/dt);i++) {
            t=tm+dt+(float)i*dt;
            tau[n++] = pow(Tr*(alpha+1.)*t, 1./(alpha+1.));
        }
    }

    /* start as well as end with gslew limited case */
    else {
        for(t=0; t<=Ts; t+=dt)
            tau[n++] = pow((alpha/2.+1.)*Te*t, 1./(alpha/2.+1.));
    }


    for (i=1; i<n; i++) {
        c2 = cos(omega*tau[i]);
        c1 = cos(omega*tau[i-1]);
        s2 = sin(omega*tau[i]);
        s1 = sin(omega*tau[i-1]);

        w->gx[i-1] = chi*(pow(tau[i], alpha)*c2-pow(tau[i-1], alpha)*c1)/gammavd/dt;
        w->gy[i-1] = chi*(pow(tau[i], alpha)*s2-pow(tau[i-1], alpha)*s1)/gammavd/dt;

        if(w->gx[i-1]>gamp) w->gx[i-1]=gamp;
        if(w->gx[i-1]<-1.*gamp) w->gx[i-1]=-1.*gamp;
        if(w->gy[i-1]>gamp) w->gy[i-1]=gamp;
        if(w->gy[i-1]<-1.*gamp) w->gy[i-1]=-1.*gamp;

        /* limit for 45' rotation as well */
        g_over = sqrt(w->gx[i-1]*w->gx[i-1] + w->gy[i-1]*w->gy[i-1]);
        if (g_over > gamp)
        {
            w->gx[i-1] = w->gx[i-1] * gamp / g_over;
            w->gy[i-1] = w->gy[i-1] * gamp / g_over;
        }
    }

    /* making sure that the first point is 0*/
    for (j=i-2;j>=0;j--)
    {
        w->gx[j+1]=w->gx[j];
        w->gy[j+1]=w->gy[j];
    }
    w->gx[0]=0.0;
    w->gy[0]=0.0;
    i++;

    /* making sure that the resolution is even */
    if ((i-1) % 2)
    {
        w->gx[i-1]=w->gx[i-2];
        w->gy[i-1]=w->gy[i-2];
        i++;
    }
    n=0;

    /* this does not need to be decimated */
    for (j=0; j<i-1; j++)
    {
        bx = w->gx[n];
        by = w->gy[n];
        w->gx[n] = LIMIT(bx, -gamp, gamp);
        w->gy[n++] = LIMIT(by, -gamp, gamp);
    }

    w->gx_ctr=0;
    w->gy_ctr=0;
    w->gx_res=n;
    w->gy_res=n;
    w->gz_res=0;
    w->gx_start=0;
    w->gy_start=0;
    w->gx_end=n-1;
    w->gy_end=n-1;

    /* going from gradients to k-space, assuming spiral starts at 0 */
    gen_k_from_g(w->gx,w->gx_res,w->dts,0.0,w->kx);
    gen_k_from_g(w->gy,w->gy_res,w->dts,0.0,w->ky);

    /* constructing all interleaves */
    for (i=0;i<nl;i++) {
        theta = i*2.0*M_PI/(float)nl;
        c = cos(theta);
        s = sin(theta);
        w->T[12*i+0]=c;   w->T[12*i+1]=-s;   w->T[12*i+2] =0.0; w->T[12*i+3] =0.0;
        w->T[12*i+4]=s;   w->T[12*i+5]=c;    w->T[12*i+6] =0.0; w->T[12*i+7] =0.0;
        w->T[12*i+8]=0.0; w->T[12*i+9]=0.0;  w->T[12*i+10]=1.0; w->T[12*i+11]=0.0;
    }

    /* number of segments with different transformation matrices*/
    w->nseg=1;
    w->seg_nodes[0]=0;
    w->seg_nodes[1]=w->gx_res;
    /* number of segments with different WF_pulses */
    /*   w->n_psd_seg_gx=1; */
    /*   w->psd_segs_gx[0]=0; */
    /*   w->psd_segs_gx[1]=w->gx_res; */
    /*   w->n_psd_seg_gy=1; */
    /*   w->psd_segs_gy[0]=0; */
    /*   w->psd_segs_gy[1]=w->gy_res; */
    /*   w->n_psd_seg_gz=0; */
    /* rhfrsize */
    w->frsize=w->gx_end-w->gx_start+1;



    return 1;

};
