/*
 * gen_turbine.c
 *
 *  Created on: Aug 17, 2015
 *      Author: bzahneisen
 */


int gen_turbine(generic_ro *w )
{
    float c,s;
    int i,j,k,l;

    generic_ro * wepi=(generic_ro*)malloc(sizeof(generic_ro));

    /* generating the EPI part */
    w->ni=w->ni/w->turbine_radial_nspokes;
    gen_epi(w);

    /* adding the ramp and prewinders because doing this retrospectively causes
     problems
     */
    add_ramp_prewinder_to_g(w);



    /* add the rewinder if needed */
    if(w->rewind_readout){
        add_rewinder_to_g(w);
    }

    /* copying to a temporary variable */
    copy_w(w,wepi);


    /* setting back the number of interleaves*/
    w->ni=w->ni*w->turbine_radial_nspokes;

    /*   w->n_psd_seg_gz=w->n_psd_seg_gx; */
    /*   for (i=0;i<w->n_psd_seg_gx+1;i++) */
    /*     { */
    /*       w->psd_segs_gz[i]=w->psd_segs_gx[i]; */
    /*     } */



    w->gx_res=wepi->gx_res;
    w->gy_res=wepi->gx_res;
    w->gz_res=wepi->gx_res;
    w->gx_start=wepi->gx_start;
    w->gx_end=wepi->gx_end;
    w->gy_start=wepi->gy_start;
    w->gy_end=wepi->gy_end;
    w->gz_start=wepi->gx_start;
    w->gz_end=wepi->gx_end;
    w->gx_ctr=wepi->gx_ctr;
    w->gy_ctr=wepi->gy_ctr;
    w->gz_ctr=wepi->gx_ctr;


    for (j=0;j<w->gx_res*wepi->ni;j++)
    {
        w->gx[j]= wepi->gx[j];
        w->gy[j]= wepi->gy[j];
        w->gz[j]= 0.0;
    }
    for (j=0;j<(w->gx_res+1)*wepi->ni;j++)
    {
        w->kx[j]= wepi->kx[j];
        w->ky[j]= wepi->ky[j];
        w->kz[j]= 0.0;
    }


    /* This one also has prewinders in there, I should modify
     all transfer functions */
    for (l=0;l<w->nseg;l++)
    {
        for (i=0;i<w->turbine_radial_nspokes;i++)
        {

            switch (w->golden_angle){
                case 0:
                    c=cos((float)i/(float)w->turbine_radial_nspokes*M_PI);
                    s=sin((float)i/(float)w->turbine_radial_nspokes*M_PI);
                    break;
                case 1:
                    c=cos(golden_angle_coef(i,1,1)*M_PI);
                    s=sin(golden_angle_coef(i,1,1)*M_PI);
                    break;
            }

            for (j=0;j<wepi->ni;j++)
            {
                k=l*w->ni+i*wepi->ni+j;

                w->T[k*12+0]  =wepi->T[l*wepi->ni*12+j*12+0]*c + wepi->T[l*wepi->ni*12+j*12+8]*s;
                w->T[k*12+1]  =wepi->T[l*wepi->ni*12+j*12+1]*c + wepi->T[l*wepi->ni*12+j*12+9]*s;
                w->T[k*12+2]  =wepi->T[l*wepi->ni*12+j*12+2]*c + wepi->T[l*wepi->ni*12+j*12+10]*s;
                w->T[k*12+3]  =wepi->T[l*wepi->ni*12+j*12+3]*c + wepi->T[l*wepi->ni*12+j*12+11]*s;
                w->T[k*12+4]  =wepi->T[l*wepi->ni*12+j*12+4];
                w->T[k*12+5]  =wepi->T[l*wepi->ni*12+j*12+5];
                w->T[k*12+6]  =wepi->T[l*wepi->ni*12+j*12+6];
                w->T[k*12+7]  =wepi->T[l*wepi->ni*12+j*12+7];
                w->T[k*12+8]  =wepi->T[l*wepi->ni*12+j*12+8]*c - wepi->T[l*wepi->ni*12+j*12+0]*s;
                w->T[k*12+9]  =wepi->T[l*wepi->ni*12+j*12+9]*c - wepi->T[l*wepi->ni*12+j*12+1]*s;
                w->T[k*12+10]  =wepi->T[l*wepi->ni*12+j*12+10]*c - wepi->T[l*wepi->ni*12+j*12+2]*s;
                w->T[k*12+11]  =wepi->T[l*wepi->ni*12+j*12+11]*c - wepi->T[l*wepi->ni*12+j*12+3]*s;

            }
        }
    }

    /* for testing only ! */

    /*add_nav_to_g(w);

     */

    free_w(wepi);

    fprintf(stderr,"\tend\n");fflush(stderr);

    return 1;
};

