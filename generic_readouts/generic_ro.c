/* generic_ro.c
 * Murat Aksoy aksoymurat@gmail.com
 * Rafael O'Halloran rafael.ohalloran@gmail.com
 * Stanford University, 2011
 */

#include "generic_ro.h"


void write_log(char *msg)
{
#ifdef SIM
    FILE * f=fopen("gen_ro_log.txt","a");
#else
FILE * f=fopen("/usr/g/bin/gen_ro_log.txt","a");
#endif

static char time_str[1000] = {0};   /* Stroes the time as a string */
struct tm *now = NULL;
int hour = 0;
time_t time_value = 0;
time_value = time(NULL);          /* Get time value              */
now = localtime(&time_value);     /* Get time and date structure */
hour = now->tm_hour;              /* Save the hour value         */
sprintf(time_str,"%04d_%02d_%02d_%02d_%02d_%02d\n",
        now->tm_year,now->tm_mon,now->tm_mday,now->tm_hour,
        now->tm_min,now->tm_sec);
fprintf(f,time_str);
fprintf(f,msg);
fprintf(f,"\n");
fclose(f);
};

void write_log_2(generic_ro * w)
{
    FILE * f=fopen("gen_ro_log.txt","a");
    char txt[1000];
    static char time_str[1000] = {0};   /* Stroes the time as a string */
    struct tm *now = NULL;
    int hour = 0;
    time_t time_value = 0;
    time_value = time(NULL);          /* Get time value              */
    now = localtime(&time_value);     /* Get time and date structure */
    hour = now->tm_hour;              /* Save the hour value         */
    sprintf(time_str,"%04d_%02d_%02d_%02d_%02d_%02d\n",
            now->tm_year,now->tm_mon,now->tm_mday,now->tm_hour,
            now->tm_min,now->tm_sec);
    fprintf(f,time_str);  
    sprintf(txt,"fovx %f\n",w->fovx);fprintf(f,txt);
    sprintf(txt,"fovy %f\n",w->fovy);fprintf(f,txt);
    sprintf(txt,"fovz %f\n",w->fovz);fprintf(f,txt);
    sprintf(txt,"gx_ctr %d\n",w->gx_ctr);fprintf(f,txt);
    sprintf(txt,"gy_ctr %d\n",w->gy_ctr);fprintf(f,txt);
    sprintf(txt,"gz_ctr %d\n",w->gz_ctr);fprintf(f,txt);
    sprintf(txt,"gx_res %d\n",w->gx_res);fprintf(f,txt);
    sprintf(txt,"gy_res %d\n",w->gy_res);fprintf(f,txt);
    sprintf(txt,"gz_res %d\n",w->gz_res);fprintf(f,txt);
    sprintf(txt,"gx_start %d\n",w->gx_start);fprintf(f,txt);
    sprintf(txt,"gy_start %d\n",w->gy_start);fprintf(f,txt);
    sprintf(txt,"gz_start %d\n",w->gz_start);fprintf(f,txt);
    sprintf(txt,"gx_end %d\n",w->gx_end);fprintf(f,txt);
    sprintf(txt,"gy_end %d\n",w->gy_end);fprintf(f,txt);
    sprintf(txt,"gz_end %d\n",w->gz_end);fprintf(f,txt);
    sprintf(txt,"ro_type %d\n",w->ro_type);fprintf(f,txt);
    sprintf(txt,"frsize %d\n",w->frsize);fprintf(f,txt);
    sprintf(txt,"ni %d\n",w->ni);fprintf(f,txt);
    sprintf(txt,"nx %d\n",w->nx);fprintf(f,txt);
    sprintf(txt,"ny %d\n",w->ny);fprintf(f,txt);
    sprintf(txt,"nz %d\n",w->nz);fprintf(f,txt);
    sprintf(txt,"dts %f\n",w->dts);fprintf(f,txt);
    sprintf(txt,"fn %f\n",w->fn);fprintf(f,txt);
    sprintf(txt,"vdspiral_alpha %f\n",w->vdspiral_alpha);fprintf(f,txt);
    sprintf(txt,"gmax %f\n",w->gmax);fprintf(f,txt);
    sprintf(txt,"gslew %f\n",w->gslew);fprintf(f,txt);
    sprintf(txt,"Tmax %f\n",w->Tmax);fprintf(f,txt);
    sprintf(txt,"epi_blip_res %d\n",w->epi_blip_res);fprintf(f,txt);
    sprintf(txt,"epi_flat_res %d\n",w->epi_flat_res);fprintf(f,txt);
    sprintf(txt,"epi_rampsamp %d\n",w->epi_rampsamp);fprintf(f,txt);
    sprintf(txt,"pepi_nblades %d\n",w->pepi_nblades);fprintf(f,txt);
    sprintf(txt,"spiral_radial_nspokes %d\n",w->spiral_radial_nspokes);fprintf(f,txt);
    fprintf(f,"\n");
    fclose(f);
};

void write_log_3(generic_ro * w)
{
    
    fprintf(stderr,"fovx %f\n",w->fovx);
    fprintf(stderr,"fovy %f\n",w->fovy);
    fprintf(stderr,"fovz %f\n",w->fovz);
    fprintf(stderr,"gx_ctr %d\n",w->gx_ctr);
    fprintf(stderr,"gy_ctr %d\n",w->gy_ctr);
    fprintf(stderr,"gz_ctr %d\n",w->gz_ctr);
    fprintf(stderr,"gx_res %d\n",w->gx_res);
    fprintf(stderr,"gy_res %d\n",w->gy_res);
    fprintf(stderr,"gz_res %d\n",w->gz_res);
    fprintf(stderr,"gx_start %d\n",w->gx_start);
    fprintf(stderr,"gy_start %d\n",w->gy_start);
    fprintf(stderr,"gz_start %d\n",w->gz_start);
    fprintf(stderr,"gx_end %d\n",w->gx_end);
    fprintf(stderr,"gy_end %d\n",w->gy_end);
    fprintf(stderr,"gz_end %d\n",w->gz_end);
    fprintf(stderr,"ro_type %d\n",w->ro_type);
    fprintf(stderr,"frsize %d\n",w->frsize);
    fprintf(stderr,"ni %d\n",w->ni);
    fprintf(stderr,"nx %d\n",w->nx);
    fprintf(stderr,"ny %d\n",w->ny);
    fprintf(stderr,"nz %d\n",w->nz);
    fprintf(stderr,"fn %f\n",w->fn);
    fprintf(stderr,"fn2 %f\n",w->fn2);
    fprintf(stderr,"dts %f\n",w->dts);
    fprintf(stderr,"fn %f\n",w->fn);
    fprintf(stderr,"vdspiral_alpha %f\n",w->vdspiral_alpha);
    fprintf(stderr,"gmax %f\n",w->gmax);
    fprintf(stderr,"gslew %f\n",w->gslew);
    fprintf(stderr,"Tmax %f\n",w->Tmax);
    fprintf(stderr,"epi_blip_res %d\n",w->epi_blip_res);
    fprintf(stderr,"epi_flat_res %d\n",w->epi_flat_res);
    fprintf(stderr,"epi_rampsamp %d\n",w->epi_rampsamp);
    fprintf(stderr,"pepi_nblades %d\n",w->pepi_nblades);
    fprintf(stderr,"spiral_radial_nspokes %d\n",w->spiral_radial_nspokes);
    fprintf(stderr,"turbine_radial_nspokes %d\n",w->turbine_radial_nspokes);
    fprintf(stderr,"rewind_readout %d\n",w->rewind_readout);
    fprintf(stderr,"golden_angle %d\n",w->golden_angle);
    fprintf(stderr,"reverse_readout %d\n",w->reverse_readout);
    fprintf(stderr,"permute %d\n",w->permute);
    fprintf(stderr,"i_ctr %d\n",w->i_ctr);
    fprintf(stderr,"R %d\n",w->R);
    fprintf(stderr,"R_skip %d\n",w->R_skip);
    fflush(stderr);
};


void write_kspace(generic_ro * w, char *fname)
{
    /*#ifdef SIM
  FILE * f=fopen("kspace_traj.txt","w");
#else
  FILE * f=fopen("/usr/g/bin/kspace_traj.txt","w");
#endif*/
    FILE *f;
    char filestring[150];
    int j;
    
#ifdef SIM
strcpy( filestring, fname );
#else
char *fpath="/usr/g/bin/";
strcpy( filestring, fpath );
strcat( filestring, fname );
#endif

f = fopen( filestring, "w" );


for (j=0;j<w->gx_res+1;j++) {
    
    fprintf(f,"%f \t %f \t %f \n",w->kx[j],w->ky[j],w->kz[j]);
};

fclose(f);
};


void init_ro(generic_ro * w) {
    w->gx_res=0;
    w->gy_res=0;
    w->gz_res=0;
    w->nseg=0;
    w->frsize=0;
    w->ro_type=-1;
    w->gx_ctr=0;
    w->gy_ctr=0;
    w->gz_ctr=0;
    w->gx_start=0;
    w->gy_start=0;
    w->gz_start=0;
    w->gx_end=0;
    w->gy_end=0;
    w->gz_end=0;
    w->ni=0;
    w->nx=0;
    w->ny=0;
    w->nz=0;
    w->dts=0.0;
    w->dts_adc=0.0;
    w->fn=1.0;
    w->fn2=1.0;
    w->vdspiral_alpha=0.0;
    w->gmax=0.0;
    w->gslew=0.0;
    w->Tmax=0.0;
    w->epi_blip_res=0;
    w->epi_flat_res=0;
    w->epi_ramp_res=0;
    w->epi_rampsamp=0;
    w->epi_blip_up_down=0;
    w->epi_multiband_factor=1;
    w->epi_multiband_fov = -1.0;
    w->epi_multiband_R=1.0;
    w->pepi_nblades=0;    
    w->spiral_radial_nspokes=0;
    w->turbine_radial_nspokes=0;
    w->rewind_readout=0;
    w->golden_angle=0;
    w->reverse_readout=0;
    w->permute=12; /* 012 = xyz */
    w->i_ctr=0;
    w->R=1;
    w->R_skip=1;
    /*ADD_NEW_RO_TYPE*/
    
#ifdef COMPILE_FOR_PSD
/*w->gxw=NULL;
 * w->gyw=NULL;
 * w->gzw=NULL;
 * w->a_gxw=0.0;
 * w->a_gyw=0.0;
 * w->a_gzw=0.0;
 * w->ia_gxw=0;
 * w->ia_gyw=0;
 * w->ia_gzw=0;
 * w->pw_gxw=0;
 * w->pw_gyw=0;
 * w->pw_gzw=0;*/
#endif

};

int gen_g_from_k(float * k,
        int kres,
        float dts,
        float * g)
{
    
    int i;
    float gamma = 2.0*M_PI*4.257e3;/* rad * Hz / G */
    
    for (i=0;i<kres-1;i++)
        g[i]=(k[i+1]-k[i])/gamma/dts/0.1;
    
    return 1;
};

int gen_k_from_g(float * g,
        int gres,
        float dts,
        float k0,
        float * k)
{
    int i;
    float gamma = 2.0*M_PI*4.257e3;/* rad * Hz / G */
    k[0]=k0;
    for (i=0;i<gres;i++)
        k[i+1]=k[i]+g[i]*gamma*dts*0.1;
    
    return 1;
};


int revert_w(generic_ro * w)
{
    /* for 2d acquisitions add zeros to gz and kz */
    
    int i,j;
    
    generic_ro * wtemp=(generic_ro*)malloc(sizeof(generic_ro));
    
    /* copying to a temporary variable */
    copy_w(w,wtemp);
    
    /* reversing the k-space trajectory */
    if (w->gx_res!=0)
    {
        for (j = 0; j < w->gx_res+1; j++) w->kx[j]=wtemp->kx[w->gx_res-j];
        gen_g_from_k(w->kx,w->gx_res+1,w->dts,w->gx);
    }
    if (w->gy_res!=0)
    {
        for (j = 0; j < w->gy_res+1 ; j++) w->ky[j]=wtemp->ky[w->gy_res-j];
        gen_g_from_k(w->ky,w->gy_res+1,w->dts,w->gy);
    }
    if (w->gz_res!=0)
    {
        for (j = 0; j < w->gz_res+1 ; j++) w->kz[j]=wtemp->kz[w->gz_res-j];
        gen_g_from_k(w->kz,w->gz_res+1,w->dts,w->gz);
    }
    
    
    
    
    /* changing psd nodes */
    /*   if (w->gx_res!=0) */
    /*     for (i=0;i<w->n_psd_seg_gx+1;i++)  */
    /*       w->psd_segs_gx[i] = w->gx_res-wtemp->psd_segs_gx[w->n_psd_seg_gx-i]; */
    /*   if (w->gy_res!=0) */
    /*     for (i=0;i<w->n_psd_seg_gy+1;i++)  */
    /*       w->psd_segs_gy[i] = w->gy_res-wtemp->psd_segs_gy[w->n_psd_seg_gy-i]; */
    /*   if (w->gz_res!=0) */
    /*     for (i=0;i<w->n_psd_seg_gz+1;i++)  */
    /*       w->psd_segs_gz[i] = w->gz_res-wtemp->psd_segs_gz[w->n_psd_seg_gz-i]; */
    
    /* the transformation matrix nodes  and transformation matrices*/
    for (i=0;i<w->nseg+1;i++)
        w->seg_nodes[i] = wtemp->gx_res-wtemp->seg_nodes[w->nseg-i];
    for (i=0;i<w->nseg;i++)
        for (j=0;j<12*w->ni;j++)
            w->T[i*12*w->ni+j]=wtemp->T[(w->nseg-i-1)*12*w->ni+j];
    
    
    /* start, end and center */
    if (w->gx_res!=0)
    {
        w->gx_start=w->gx_res-wtemp->gx_end-1;
        w->gx_end=w->gx_res-wtemp->gx_start-1;
        w->gx_ctr=w->gx_res-wtemp->gx_ctr-1;
    }
    if (w->gy_res!=0)
    {
        w->gy_start=w->gy_res-wtemp->gy_end-1;
        w->gy_end=w->gy_res-wtemp->gy_start-1;
        w->gy_ctr=w->gy_res-wtemp->gy_ctr-1;
    }
    if (w->gz_res!=0)
    {
        w->gz_start=w->gz_res-wtemp->gz_end-1;
        w->gz_end=w->gz_res-wtemp->gz_start-1;
        w->gz_ctr=w->gz_res-wtemp->gz_ctr-1;
    }
    
    free_w(wtemp);
    
    return 1;
};




int zero_w(generic_ro * w)
{
    /* for 2d acquisitions add zeros to gz and kz */
    
    int j;
    
    
    if (w->gx_res!=0)
    {
        for (j = 0; j < w->gx_res; j++) w->gx[j]=0;
        gen_k_from_g(w->gx,w->gx_res,w->dts,w->kx[0],w->kx);
    }
    if (w->gy_res!=0)
    {
        for (j = 0; j < w->gy_res; j++) w->gy[j]=0;
        gen_k_from_g(w->gy,w->gy_res,w->dts,w->ky[0],w->ky);
    }
    if (w->gz_res!=0)
    {
        for (j = 0; j < w->gz_res; j++) w->gz[j]=0;
        gen_k_from_g(w->gz,w->gz_res,w->dts,w->kz[0],w->kz);
        
    }
    
    return 1;
};





int cat_w(generic_ro * w1, generic_ro * w2, int centerref)
{
    int i;
    
    /* adding the gradient waveforms and the k-space trajs */
    for (i=0;i<w2->gx_res;i++)
        w1->gx[w1->gx_res+i]=w2->gx[i];
    gen_k_from_g(w1->gx,w1->gx_res+w2->gx_res,w1->dts,w1->kx[0],w1->kx);
    
    for (i=0;i<w2->gy_res;i++)
        w1->gy[w1->gy_res+i]=w2->gy[i];
    gen_k_from_g(w1->gy,w1->gy_res+w2->gy_res,w1->dts,w1->ky[0],w1->ky);
    
    for (i=0;i<w2->gz_res;i++)
        w1->gz[w1->gz_res+i]=w2->gz[i];
    gen_k_from_g(w1->gz,w1->gz_res+w2->gz_res,w1->dts,w1->kz[0],w1->kz);
    
    /* center, end */
    
    
    /* psd nodes */
    
    /*   for (i=0;i<w2->n_psd_seg_gx;i++)     */
    /*     w1->psd_segs_gx[w1->n_psd_seg_gx+i+1]=w2->psd_segs_gx[i+1]+w1->gx_res;     */
    /*   w1->n_psd_seg_gx+=w2->n_psd_seg_gx; */
    
    /*   for (i=0;i<w2->n_psd_seg_gy;i++)     */
    /*     w1->psd_segs_gy[w1->n_psd_seg_gy+i+1]=w2->psd_segs_gy[i+1]+w1->gy_res;     */
    /*   w1->n_psd_seg_gy+=w2->n_psd_seg_gy; */
    
    /*   for (i=0;i<w2->n_psd_seg_gz;i++) */
    /*     w1->psd_segs_gz[w1->n_psd_seg_gz+i+1]=w2->psd_segs_gz[i+1]+w1->gz_res;     */
    /*   w1->n_psd_seg_gz+=w2->n_psd_seg_gz; */
    
    
    /* the transformation matrix nodes and transformation matrices
     * remember that both readouts must have the same number of interleaves
     */
    for (i=0;i<w2->nseg;i++)
        w1->seg_nodes[w1->nseg+i+1] = w2->seg_nodes[i+1]+w1->gx_res;
    
    for (i=0;i<w2->nseg*w1->ni*12;i++)
        w1->T[w1->nseg*w1->ni*12+i]=w2->T[i];
    w1->nseg+=w2->nseg;
    
    
    /* centerref determines to use either the first (0) or the second structure's center point */
    if (centerref==1)
    {
        w1->gx_ctr=w2->gx_ctr+w1->gx_res;
        w1->gy_ctr=w2->gy_ctr+w1->gy_res;
        w1->gz_ctr=w2->gz_ctr+w1->gz_res;
    }
    
    w1->gx_end=w2->gx_end+w1->gx_res; w1->gx_res+=w2->gx_res;
    w1->gy_end=w2->gy_end+w1->gy_res; w1->gy_res+=w2->gy_res;
    w1->gz_end=w2->gz_end+w1->gz_res; w1->gz_res+=w2->gz_res;
    
    /* updating the frame size */
    w1->frsize=w1->gx_end-w1->gx_start+1;
    
    
    return 1;
};


int copy_w(generic_ro * w_src, generic_ro * w_dst){
    
    int j;
    
    w_dst->fovx=w_src->fovx;
    w_dst->fovy=w_src->fovy;
    w_dst->fovz=w_src->fovz;
    w_dst->gx_ctr=w_src->gx_ctr;
    w_dst->gy_ctr=w_src->gy_ctr;
    w_dst->gz_ctr=w_src->gz_ctr;
    w_dst->gx_res=w_src->gx_res;
    w_dst->gy_res=w_src->gy_res;
    w_dst->gz_res=w_src->gz_res;
    w_dst->gx_start=w_src->gx_start;
    w_dst->gy_start=w_src->gy_start;
    w_dst->gz_start=w_src->gz_start;
    w_dst->gx_end=w_src->gx_end;
    w_dst->gy_end=w_src->gy_end;
    w_dst->gz_end=w_src->gz_end;
    w_dst->ro_type=w_src->ro_type;
    w_dst->ni=w_src->ni;
    w_dst->nx=w_src->nx;
    w_dst->ny=w_src->ny;
    w_dst->nz=w_src->nz;
    w_dst->fn=w_src->fn;
    w_dst->fn2=w_src->fn2;
    w_dst->dts=w_src->dts;
    w_dst->dts_adc=w_src->dts_adc;
    w_dst->gmax=w_src->gmax;
    w_dst->gslew=w_src->gslew;
    w_dst->Tmax=w_src->Tmax;
    w_dst->vdspiral_alpha=w_src->vdspiral_alpha;
    w_dst->epi_blip_res=w_src->epi_blip_res;
    w_dst->epi_flat_res=w_src->epi_flat_res;
    w_dst->epi_ramp_res=w_src->epi_ramp_res;
    w_dst->epi_rampsamp=w_src->epi_rampsamp;
    w_dst->epi_blip_up_down=w_src->epi_blip_up_down;
    w_dst->epi_multiband_factor=w_src->epi_multiband_factor;
    w_dst->epi_multiband_fov=w_src->epi_multiband_fov;
    w_dst->epi_multiband_R=w_src->epi_multiband_R;
    w_dst->pepi_nblades=w_src->pepi_nblades;
    w_dst->spiral_radial_nspokes=w_src->spiral_radial_nspokes;
    w_dst->turbine_radial_nspokes=w_src->turbine_radial_nspokes;
    w_dst->rewind_readout=w_src->rewind_readout;
    w_dst->nseg=w_src->nseg;
    /*   w_dst->n_psd_seg_gx=w_src->n_psd_seg_gx; */
    /*   w_dst->n_psd_seg_gy=w_src->n_psd_seg_gy; */
    /*   w_dst->n_psd_seg_gz=w_src->n_psd_seg_gz; */
    w_dst->frsize=w_src->frsize;
    w_dst->golden_angle=w_src->golden_angle;
    w_dst->reverse_readout=w_src->reverse_readout;
    /*w_dst->turnoff_readout=w_src->turnoff_readout;  */
    w_dst->permute=w_src->permute;
    w_dst->i_ctr=w_src->i_ctr;
    w_dst->R=w_src->R;
    w_dst->R_skip=w_src->R_skip;
    /*ADD_NEW_RO_TYPE*/
    
    if (w_dst->gx_res!=0)
    {
        for (j=0;j<w_dst->gx_res;j++)
            w_dst->gx[j]=w_src->gx[j];
        for (j=0;j<w_dst->gx_res+1;j++)
            w_dst->kx[j]=w_src->kx[j];
    }
    if (w_dst->gy_res!=0)
    {
        for (j=0;j<w_dst->gy_res;j++)
            w_dst->gy[j]=w_src->gy[j];
        for (j=0;j<w_dst->gy_res+1;j++)
            w_dst->ky[j]=w_src->ky[j];
    }
    
    if (w_dst->gz_res!=0)
    {
        for (j=0;j<w_dst->gz_res;j++)
            w_dst->gz[j]=w_src->gz[j];
        for (j=0;j<w_dst->gz_res+1;j++)
            w_dst->kz[j]=w_src->kz[j];
    }
    
    for (j=0;j<w_dst->nseg*w_dst->ni*12;j++)
        w_dst->T[j]=w_src->T[j];
    
    for (j=0;j<w_dst->nseg+1;j++)
        w_dst->seg_nodes[j]=w_src->seg_nodes[j];
    
    /*   for (j=0;j<w_dst->n_psd_seg_gx+1;j++) */
    /*     w_dst->psd_segs_gx[j]=w_src->psd_segs_gx[j]; */
    
    /*   for (j=0;j<w_dst->n_psd_seg_gy+1;j++) */
    /*     w_dst->psd_segs_gy[j]=w_src->psd_segs_gy[j]; */
    
    /*   for (j=0;j<w_dst->n_psd_seg_gz+1;j++) */
    /*     w_dst->psd_segs_gz[j]=w_src->psd_segs_gz[j]; */
    
    
    return 1;
    
}

void free_w(generic_ro * w)
{
    free(w);
}

int add_ramp_prewinder_to_g(generic_ro * w)
{
    add_ramp_to_g(w);
    return add_prewinder_to_g(w);
    
};

int add_ramp_to_g(generic_ro * w)
{
    int i, j;
    float ramp_wav1[MAXPTSPERINT];
    float ramp_wav2[MAXPTSPERINT];
    float area;
    int ramp_res1=0;
    int ramp_res2=0;
    float ginit_max=0.0;
    float gend_max=0.0;
    float gslew = w->gslew;
    float dts=w->dts;
    float temp1,temp2;
    
    /* first, finding which interleaf needs the largest ramp */
    if (w->gx_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            if (fabs(w->gx_cur[0]) > ginit_max)
                ginit_max=fabs(w->gx_cur[0]);
            if (fabs(w->gx_cur[w->gx_res-1]) > gend_max)
                gend_max=fabs(w->gx_cur[w->gx_res-1]);
        }
    }
    
    if (w->gy_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            if (fabs(w->gy_cur[0]) > ginit_max)
                ginit_max=fabs(w->gy_cur[0]);
            if (fabs(w->gy_cur[w->gy_res-1]) > gend_max)
                gend_max=fabs(w->gy_cur[w->gy_res-1]);
        }
    }
    
    if (w->gz_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            if (fabs(w->gz_cur[0]) > ginit_max)
                ginit_max=fabs(w->gz_cur[0]);
            if (fabs(w->gz_cur[w->gz_res-1]) > gend_max)
                gend_max=fabs(w->gz_cur[w->gz_res-1]);
        }
    }
    
    /* generating the ramp according to the largest initial gradient amplitude */
    if (ginit_max !=0)
    {
        gen_ramp(0.0,
                ginit_max,
                0.0,
                gslew,
                dts,
                ramp_wav1,
                &ramp_res1,
                NULL);
        area=calc_g_area(ramp_wav1,ramp_res1,w->dts,w->tempmem);
    }
    else
    {
        ramp_res1=0;
        area=0.0;
    }
    
    if (gend_max != 0)
    {
        gen_ramp(gend_max,
                0.0,
                0.0,
                gslew,
                dts,
                ramp_wav2,
                &ramp_res2,
                NULL);
    }
    else
    {
        ramp_res2=0;
    }
    
    
    /*adding the ramps to the beginning and to the end*/
    if (w->gx_res != 0)
    {
        temp1=w->gx[0];
        temp2=w->gx[w->gx_res-1];
        for (j=w->gx_res-1;j>=0;j--)
            w->gx[j+ramp_res1]=w->gx[j];
        for (j=0;j<ramp_res1;j++)
            w->gx[j]=ramp_wav1[j]*temp1/ginit_max;
        for (j=0;j<ramp_res2;j++)
            w->gx[j+ramp_res1+w->gx_res]=ramp_wav2[j]*temp2/gend_max;
        w->gx_res+=ramp_res1+ramp_res2;
        if (ginit_max!=0)
            w->kx[0]-=area*temp1/ginit_max;
        gen_k_from_g(w->gx,w->gx_res,w->dts,w->kx[0],w->kx);
    }
    
    if (w->gy_res != 0)
    {
        temp1=w->gy[0];
        temp2=w->gy[w->gy_res-1];
        for (j=w->gy_res-1;j>=0;j--)
            w->gy[j+ramp_res1]=w->gy[j];
        for (j=0;j<ramp_res1;j++)
            w->gy[j]=ramp_wav1[j]*temp1/ginit_max;
        for (j=0;j<ramp_res2;j++)
            w->gy[j+ramp_res1+w->gy_res]=ramp_wav2[j]*temp2/gend_max;
        w->gy_res+=ramp_res1+ramp_res2;
        if (ginit_max!=0)
            w->ky[0]-=area*temp1/ginit_max;
        gen_k_from_g(w->gy,w->gy_res,w->dts,w->ky[0],w->ky);
    }
    
    if (w->gz_res != 0)
    {
        temp1=w->gz[0];
        temp2=w->gz[w->gz_res-1];
        for (j=w->gz_res-1;j>=0;j--)
            w->gz[j+ramp_res1]=w->gz[j];
        for (j=0;j<ramp_res1;j++)
            w->gz[j]=ramp_wav1[j]*temp1/ginit_max;
        for (j=0;j<ramp_res2;j++)
            w->gz[j+ramp_res1+w->gz_res]=ramp_wav2[j]*temp2/gend_max;
        w->gz_res+=ramp_res1+ramp_res2;
        if (ginit_max!=0)
            w->kz[0]-=area*temp1/ginit_max;
        gen_k_from_g(w->gz,w->gz_res,w->dts,w->kz[0],w->kz);
    }
    
    /* adjusting the nodes so that the ramp is part of the initial node */
    for (j=1;j<w->nseg+1;j++)
        w->seg_nodes[j]+=ramp_res1;
    w->seg_nodes[w->nseg]+=ramp_res2;
    
    /* adjusting the psd segments
     * initial ramp is added to the first WF_PULSE
     * final ramp is added to the last WF_PULSE
     */
    /*   if (w->gx_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gx+1;j++) */
    /* 	w->psd_segs_gx[j]+=ramp_res1; */
    /*       w->psd_segs_gx[w->n_psd_seg_gx]+=ramp_res2; */
    /*     } */
    /*   if (w->gy_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gy+1;j++) */
    /* 	w->psd_segs_gy[j]+=ramp_res1; */
    /*       w->psd_segs_gy[w->n_psd_seg_gy]+=ramp_res2; */
    /*     } */
    /*   if (w->gz_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gz+1;j++) */
    /* 	w->psd_segs_gz[j]+=ramp_res1; */
    /*       w->psd_segs_gz[w->n_psd_seg_gz]+=ramp_res2; */
    /*     } */
    
    /*write_log("add_ramp_prewinder - 2\n");
    write_log_2(w);*/
    
    
    /* adjusting start & end */
    if (w->gx_res !=0)
    {
        w->gx_ctr   += ramp_res1;
        w->gx_start += ramp_res1;
        w->gx_end   += ramp_res1;
    }
    if (w->gy_res !=0)
    {
        w->gy_ctr   += ramp_res1;
        w->gy_start += ramp_res1;
        w->gy_end   += ramp_res1;
    }
    if (w->gz_res !=0)
    {
        w->gz_ctr   += ramp_res1;
        w->gz_start += ramp_res1;
        w->gz_end   += ramp_res1;
    }
    
    
    return 1;
    
}

int add_prewinder_to_g(generic_ro * w)
{
    
    int i, j;
    int need_to_fix;
    int need_to_fix_max;
    int best_base_ind;
    float prewinder_wav[MAXPTSPERINT];
    float * k0x;
    float * k0y;
    float * k0z;
    int prewinder_res=0;
    float kinit_max=0.0;
    float gmax=w->gmax;
    float gslew = w->gslew;
    float dts=w->dts;
    
    
    /* if the base waveform starts from 0 but one of the interleaves
     * does not, it will be a problem since the base waveform will have
     * a 0 prewinder, and it is impossible to get to that specific 
     * interleaf by multiplying 0 by a number. Thi ssituation happens
     * in trajectories like 3D epi where the base waveform has kz=0,
     * so that I cannot generating the prewinders correctly by just 
     * multiplying. The solution I have for now is to change the base 
     * waveform if necessary. This whole situation arises because the
     * transformation matrices of the prewinder segments are multiplicative
     */
    
    if ((w->gx_res!=0 && fabs(w->kx[0])<EPS_K) ||
        (w->gy_res!=0 && fabs(w->ky[0])<EPS_K) ||
        (w->gz_res!=0 && fabs(w->kz[0])<EPS_K))
    {
        need_to_fix_max=0;
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            need_to_fix=(w->gx_res!=0 && fabs(w->kx[0])<EPS_K && fabs(w->kx_cur[0]) > EPS_K) + 
                        (w->gy_res!=0 && fabs(w->ky[0])<EPS_K && fabs(w->ky_cur[0]) > EPS_K) + 
                        (w->gz_res!=0 && fabs(w->kz[0])<EPS_K && fabs(w->kz_cur[0]) > EPS_K);
            if (need_to_fix > need_to_fix_max)
            {
                need_to_fix_max=need_to_fix;
                best_base_ind=i;
            }
        }
                
        if (need_to_fix_max > 0)
        {
            set_base_ro(w,best_base_ind);
        }
    }
                
    /* seeing which interleaf needs the highest prewinder */
    k0x=(float*)malloc(w->ni*sizeof(float));
    k0y=(float*)malloc(w->ni*sizeof(float));
    k0z=(float*)malloc(w->ni*sizeof(float));
    
    if (w->gx_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            k0x[i]=w->kx_cur[0];
            if (kinit_max < fabs(w->kx_cur[0]))
                kinit_max=fabs(w->kx_cur[0]);
        }
    }
    else
    {
        for (i=0;i<w->ni;i++)
            k0x[i]=0.0;
    }
    
    if (w->gy_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            k0y[i]=w->ky_cur[0];
            if (kinit_max < fabs(w->ky_cur[0]))
                kinit_max=fabs(w->ky_cur[0]);
        }
    }
    else
    {
        for (i=0;i<w->ni;i++)
            k0y[i]=0.0;
    }
    
    if (w->gz_res != 0)
    {
        for (i=0;i<w->ni;i++)
        {
            gen_cur_ro(w,i);
            k0z[i]=w->kz_cur[0];
            if (kinit_max < fabs(w->kz_cur[0]))
                kinit_max=fabs(w->kz_cur[0]);
        }
    }
    else
    {
        for (i=0;i<w->ni;i++)
            k0z[i]=0.0;
    }
    
    
    /* generating the prewinder */
    if (fabs(kinit_max) > EPS_K)
    {
        gen_trap(kinit_max,
                gmax,
                gslew,
                dts,
                prewinder_wav,
                &prewinder_res);
        
        /* prewinder_res=0;
         */
        
        /* prewinder_res=1000;
         * for (j=0;j<prewinder_res;j++)
         * prewinder_wav[j]=(float)j;
         */
        
        /* gen_ramp(0,
         * kinit_max,
         * 0.0,
         * gslew,
         * dts,
         * prewinder_wav,
         * &prewinder_res);
         */
        
        /*area=calc_g_area(prewinder_wav,prewinder_res,dts);*/
    }
    else
    {
        prewinder_res=0;
    }
    
    /* putting the prewinder at the beginning*/
    if (prewinder_res!=0)
    {
        if (w->gx_res !=0)
        {
            for (j=w->gx_res-1;j>=0;j--)
                w->gx[prewinder_res+j]=w->gx[j];
            for (j=0;j<prewinder_res;j++)
                w->gx[j]=prewinder_wav[j]*w->kx[0]/kinit_max;
        }
        if (w->gy_res !=0)
        {
            for (j=w->gy_res-1;j>=0;j--)
                w->gy[prewinder_res+j]=w->gy[j];
            for (j=0;j<prewinder_res;j++)
                w->gy[j]=prewinder_wav[j]*w->ky[0]/kinit_max;
        }
        if (w->gz_res !=0)
        {
            for (j=w->gz_res-1;j>=0;j--)
                w->gz[prewinder_res+j]=w->gz[j];
            for (j=0;j<prewinder_res;j++)
                w->gz[j]=prewinder_wav[j]*w->kz[0]/kinit_max;
        }
    }
    
    
    
    /* adding in a new segment for the 
     * prewinder since the transformation matrix for rewinder
     * is different 
     */
    
    if (prewinder_res!=0)
    {
        /* shifting all the transformation matrices first */
        for (j=w->nseg-1;j>=0;j--)
            memmove(&(w->T[12*w->ni*(j+1)]),
                    &(w->T[12*w->ni*j]),
                    12*w->ni*sizeof(float));
        
        
        /* insert prewinder as the first segment
         * the prewinder changes its amplitude according
         * to the starting k-space location so it can be
         * modeled as a multiplicative term. However, if
         * one of the axis has 0 waveform for the first
         * interleaf and increase for others, this will be
         * a problem. In this case, other methods are needed
         * see gen_turbine for such an example where the z
         * axis starts from 0 so the standard approach
         * cannot be used. However, I should invent a better
         * way to handle this
         *
         * update 2012 08 31 - I think I fixed this above using the
         * set_base_ro function.
         */
        for (j=0;j<w->ni;j++)
        {
            gen_cur_ro(w,j);
            if (k0x[0] !=0.0)
                w->T[12*j+0] = k0x[j]/w->kx[0];
            else
                w->T[12*j+0] = 0.0;
            w->T[12*j+1] = 0.0;
            w->T[12*j+2] = 0.0;
            w->T[12*j+3] = 0.0;
            w->T[12*j+4] = 0.0;
            if (k0y[0]!=0.0)
                w->T[12*j+5] = k0y[j]/w->ky[0];
            else
                w->T[12*j+5] = 0.0;
            w->T[12*j+6] = 0.0;
            w->T[12*j+7] = 0.0;
            w->T[12*j+8] = 0.0;
            w->T[12*j+9] = 0.0;
            if (k0z[0]!=0.0)
                w->T[12*j+10]= k0z[j]/w->kz[0];
            else
                w->T[12*j+10] = 0.0;
            w->T[12*j+11]= 0.0;
        }
        
        /* shifting the segments by one */
        for (j=0;j<w->nseg+1;j++)
            w->seg_nodes[j]+=prewinder_res;
        memmove(&(w->seg_nodes[1]),
                &(w->seg_nodes[0]),
                (w->nseg+1)*sizeof(int));
        w->seg_nodes[0]=0;
        w->nseg++;
        
        /* as for the psd segments, adding the prewinder as the first segment */
        /*       if (w->gx_res!=0) */
        /* 	{ */
        /* 	  for (j=0;j<w->n_psd_seg_gx+1;j++) */
        /* 	    w->psd_segs_gx[j]+=prewinder_res; */
        /* 	  memmove(&(w->psd_segs_gx[1]), */
        /* 		  &(w->psd_segs_gx[0]), */
        /* 		  (w->n_psd_seg_gx+1)*sizeof(int)); */
        /* 	  w->psd_segs_gx[0]=0; */
        /* 	  w->n_psd_seg_gx++; */
        /* 	} */
        /*       if (w->gy_res!=0) */
        /* 	{ */
        /* 	  for (j=0;j<w->n_psd_seg_gy+1;j++) */
        /* 	    w->psd_segs_gy[j]+=prewinder_res; */
        /* 	  memmove(&(w->psd_segs_gy[1]), */
        /* 		  &(w->psd_segs_gy[0]), */
        /* 		  (w->n_psd_seg_gy+1)*sizeof(int)); */
        /* 	  w->psd_segs_gy[0]=0; */
        /* 	  w->n_psd_seg_gy++; */
        /* 	} */
        /*       if (w->gz_res!=0) */
        /* 	{ */
        /* 	  for (j=0;j<w->n_psd_seg_gz+1;j++) */
        /* 	    w->psd_segs_gz[j]+=prewinder_res; */
        /* 	  memmove(&(w->psd_segs_gz[1]), */
        /* 		  &(w->psd_segs_gz[0]), */
        /* 		  (w->n_psd_seg_gz+1)*sizeof(int)); */
        /* 	  w->psd_segs_gz[0]=0; */
        /* 	  w->n_psd_seg_gz++; */
        /* 	} */
        
        
    }
    
    /*write_log("add_ramp_prewinder - 3\n");
    write_log_2(w);*/
    
    
    if (w->gx_res !=0)
    {
        w->gx_res+=+prewinder_res;
        w->gx_ctr+=prewinder_res;
        w->gx_start+=prewinder_res;
        w->gx_end+=prewinder_res;
    }
    if (w->gy_res !=0)
    {
        w->gy_res+=prewinder_res;
        w->gy_ctr+=prewinder_res;
        w->gy_start+=prewinder_res;
        w->gy_end+=prewinder_res;
    }
    if (w->gz_res !=0)
    {
        w->gz_res+=prewinder_res;
        w->gz_ctr+=prewinder_res;
        w->gz_start+=prewinder_res;
        w->gz_end+=prewinder_res;
    }
    
    
    /* calculating k for each */
    gen_k_from_g(w->gx,w->gx_res,w->dts,0.0,w->kx);
    gen_k_from_g(w->gy,w->gy_res,w->dts,0.0,w->ky);
    gen_k_from_g(w->gz,w->gz_res,w->dts,0.0,w->kz);
    
    free(k0x);
    free(k0y);
    free(k0z);
    
    return 1;
    
}

int add_rewinder_to_g(generic_ro * w)
{
    revert_w(w);
    add_prewinder_to_g(w);
    return revert_w(w);
    
};




int add_zeros_before_g(generic_ro * w, int num_zeros)
{
    int j;
    int ramp_res1=0;
    
    if (num_zeros !=0)
    {
        ramp_res1 = num_zeros;
    }
    else
    {
        ramp_res1=0;
        
    }
    
    
    
    /*adding the ramps to the beginning and to the end*/
    if (w->gx_res != 0)
    {
        
        for (j=w->gx_res-1;j>=0;j--)
            w->gx[j+ramp_res1]=w->gx[j];
        
        for (j=0;j<ramp_res1;j++)
            w->gx[j]=0;
        
        w->gx_res+=ramp_res1;
        
        gen_k_from_g(w->gx,w->gx_res,w->dts,w->kx[0],w->kx);
    }
    
    if (w->gy_res != 0)
    {
        
        for (j=w->gy_res-1;j>=0;j--)
            w->gy[j+ramp_res1]=w->gy[j];
        
        for (j=0;j<ramp_res1;j++)
            w->gy[j]=0;
        
        w->gy_res+=ramp_res1;
        
        gen_k_from_g(w->gy,w->gy_res,w->dts,w->ky[0],w->ky);
    }
    
    
    if (w->gz_res != 0)
    {
        
        for (j=w->gz_res-1;j>=0;j--)
            w->gz[j+ramp_res1]=w->gz[j];
        
        for (j=0;j<ramp_res1;j++)
            w->gz[j]=0;
        
        w->gz_res+=ramp_res1;
        
        gen_k_from_g(w->gz,w->gz_res,w->dts,w->kz[0],w->kz);
    }
    
    
    /* adjusting the nodes so that the ramp is part of the initial node */
    for (j=1;j<w->nseg+1;j++)
        w->seg_nodes[j]+=ramp_res1;
    
    
    /* adjusting the psd segments
     * initial ramp is added to the first WF_PULSE
     * final ramp is added to the last WF_PULSE
     */
    /*   if (w->gx_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gx+1;j++) */
    /* 	w->psd_segs_gx[j]+=ramp_res1; */
    /*     } */
    /*   if (w->gy_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gy+1;j++) */
    /* 	w->psd_segs_gy[j]+=ramp_res1; */
    /*     } */
    /*   if (w->gz_res!=0) */
    /*     { */
    /*       for (j=1;j<w->n_psd_seg_gz+1;j++) */
    /* 	w->psd_segs_gz[j]+=ramp_res1; */
    /*     } */
    
    
    
    
    /* adjusting start & end */
    if (w->gx_res !=0)
    {
        w->gx_ctr   += ramp_res1;
        /*   w->gx_start += ramp_res1;*/
        w->gx_end   += ramp_res1;
    }
    if (w->gy_res !=0)
    {
        w->gy_ctr   += ramp_res1;
        /*     w->gy_start += ramp_res1;*/
        w->gy_end   += ramp_res1;
    }
    if (w->gz_res !=0)
    {
        w->gz_ctr   += ramp_res1;
        /*w->gz_start += ramp_res1;*/
        w->gz_end   += ramp_res1;
    }
    
    
    return 1;
    
} /* end of add zeros */



float calc_g_area(float * g,
        int gres,
        float dts,
        float * tempmem)
{
    float temp;
    int dealloc_tempmem=0;
    float * tempmem2;

    if (tempmem == NULL)
    {
        dealloc_tempmem=1;
        tempmem2=(float*)malloc((gres+1)*sizeof(float));
    }
    else
    {
        tempmem2=tempmem;
    }

    gen_k_from_g(g,
            gres,
            dts,
            0.0,
            tempmem2);

    temp=tempmem2[gres];

    if (dealloc_tempmem)
        free(tempmem2);

    return temp;
};


int gen_ramp(float g1, /* G / cm */
        float g2, /* G / cm */
        float tr, /* sec */
        float gslew, /* mT / m / ms */
        float dts, /* sec */
        float *g,
        int * gres,
        float * gslew_used) {
    float tr2;
    int i;
    float damp; /* amplitude difference per dts */
    int npts;
    
    tr2=fabs(g1-g2)/gslew*0.01;

    if (tr2 > tr)
        tr=tr2;

    gslew = fabs(g1-g2)/tr*0.01;
    
    npts=(int)(tr/dts+0.5);
    damp=(g2-g1)/npts;
    
    for (i=0;i<npts;i++)
    {
        g[i]=g1 + damp * i;
    }
    if (i%2) g[i++]=g2;

    /*i=0;
  while (i*dts<tr) {
    g[i]=g1+2.0*((float)(g2>g1)-0.5)*gslew * i * dts * 100.0;
    i++;
    }*/
    
    
    /*!!!!!!!*/
    /*g[i++]=g2;*/
    
    /* making sure that the frame size is even */
    /*  if (i%2) g[i++]=g2;*/
    
    *gres=i;
    
    if (gslew_used !=NULL)
        *gslew_used = gslew;
    
    return *gres;
};



int gen_trap(float kmax,
        float gmax,
        float gslew,
        float dts,
        float * g,
        int * gres)
{
    
    int i;
    float temp;
    float area=0.0; /* rad/mm */
    float gamma = 2.0*M_PI*4.257e3;/* rad * Hz / G */
    float amp; /* amplitude */
    float tr=gmax/gslew/100.0; /* sec */
    /* rise time to the maximum gradient value
     * G / cm / mT * m * ms * 1000 mT / T * 0.0001 T / G * 100 cm / m * 0.001 sec / ms
     */
    float tflat; /*sec */
    /* flat area time */
    
    /* just to be safe */
    /*gmax=0.99*gmax;*/
    
    /* area during rise and decay time */
    area=tr*gmax*gamma*0.1; /* rad / mm */
    /* sec * G / cm *rad * Hz / G * 0.1 cm / mm = 0.1 rad/mm */
    
    
    if (kmax >= area) {
        amp=gmax;
        tflat=(kmax-area)/amp/gamma*10.0;
        /* rad / mm * cm / G / rad / Hz * G * 10 mm / cm = 10 sec */
    }
    
    else if (area > kmax) {

        tflat = dts;
        /* gamma * (0.001*amp/gslew * amp + 0.1*tflat * amp) = kmax
         * 0.001*gamma/gslew * amp^2 + 0.1*tflat*amp*gamma - kmax=0;
         * rad Hz / G * ( G / cm * m * ms / mT * G / cm * 0.1 mT / G * 100 cm / m * 0.001 sec / ms * 0.1 cm/mm  + sec * G / cm * 0.1 cm / mm )
         */
        amp=(-0.1*tflat*gamma+sqrt(0.1*tflat*gamma*0.1*tflat*gamma+4*0.001*gamma/gslew*kmax))/(2*0.001*gamma/gslew);
        tr=amp/gslew*0.01; /* sec*/

    }
    
    i=0;
    area=0.0;
    
    /* initial ramp*/
    gen_ramp(0.0,
            amp,
            tr,
            gslew,
            dts,
            g,
            &i,
            NULL);
    
    /* flat region */
    while (i*dts<tr+tflat)
    {
        g[i]=amp;
        i++;
    }

    /* making sure that the frame size of the flat region is even */
    if (i%2)
        g[i++]=amp;
    
    
    /* ramp down*/
    gen_ramp(amp,
            0.0,
            tr,
            gslew,
            dts,
            &(g[i]),
            gres,
            NULL);
    
    (*gres)=(*gres)+i;
    
    /* final rescaling */
    temp=calc_g_area(g,
            *gres,
            dts,
            NULL);
    
    for (i=0;i<*gres;i++) {
        g[i]=g[i]/temp*kmax;
    }
    
    return 1;
};











/*ADD_NEW_RO_TYPE*/

int shift(generic_ro * w,int n,float d,float start_index,int axis)
{
    
    int i,l,p,k,m,j;
    generic_ro * w2;
    
    if (n==1) return 1;
    
    /* create a copy of this waveform */
    w2=(generic_ro*)malloc(sizeof(generic_ro));
    memcpy(w2,w,sizeof(generic_ro));
    
    
    
    /* after shifting, the number of interleaves will be different */
    w->ni=w2->ni*n;
    
    for (l=0;l<w2->nseg;l++)
    {
        for (p=0;p<n;p++)
        {
            for (i=0;i<w2->ni;i++)
            {
                k=l*w->ni+p*w2->ni+i;
                m=l*w2->ni+i;
                
                /* copy the transformation matrix */
                for (j=0;j<12;j++)
                    w->T[k*12+j] = w2->T[m*12+j];
                
                /* add to the the translation part of the matrix */
                w->T[k*12+axis*4+3]+=(p+start_index)*d;
            }
        }
    }
    
    free(w2);
    
    return 1;
    
}


int shift_ilv(generic_ro * w,int n,float d,int axis){

    int i,l,k,m;
    
    i = n;/*which interleave*/
    
    for (l=0;l<w->nseg;l++)
    {
        /*for (i=0;i<w->ni;i++)*/
            /*{*/
                k=l*w->ni+i;
                m=l*w->ni+i;
                
                /* copy the transformation matrix */
                /*for (j=0;j<12;j++)
                    w->T[k*12+j] = w2->T[m*12+j];*/
                
                /* add to the the translation part of the matrix */
                w->T[k*12+axis*4+3]+=d;
            /*}*/
     
    }

    return 1;
};



int rotate(generic_ro * w,
        int n,
        float d,
        float start_index,
        int axis)
{
 
    int i,l,p,k,m;
    generic_ro * w2;
    float R[12]; /* the rotation matrix */
    float theta;
    
 
    
    if (n==1) return 1;
    
    /* create a copy of this waveform */
    w2=(generic_ro*)malloc(sizeof(generic_ro));
    copy_w(w,w2);
    

    
    /* after rotating, the number of interleaves will be different */
    w->ni=w2->ni*n;
    
    for (l=0;l<w2->nseg;l++)
    {
        for (p=0;p<n;p++)
        {
            for (i=0;i<w2->ni;i++)
            {
                k=l*w->ni+p*w2->ni+i;
                m=l*w2->ni+i;
                
                /* the new transformation matrix is 
                 * T_new = R * T_old
                 */
                
                /* rotation angle */
                theta=(float)(p+start_index)*d;
                
                /* generating the rotation matrix according to the axis of rotation */
                if (axis==0)
                {
                    R[0]=1.0;
                    R[1]=0.0;
                    R[2]=0.0;
                    R[3]=0.0;
                    R[4]=0.0;
                    R[5]=cos(theta/180.0*M_PI);
                    R[6]=sin(theta/180.0*M_PI);
                    R[7]=0.0;
                    R[8]=0.0;
                    R[9]=-sin(theta/180.0*M_PI);
                    R[10]=cos(theta/180.0*M_PI);
                    R[11]=0.0;
                }
                else if (axis==1)
                {
                    R[0]=cos(theta/180.0*M_PI);
                    R[1]=0.0;
                    R[2]=sin(theta/180.0*M_PI);
                    R[3]=0.0;
                    R[4]=0.0;
                    R[5]=1.0;
                    R[6]=0.0;
                    R[7]=0.0;
                    R[8]=-sin(theta/180.0*M_PI);
                    R[9]=0.0;
                    R[10]=cos(theta/180.0*M_PI);
                    R[11]=0.0;                
                }
                if (axis==2)
                {
                    R[0]=cos(theta/180.0*M_PI);
                    R[1]=sin(theta/180.0*M_PI);
                    R[2]=0.0;
                    R[3]=0.0;
                    R[4]=-sin(theta/180.0*M_PI);
                    R[5]=cos(theta/180.0*M_PI);
                    R[6]=0.0;
                    R[7]=0.0;
                    R[8]=0.0;
                    R[9]=0.0;
                    R[10]=1.0;
                    R[11]=0.0;
                }                                                
                
                /* left multiply the transformation matrix */
                multiply(R,&(w2->T[m*12]),&(w->T[k*12]));                                
            }
        }
    }        
    
    free(w2);
    
    return 1;
    
}



void rep_w(generic_ro * w,
          int n)
{
    int i;
    /* copy the transformation matrices */
    for (i=1;i<n;i++)
    {
        memcpy(&(w->T[i*4*3*w->ni*w->nseg]),
                &(w->T[0]),
                4*3*w->ni*w->nseg*sizeof(float));
    }
            
    /* the new number of interleaves */
    w->ni=w->ni*n;          
    

}


void multiply_w(generic_ro * w, 
        float * T, 
        int leafi)
{
    int i,j;    
    
    for (j=0;j<w->nseg;j++)
    {
        i=j*w->ni*12+leafi*12;        
        multiply(T,&(w->T[i]),&(w->T[i]));
    }            
}


int apply_reduction(generic_ro *w)
{
    
    /* R_skip=1, R=3
     * X--X--X--X--....
     *
     * R_skip=2, R=3
     * XX----XX----XX----
     *
     * R_skip is used for doing reduction in the second phase encode direction for
     * 3D acquisitions. e.g., for undersampling in the stack direction in STACKOFEPI
     */
    
    int i,k,l;
    
    generic_ro *w2=(generic_ro*)malloc(sizeof(generic_ro));
    copy_w(w,w2);
    
    w->ni=(int)((float)(w->ni)/(float)(w->R));
    
    
    for (i=0;i<w2->ni/w2->R/w2->R_skip;i++)
    {
        for (k=0;k<w2->R_skip;k++)
        {
            for (l=0;l<w->nseg;l++)
            {
                memcpy(&(w->T[(l*w->ni+i*w2->R_skip+k)*12]),
                        &(w2->T[(l*w2->ni+i*w2->R*w2->R_skip+k)*12]),
                        sizeof(float)*12);
                
            }
        }
    }
    
    free(w2);
    return 1;
}



int fill_gz_with_zeros(generic_ro *w )
{
    /* for 2d acquisitions add zeros to gz and kz */
    
    int i;
    
    /* match the gz to the gx */
    w->gz_res   = w->gx_res;
    w->gz_start = w->gx_start;
    w->gz_end   = w->gx_end;
    w->gz_ctr   = w->gx_ctr;
    
    /*   w->n_psd_seg_gz=w->n_psd_seg_gx; */
    
    
    /*   for (i=0;i<w->n_psd_seg_gx+1;i++) */
    /*   { */
    /*       w->psd_segs_gz[i]=w->psd_segs_gx[i]; */
    
    /*   } */
    
    for (i=0;i<w->gz_res;i++)
    {
        w->gz[i]=0;
        
    }
    
    for (i=0;i<w->gz_res+1;i++)
    {
        
        w->kz[i]=0;
    }
    
    
    return 1;
};



float golden_angle_coef(int idx, int dim, int dim_idx )
{
    float idx_1;
    float idx_2;
    float xx,yy;
    
    /* golden angle coefficients for one and 2D */
    /* ------------------------------------------------------------ */
    switch (dim){
        case 1:
            
            xx = (float)idx * GOLDEN_1D;
            idx_1 = xx-floor(xx);
            
            break;
            
        case 2:
            
            xx = (float)idx * GOLDEN_2Da;
            idx_1 = xx-floor(xx);
            
            yy = (float)idx * GOLDEN_2Db;
            idx_2 = yy-floor(yy);
            break;
            
        default:
            break;
    }
    
    if(dim_idx==1){
        
        return idx_1;
        
    }else if (dim_idx==2){
        
        return idx_2;
        
    }else{
        return -1.0;
    }
    
};



int permute(generic_ro *w)
{
    int a0,a1,a2;
    int i,j;
    float permute=(float)(w->permute);
    float T2[12]; /* the axis permuting transformation matrix */
    float T3[12]; /* temporary storage */
    float T4[12]; /* temporary storage */
    
    a0=(int)(permute/100.0);
    a1=(int)((permute-a0*100.0)/10.0);
    a2=permute-a0*100-a1*10;
    
    
    for (i=0;i<12;i++) T2[i]=0.0;
    if (a0<=5) T2[a0%3]=2.0*((float)(a0<3)-0.5);
    if (a1<=5)  T2[(a1%3)+4]=2.0*((float)(a1<3)-0.5);
    if (a2<=5)  T2[(a2%3)+8]=2.0*((float)(a2<3)-0.5);
    
    for (j=0;j<w->nseg;j++)
    {
        for (i=0;i<w->ni;i++)
        {
            memcpy(T3,&(w->T[j*w->ni*12+i*12]),12*sizeof(float));
            multiply(T2,T3,T4);            
            memcpy(&(w->T[j*w->ni*12+i*12]),T4,12*sizeof(float));
        }
    }
    
    w->permute=12;

	return 1;
}

void multiply(float * T1, float * T2, float * T)
{
    
    float * T3;
    int in_place;
    /* check in-place multiplication */
    if (T==T1 || T==T2)
    {
        in_place=1;
        T3=(float*)malloc(12*sizeof(float));
    }
    else
    {
        in_place=0;
        T3=T;
    }
    
    T3[0] =T1[0]*T2[0] + T1[1]*T2[4] + T1[2]*T2[ 8]        ;
    T3[1] =T1[0]*T2[1] + T1[1]*T2[5] + T1[2]*T2[ 9]        ;
    T3[2] =T1[0]*T2[2] + T1[1]*T2[6] + T1[2]*T2[10]        ;          
    T3[3] =T1[0]*T2[3] + T1[1]*T2[7] + T1[1]*T2[11] + T1[3];
    T3[4] =T1[4]*T2[0] + T1[5]*T2[4] + T1[6]*T2[ 8]        ;
    T3[5] =T1[4]*T2[1] + T1[5]*T2[5] + T1[6]*T2[ 9]        ;
    T3[6] =T1[4]*T2[2] + T1[5]*T2[6] + T1[6]*T2[10]        ;
    T3[7] =T1[4]*T2[3] + T1[5]*T2[7] + T1[6]*T2[11] + T1[7];
    T3[8] =T1[8]*T2[0] + T1[9]*T2[4] + T1[10]*T2[8];          
    T3[9] =T1[8]*T2[1] + T1[9]*T2[5] + T1[10]*T2[9];
    T3[10]=T1[8]*T2[2] + T1[9]*T2[6] + T1[10]*T2[10];
    T3[11]=T1[8]*T2[3] + T1[9]*T2[7] + T1[10]*T2[11] + T1[11];    
    
    if (in_place)
    {
        memcpy(T,T3,12*sizeof(float));
        free(T3);
    }
}




void identity(float * T,int n)
{
    int j;
    for (j=0;j<12;j++)
        T[j + n*12]=0.0;
    
    T[0+n*12]=1.0;T[5+n*12]=1.0;T[10+ n*12]=1.0;    
}

/* http://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix */
int gluInvertMatrix(double m[16], double invOut[16])
{
    double inv[16], det;
    int i;

    inv[0] = m[5]  * m[10] * m[15] - 
             m[5]  * m[11] * m[14] - 
             m[9]  * m[6]  * m[15] + 
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] - 
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] + 
              m[4]  * m[11] * m[14] + 
              m[8]  * m[6]  * m[15] - 
              m[8]  * m[7]  * m[14] - 
              m[12] * m[6]  * m[11] + 
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] - 
             m[4]  * m[11] * m[13] - 
             m[8]  * m[5] * m[15] + 
             m[8]  * m[7] * m[13] + 
             m[12] * m[5] * m[11] - 
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] + 
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] - 
               m[8]  * m[6] * m[13] - 
               m[12] * m[5] * m[10] + 
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] + 
              m[1]  * m[11] * m[14] + 
              m[9]  * m[2] * m[15] - 
              m[9]  * m[3] * m[14] - 
              m[13] * m[2] * m[11] + 
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] - 
             m[0]  * m[11] * m[14] - 
             m[8]  * m[2] * m[15] + 
             m[8]  * m[3] * m[14] + 
             m[12] * m[2] * m[11] - 
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] + 
              m[0]  * m[11] * m[13] + 
              m[8]  * m[1] * m[15] - 
              m[8]  * m[3] * m[13] - 
              m[12] * m[1] * m[11] + 
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] - 
              m[0]  * m[10] * m[13] - 
              m[8]  * m[1] * m[14] + 
              m[8]  * m[2] * m[13] + 
              m[12] * m[1] * m[10] - 
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] - 
             m[1]  * m[7] * m[14] - 
             m[5]  * m[2] * m[15] + 
             m[5]  * m[3] * m[14] + 
             m[13] * m[2] * m[7] - 
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] + 
              m[0]  * m[7] * m[14] + 
              m[4]  * m[2] * m[15] - 
              m[4]  * m[3] * m[14] - 
              m[12] * m[2] * m[7] + 
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] - 
              m[0]  * m[7] * m[13] - 
              m[4]  * m[1] * m[15] + 
              m[4]  * m[3] * m[13] + 
              m[12] * m[1] * m[7] - 
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] + 
               m[0]  * m[6] * m[13] + 
               m[4]  * m[1] * m[14] - 
               m[4]  * m[2] * m[13] - 
               m[12] * m[1] * m[6] + 
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] + 
              m[1] * m[7] * m[10] + 
              m[5] * m[2] * m[11] - 
              m[5] * m[3] * m[10] - 
              m[9] * m[2] * m[7] + 
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] - 
             m[0] * m[7] * m[10] - 
             m[4] * m[2] * m[11] + 
             m[4] * m[3] * m[10] + 
             m[8] * m[2] * m[7] - 
             m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] + 
               m[0] * m[7] * m[9] + 
               m[4] * m[1] * m[11] - 
               m[4] * m[3] * m[9] - 
               m[8] * m[1] * m[7] + 
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] - 
              m[0] * m[6] * m[9] - 
              m[4] * m[1] * m[10] + 
              m[4] * m[2] * m[9] + 
              m[8] * m[1] * m[6] - 
              m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0)
        return 0;

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        invOut[i] = inv[i] * det;

    return 1;
}

void invert(float * T, float * Tinv)
{
    int i;
    double m[16];
    double invOut[16];
    for (i=0;i<12;i++)
    {
        m[i]=(double)T[i];
    }
    m[12]=0.0;
    m[13]=0.0;
    m[14]=0.0;
    m[15]=1.0;
    
    gluInvertMatrix(m,invOut);
    
    for (i=0;i<12;i++)
    {
        Tinv[i]=(float)invOut[i];
    }
    
}




int gen_cur_ro(generic_ro * w,int i)
{
    int j,k;
    float * T;
    /*   write_log("gen_cur_ro >> x waveform\n"); */
    if (w->gx_res!=0)
    {
        for (k=0;k<w->nseg;k++)
        {
            T=&(w->T[k*w->ni*12]);
            for (j=w->seg_nodes[k];j<w->seg_nodes[k+1]+1;j++)
                w->kx_cur[j]=
                        T[12*i+0]*w->kx[j]+
                        T[12*i+1]*w->ky[j]+
                        T[12*i+2]*w->kz[j]+
                        T[12*i+3];
            gen_g_from_k(w->kx_cur,
                    w->gx_res+1,
                    w->dts,
                    w->gx_cur);
        }
    }
    /*   write_log("gen_cur_ro >> y waveform\n"); */
    if (w->gy_res!=0)
    {
        for (k=0;k<w->nseg;k++)
        {
            T=&(w->T[k*w->ni*12]);
            for (j=w->seg_nodes[k];j<w->seg_nodes[k+1]+1;j++)
                w->ky_cur[j]=
                        T[12*i+4]*w->kx[j]+
                        T[12*i+5]*w->ky[j]+
                        T[12*i+6]*w->kz[j]+
                        T[12*i+7];
            gen_g_from_k(w->ky_cur,
                    w->gy_res+1,
                    w->dts,
                    w->gy_cur);
        }
    }
    /*   write_log("gen_cur_ro >> z waveform\n"); */
    if (w->gz_res!=0)
    {
        for (k=0;k<w->nseg;k++)
        {
            T=&(w->T[k*w->ni*12]);
            for (j=w->seg_nodes[k];j<w->seg_nodes[k+1]+1;j++)
                w->kz_cur[j]=
                        T[12*i+8]*w->kx[j]+
                        T[12*i+9]*w->ky[j]+
                        T[12*i+10]*w->kz[j]+
                        T[12*i+11];
            gen_g_from_k(w->kz_cur,
                    w->gz_res+1,
                    w->dts,
                    w->gz_cur);
        }
    }
    /*   write_log("gen_cur_ro >> finished\n"); */
    
    return 1;
}

int set_base_ro(generic_ro *w, int ileaf)
{   
    
    int i,l,j;    
    float T2[4*3*MAXSEG];
    float T2inv[4*3*MAXSEG];    
    float Tnew[4*3];
    
    /* generate the interleaf and set it as teh base waveform*/
    gen_cur_ro(w,ileaf);
    if (w->gx_res!=0)
    {
        memcpy(w->kx,w->kx_cur,(w->gx_res+1)*sizeof(float));
        memcpy(w->gx,w->gx_cur,(w->gx_res)*sizeof(float));
    }
    if (w->gy_res!=0)
    {
        memcpy(w->ky,w->ky_cur,(w->gy_res+1)*sizeof(float));
        memcpy(w->gy,w->gy_cur,(w->gy_res)*sizeof(float));
    }
    if (w->gz_res!=0)
    {
        memcpy(w->kz,w->kz_cur,(w->gz_res+1)*sizeof(float));
        memcpy(w->gz,w->gz_cur,(w->gz_res)*sizeof(float));
    }
    
    /* create a copy of the ileaf th transformation matrix
     * and invert
     */
    for (l=0;l<w->nseg;l++)
    {
        for (j=0;j<12;j++)
        {
            T2[l*12+j]=w->T[l*w->ni*12+ileaf*12+j];
        }
        invert(&(T2[l*12]),&(T2inv[l*12]));
    }
                           
    /* invert all the transformation matrices according
     * to the new waveform
     */
    for (l=0;l<w->nseg;l++)
    {
        for (i=0;i<w->ni;i++)
        {
            multiply(&(T2inv[l*12]),
                    &(w->T[l*w->ni*12+i*12]),
                    Tnew);                                    
            
            for (j=0;j<12;j++)
            {
                w->T[l*w->ni*12+i*12+j] = Tnew[j];
            }                        
        }
    }
    return 1;
    
}

#ifndef COMPILE_FOR_PSD
int gen_all_ro(generic_ro * w)
{
    int i,j;
    
    
    for (i=0;i<w->ni;i++)
    {  /***************/
        gen_cur_ro(w,i);
        if (w->gx_res !=0)
        {
            memmove(&(w->kx_all[i*(w->gx_res+1)]),
                    w->kx_cur,
                    (w->gx_res+1)*sizeof(float));
        }
        if (w->gy_res !=0)
        {
            memmove(&(w->ky_all[i*(w->gy_res+1)]),
                    w->ky_cur,
                    (w->gy_res+1)*sizeof(float));
        }
        if (w->gz_res !=0)
        {
            memmove(&(w->kz_all[i*(w->gz_res+1)]),
                    w->kz_cur,
                    (w->gz_res+1)*sizeof(float));
        }
    }
}
#endif

/*
 *
 * compile this part only in digdug2, mex realted functions here
 *
 */

#ifdef MATLAB_MEX_FILE

#include "mex.h"

mxArray * ro_c_to_matlab(generic_ro * w);
generic_ro * ro_matlab_to_c( const mxArray * w_mx);

mxArray * ro_c_to_matlab(generic_ro * w)
{
    
    int i,j,k,l;
    
    mxArray * w_mx=mxCreateStructMatrix(1,1,generic_ro_nfields,generic_ro_field_names);
    
    mxSetField(w_mx,0,"fovx",mxCreateDoubleScalar((double)w->fovx));
    mxSetField(w_mx,0,"fovy",mxCreateDoubleScalar((double)w->fovy));
    mxSetField(w_mx,0,"fovz",mxCreateDoubleScalar((double)w->fovz));
    mxSetField(w_mx,0,"gx_ctr",mxCreateDoubleScalar((double)w->gx_ctr));
    mxSetField(w_mx,0,"gy_ctr",mxCreateDoubleScalar((double)w->gy_ctr));
    mxSetField(w_mx,0,"gz_ctr",mxCreateDoubleScalar((double)w->gz_ctr));
    mxSetField(w_mx,0,"gx_res",mxCreateDoubleScalar((double)w->gx_res));
    mxSetField(w_mx,0,"gy_res",mxCreateDoubleScalar((double)w->gy_res));
    mxSetField(w_mx,0,"gz_res",mxCreateDoubleScalar((double)w->gz_res));
    mxSetField(w_mx,0,"gx_start",mxCreateDoubleScalar((double)w->gx_start));
    mxSetField(w_mx,0,"gy_start",mxCreateDoubleScalar((double)w->gy_start));
    mxSetField(w_mx,0,"gz_start",mxCreateDoubleScalar((double)w->gz_start));
    mxSetField(w_mx,0,"gx_end",mxCreateDoubleScalar((double)w->gx_end));
    mxSetField(w_mx,0,"gy_end",mxCreateDoubleScalar((double)w->gy_end));
    mxSetField(w_mx,0,"gz_end",mxCreateDoubleScalar((double)w->gz_end));
    mxSetField(w_mx,0,"ro_type",mxCreateDoubleScalar((double)w->ro_type));
    mxSetField(w_mx,0,"ni",mxCreateDoubleScalar((double)w->ni));
    mxSetField(w_mx,0,"nx",mxCreateDoubleScalar((double)w->nx));
    mxSetField(w_mx,0,"ny",mxCreateDoubleScalar((double)w->ny));
    mxSetField(w_mx,0,"nz",mxCreateDoubleScalar((double)w->nz));
    mxSetField(w_mx,0,"dts",mxCreateDoubleScalar((double)w->dts));
    mxSetField(w_mx,0,"dts_adc",mxCreateDoubleScalar((double)w->dts_adc));
    mxSetField(w_mx,0,"fn",mxCreateDoubleScalar((double)w->fn));
    mxSetField(w_mx,0,"fn2",mxCreateDoubleScalar((double)w->fn2));
    mxSetField(w_mx,0,"vdspiral_alpha",mxCreateDoubleScalar((double)w->vdspiral_alpha));
    mxSetField(w_mx,0,"gmax",mxCreateDoubleScalar((double)w->gmax));
    mxSetField(w_mx,0,"gslew",mxCreateDoubleScalar((double)w->gslew));
    mxSetField(w_mx,0,"Tmax",mxCreateDoubleScalar((double)w->Tmax));
    mxSetField(w_mx,0,"epi_blip_res",mxCreateDoubleScalar((double)w->epi_blip_res));
    mxSetField(w_mx,0,"epi_flat_res",mxCreateDoubleScalar((double)w->epi_flat_res));
    mxSetField(w_mx,0,"epi_ramp_res",mxCreateDoubleScalar((double)w->epi_ramp_res));
    mxSetField(w_mx,0,"epi_rampsamp",mxCreateDoubleScalar((double)w->epi_rampsamp));
    mxSetField(w_mx,0,"epi_blip_up_down",mxCreateDoubleScalar((double)w->epi_blip_up_down));
    mxSetField(w_mx,0,"epi_multiband_factor",mxCreateDoubleScalar((double)w->epi_multiband_factor));
    mxSetField(w_mx,0,"epi_multiband_fov",mxCreateDoubleScalar((double)w->epi_multiband_fov));
    mxSetField(w_mx,0,"epi_multiband_R",mxCreateDoubleScalar((double)w->epi_multiband_R));
    mxSetField(w_mx,0,"pepi_nblades",mxCreateDoubleScalar((double)w->pepi_nblades));
    mxSetField(w_mx,0,"spiral_radial_nspokes",mxCreateDoubleScalar((double)w->spiral_radial_nspokes));
    mxSetField(w_mx,0,"turbine_radial_nspokes",mxCreateDoubleScalar((double)w->turbine_radial_nspokes));
    mxSetField(w_mx,0,"rewind_readout",mxCreateDoubleScalar((double)w->rewind_readout));
    mxSetField(w_mx,0,"nseg",mxCreateDoubleScalar((double)w->nseg));
    mxSetField(w_mx,0,"frsize",mxCreateDoubleScalar((double)w->frsize));
    mxSetField(w_mx,0,"golden_angle",mxCreateDoubleScalar((double)w->golden_angle));
    mxSetField(w_mx,0,"reverse_readout",mxCreateDoubleScalar((double)w->reverse_readout));
    mxSetField(w_mx,0,"permute",mxCreateDoubleScalar((double)w->permute));
    mxSetField(w_mx,0,"i_ctr",mxCreateDoubleScalar((double)w->i_ctr));
    mxSetField(w_mx,0,"R",mxCreateDoubleScalar((double)w->R));
    mxSetField(w_mx,0,"R_skip",mxCreateDoubleScalar((double)w->R_skip));
    
    /*ADD_NEW_RO_TYPE*/
    
    int dims[4];

    
    /*gx*/
    double * gx;
    mxArray * gx_mx;
    if (w->gx_res != 0)
    {
        dims[0]=w->gx_res;
        dims[1]=1;
        gx_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        gx=mxGetPr(gx_mx);
        for (j=0;j<w->gx_res;j++)
            gx[j]=(double)w->gx[j];
        mxSetField(w_mx,0,"gx",gx_mx);
    }
    
    /*gy*/
    double * gy;
    mxArray * gy_mx;
    if (w->gy_res != 0)
    {
        dims[0]=w->gy_res;
        dims[1]=1;
        gy_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        gy=mxGetPr(gy_mx);
        for (j=0;j<w->gy_res;j++)
            gy[j]=(double)w->gy[j];
        mxSetField(w_mx,0,"gy",gy_mx);
    }
    
    /*gz*/
    double * gz;
    mxArray * gz_mx;
    if (w->gz_res != 0)
    {
        dims[0]=w->gz_res;
        dims[1]=1;
        gz_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        gz=mxGetPr(gz_mx);
        for (j=0;j<w->gz_res;j++)
            gz[j]=(double)w->gz[j];
        mxSetField(w_mx,0,"gz",gz_mx);
    }
    
    /*kx*/
    double * kx;
    mxArray * kx_mx;
    if (w->gx_res != 0)
    {
        dims[0]=w->gx_res+1;
        dims[1]=1;
        kx_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        kx=mxGetPr(kx_mx);
        for (j=0;j<w->gx_res+1;j++)
            kx[j]=(double)w->kx[j];
        mxSetField(w_mx,0,"kx",kx_mx);
    }
    
    /*ky*/
    double * ky;
    mxArray * ky_mx;
    if (w->gy_res != 0)
    {
        dims[0]=w->gy_res+1;
        dims[1]=1;
        ky_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        ky=mxGetPr(ky_mx);
        for (j=0;j<w->gy_res+1;j++)
            ky[j]=(double)w->ky[j];
        mxSetField(w_mx,0,"ky",ky_mx);
    }
    
    /*kz*/
    double * kz;
    mxArray * kz_mx;
    if (w->gz_res != 0)
    {
        dims[0]=w->gz_res+1;
        dims[1]=1;
        kz_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        kz=mxGetPr(kz_mx);
        for (j=0;j<w->gz_res+1;j++)
            kz[j]=(double)w->kz[j];
        mxSetField(w_mx,0,"kz",kz_mx);
    }
    
    /*kx_all*/
    double * kx_all;
    mxArray * kx_all_mx;
    if (w->gx_res != 0 && w->kx_all)
    {
        dims[0]=w->gx_res+1;
        dims[1]=w->ni;
        kx_all_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        kx_all=mxGetPr(kx_all_mx);
        for (j=0;j<(w->gx_res+1)*w->ni;j++)
            kx_all[j]=(double)w->kx_all[j];
        mxSetField(w_mx,0,"kx_all",kx_all_mx);
    }
    
    /*ky_all*/
    double * ky_all;
    mxArray * ky_all_mx;
    if (w->gy_res != 0 && w->ky_all)
    {
        dims[0]=w->gy_res+1;
        dims[1]=w->ni;
        ky_all_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        ky_all=mxGetPr(ky_all_mx);
        for (j=0;j<(w->gy_res+1)*w->ni;j++)
            ky_all[j]=(double)w->ky_all[j];
        mxSetField(w_mx,0,"ky_all",ky_all_mx);
    }
    
    /*kz_all*/
    double * kz_all;
    mxArray * kz_all_mx;
    if (w->gz_res != 0 && w->kz_all)
    {
        dims[0]=w->gz_res+1;
        dims[1]=w->ni;
        kz_all_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        kz_all=mxGetPr(kz_all_mx);
        for (j=0;j<(w->gz_res+1)*w->ni;j++)
            kz_all[j]=(double)w->kz_all[j];
        mxSetField(w_mx,0,"kz_all",kz_all_mx);
    }
    
    /*T*/
    double * T;
    mxArray * T_mx;
    if (w->T)
    {
        dims[0]=3;
        dims[1]=4;
        dims[2]=w->ni;
        dims[3]=w->nseg;
        T_mx=mxCreateNumericArray(4, dims, mxDOUBLE_CLASS, mxREAL);
        T=mxGetPr(T_mx);
        for (l=0;l<w->nseg;l++)
        {
            for (i=0;i<w->ni;i++)
            {
                for (j=0;j<3;j++)
                {
                    for (k=0;k<4;k++)
                    {
                        /*!!!!! something strange going on here !! */
                        T[l*(w->ni)*12+i*12+k*3+j]=(double)w->T[l*(w->ni)*12+i*12+j*4+k];
                    }
                }
            }
        }
        mxSetField(w_mx,0,"T",T_mx);
    }
    
    /*seg_nodes*/
    double * seg_nodes;
    mxArray * seg_nodes_mx;
    if (w->seg_nodes)
    {
        dims[0]=w->nseg+1;
        dims[1]=1;
        seg_nodes_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        seg_nodes=mxGetPr(seg_nodes_mx);
        for (l=0;l<w->nseg+1;l++)
        {
            seg_nodes[l]=(double)w->seg_nodes[l];
        }
        mxSetField(w_mx,0,"seg_nodes",seg_nodes_mx);
    }
    
    /* psd segment nodes,gx */
    /*     double * psd_segs_gx; */
    /*     mxArray * psd_segs_gx_mx; */
    /*     if (w->gx_res!=0) */
    /*       { */
    /* 	dims[0]=w->n_psd_seg_gx+1; */
    /* 	dims[1]=1; */
    /* 	psd_segs_gx_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL); */
    /* 	psd_segs_gx=mxGetPr(psd_segs_gx_mx); */
    /* 	for (l=0;l<w->n_psd_seg_gx+1;l++) */
    /* 	  { */
    /* 	    psd_segs_gx[l]=(double)w->psd_segs_gx[l]; */
    /* 	  } */
    /* 	mxSetField(w_mx,0,"psd_segs_gx",psd_segs_gx_mx); */
    /*       } */
    
    /*     /\* psd segment nodes,gy *\/ */
    /*     double * psd_segs_gy; */
    /*     mxArray * psd_segs_gy_mx; */
    /*     if (w->gy_res!=0) */
    /*       { */
    /* 	dims[0]=w->n_psd_seg_gy+1; */
    /* 	dims[1]=1; */
    /* 	psd_segs_gy_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL); */
    /* 	psd_segs_gy=mxGetPr(psd_segs_gy_mx); */
    /* 	for (l=0;l<w->n_psd_seg_gy+1;l++) */
    /* 	  { */
    /* 	    psd_segs_gy[l]=(double)w->psd_segs_gy[l]; */
    /* 	  } */
    /* 	mxSetField(w_mx,0,"psd_segs_gy",psd_segs_gy_mx); */
    /*       } */
    
    /*     /\* psd segment nodes,gz *\/ */
    /*     double * psd_segs_gz; */
    /*     mxArray * psd_segs_gz_mx; */
    /*     if (w->gz_res!=0) */
    /*       { */
    /* 	dims[0]=w->n_psd_seg_gz+1; */
    /* 	dims[1]=1; */
    /* 	psd_segs_gz_mx=mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL); */
    /* 	psd_segs_gz=mxGetPr(psd_segs_gz_mx); */
    /* 	for (l=0;l<w->n_psd_seg_gz+1;l++) */
    /* 	  { */
    /* 	    psd_segs_gz[l]=(double)w->psd_segs_gz[l]; */
    /* 	  } */
    /* 	mxSetField(w_mx,0,"psd_segs_gz",psd_segs_gz_mx); */
    /*       } */
    
    
    return w_mx;
    
    
};

generic_ro * ro_matlab_to_c( const mxArray * w_mx)
{
    int i,j,k,l;
    
    generic_ro * w=(generic_ro*)malloc(sizeof(generic_ro));
    mxArray * temp;
    init_ro(w);
    
    temp=mxGetField(w_mx,0,"fovx");
    if (temp!=NULL) w->fovx=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"fovy");
    if (temp!=NULL) w->fovy=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"fovz");
    if (temp!=NULL) w->fovz=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gx_ctr");
    if (temp!=NULL) w->gx_ctr=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gy_ctr");
    if (temp!=NULL) w->gy_ctr=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gz_ctr");
    if (temp!=NULL) w->gz_ctr=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gx_res");
    if (temp!=NULL) w->gx_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gy_res");
    if (temp!=NULL) w->gy_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gz_res");
    if (temp!=NULL) w->gz_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gx_start");
    if (temp!=NULL) w->gx_start=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gy_start");
    if (temp!=NULL) w->gy_start=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gz_start");
    if (temp!=NULL) w->gz_start=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gx_end");
    if (temp!=NULL) w->gx_end=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gy_end");
    if (temp!=NULL) w->gy_end=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gz_end");
    if (temp!=NULL) w->gz_end=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"ro_type");
    if (temp!=NULL) w->ro_type=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"ni");
    if (temp!=NULL) w->ni=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"nx");
    if (temp!=NULL) w->nx=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"ny");
    if (temp!=NULL) w->ny=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"nz");
    if (temp!=NULL) w->nz=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"dts");
    if (temp!=NULL) w->dts=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"dts_adc");
    if (temp!=NULL) w->dts_adc=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"fn");
    if (temp!=NULL) w->fn=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"fn2");
    if (temp!=NULL) w->fn2=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"vdspiral_alpha");
    if (temp!=NULL) w->vdspiral_alpha=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gmax");
    if (temp!=NULL) w->gmax=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"gslew");
    if (temp!=NULL) w->gslew=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"Tmax");
    if (temp!=NULL) w->Tmax=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_blip_res");
    if (temp!=NULL) w->epi_blip_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_flat_res");
    if (temp!=NULL) w->epi_flat_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_ramp_res");
    if (temp!=NULL) w->epi_ramp_res=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_rampsamp");
    if (temp!=NULL) w->epi_rampsamp=(int)(mxGetScalar(temp));    
    temp=mxGetField(w_mx,0,"epi_blip_up_down");
    if (temp!=NULL) w->epi_blip_up_down=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_multiband_factor");
    if (temp!=NULL) w->epi_multiband_factor=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_multiband_fov");
    if (temp!=NULL) w->epi_multiband_fov=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"epi_multiband_R");
    if (temp!=NULL) w->epi_multiband_R=(float)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"pepi_nblades");
    if (temp!=NULL) w->pepi_nblades=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"spiral_radial_nspokes");
    if (temp!=NULL) w->spiral_radial_nspokes=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"turbine_radial_nspokes");
    if (temp!=NULL) w->turbine_radial_nspokes=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"rewind_readout");
    if (temp!=NULL) w->rewind_readout=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"nseg");
    if (temp!=NULL) w->nseg=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"frsize");
    if (temp!=NULL) w->frsize=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"golden_angle");
    if (temp!=NULL) w->golden_angle=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"reverse_readout");
    if (temp!=NULL) w->reverse_readout=(int)(mxGetScalar(temp));
    /*temp=mxGetField(w_mx,0,"turnoff_readout");
     * if (temp!=NULL) w->turnoff_readout=(int)(mxGetScalar(temp));*/
    temp=mxGetField(w_mx,0,"permute");
    if (temp!=NULL) w->permute=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"R");
    if (temp!=NULL) w->R=(int)(mxGetScalar(temp));
    temp=mxGetField(w_mx,0,"R_skip");
    if (temp!=NULL) w->R_skip=(int)(mxGetScalar(temp));
    
    /*ADD_NEW_RO_TYPE*/
    
    
    /*gx*/
    double * gx;
    mxArray * gx_mx;
    if (w->gx_res != 0 && w->gx)
    {
        gx_mx=mxGetField(w_mx,0,"gx");
        gx=mxGetPr(gx_mx);
        for (j=0;j<w->gx_res;j++)
            w->gx[j]=(float)gx[j];
    }
    
    /*gy*/
    double * gy;
    mxArray * gy_mx;
    if (w->gy_res != 0 && w->gy)
    {
        gy_mx=mxGetField(w_mx,0,"gy");
        gy=mxGetPr(gy_mx);
        for (j=0;j<w->gy_res;j++)
            w->gy[j]=(float)gy[j];
    }
    
    /*gz*/
    double * gz;
    mxArray * gz_mx;
    if (w->gz_res != 0 && w->gz)
    {
        gz_mx=mxGetField(w_mx,0,"gz");
        gz=mxGetPr(gz_mx);
        for (j=0;j<w->gz_res;j++)
            w->gz[j]=(float)gz[j];
    }
    
    /*kx*/
    double * kx;
    mxArray * kx_mx;
    if (w->gx_res != 0 && w->kx)
    {
        kx_mx=mxGetField(w_mx,0,"kx");
        kx=mxGetPr(kx_mx);
        for (j=0;j<w->gx_res+1;j++)
            w->kx[j]=(float)kx[j];
    }
    
    /*ky*/
    double * ky;
    mxArray * ky_mx;
    if (w->gy_res != 0 && w->gy)
    {
        ky_mx=mxGetField(w_mx,0,"ky");
        ky=mxGetPr(ky_mx);
        for (j=0;j<w->gy_res+1;j++)
            w->ky[j]=(float)ky[j];
    }
    
    /*kz*/
    double * kz;
    mxArray * kz_mx;
    if (w->gz_res != 0 && w->gz)
    {
        kz_mx=mxGetField(w_mx,0,"kz");
        kz=mxGetPr(kz_mx);
        for (j=0;j<w->gz_res+1;j++)
            w->kz[j]=(float)kz[j];
    }
    
    /*kx_all*/
    double * kx_all;
    mxArray * kx_all_mx;
    if (w->gx_res != 0 && w->kx_all)
    {
        kx_all_mx=mxGetField(w_mx,0,"kx_all");
        kx_all=mxGetPr(kx_all_mx);
        for (j=0;j<(w->gx_res+1)*w->ni;j++)
            w->kx_all[j]=(float)kx_all[j];
    }
    
    /*ky_all*/
    double * ky_all;
    mxArray * ky_all_mx;
    if (w->gy_res != 0 && w->ky_all)
    {
        ky_all_mx=mxGetField(w_mx,0,"ky_all");
        ky_all=mxGetPr(ky_all_mx);
        for (j=0;j<(w->gy_res+1)*w->ni;j++)
            w->ky_all[j]=(float)ky_all[j];
    }
    
    /*kz_all*/
    double * kz_all;
    mxArray * kz_all_mx;
    if (w->gz_res != 0 && w->kz_all)
    {
        kz_all_mx=mxGetField(w_mx,0,"kz_all");
        kz_all=mxGetPr(kz_all_mx);
        for (j=0;j<(w->gz_res+1)*w->ni;j++)
            w->kz_all[j]=(float)kz_all[j];
    }
    
    
    /*T*/
    double * T;
    mxArray * T_mx;
    T_mx=mxGetField(w_mx,0,"T");
    T=mxGetPr(T_mx);
    if (T)
    {
        for (l=0;l<w->nseg;l++)
        {
            for (i=0;i<w->ni;i++)
            {
                for (j=0;j<3;j++)
                {
                    for (k=0;k<4;k++)
                    {
                        /*!!!!! something strange gpoing on here !! */
                        w->T[l*w->ni*12+i*12+4*j+k]=(float)T[l*w->ni*12+i*12+j+3*k];
                    }
                }
            }
        }
    }
    
    /*seg_nodes*/
    double * seg_nodes;
    mxArray * seg_nodes_mx;
    seg_nodes_mx=mxGetField(w_mx,0,"seg_nodes");
    seg_nodes=mxGetPr(seg_nodes_mx);
    for (l=0;l<w->nseg+1;l++)
    {
        w->seg_nodes[l]=(float)seg_nodes[l];
    }
    
    
    
    return w;
    
}

#endif



/*
 *
 * scanner related stuff, copied these to generic.e, do not need them here
 *
 */

/*BZ add another layer between the main sequence call of the trajectory and the actual function that generates the base waveform*/
#include "gen_base_ro.c"
