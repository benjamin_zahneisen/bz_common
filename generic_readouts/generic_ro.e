/*
 *  exc_sbb.e
 *
 *  This file contains all the external functions
 *  defined in exc_sbb.h
 *  
 *  Language : ANSI C
 *  Author   : Murat Aksoy & Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include "./generic_readouts/generic_ro.h"


typedef struct generic_ro_psd
{

        WF_PULSE gxw;
        WF_PULSE gxw_buf1;
        WF_PULSE gxw_buf2;
        WF_PULSE gyw;
        WF_PULSE gyw_buf1;
        WF_PULSE gyw_buf2;
        WF_PULSE gzw;
        WF_PULSE gzw_buf1;
        WF_PULSE gzw_buf2;
        WF_PULSE echo;
        WF_PULSE theta;
        int filter_num;
#ifdef SIM
#ifdef IPG
        WF_PULSE echo_sim;
#endif
#endif

        float a_gxw;
        float a_gyw;
        float a_gzw;
        int ia_gxw;
        int ia_gyw;
        int ia_gzw;
        int pw_gxw;
        int pw_gyw;
        int pw_gzw;
	int ia_theta;
        int counter; /* counter for double buffering, even or odd */
} generic_ro_psd;

short Gx[MAXPTSPERINT];
short Gy[MAXPTSPERINT];
short Gz[MAXPTSPERINT];


/**********************************************/
@host _ipgexport



/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host



/*************************************************/
@pg _pg





/***********************************/
@rsp _rsp
void init_wf_pulse( WF_PULSE * p )
{
    p->pulsename = 0;
    p->ninsts = 0;
    p->wave_addr = -1;
    p->board_type = PSDCERD;
    p->reusep = PSDREUSEP;
    p->tag = SSPUNKN;
    p->addtag = 0;
    p->id = 0;
    p->ctrlfield = 0;
    p->inst_hdr_tail = 0;
    p->inst_hdr_head = 0;
    p->wavegen_type = 0;
    p->type = 0;
    p->resolution = 0;
    p->assoc_pulse = 0;
    p->ext = 0;
}
;

void load_ro( generic_ro * w,
              generic_ro_psd * w_psd,
              int i_next )
{

    float target;
    int j;

    if( debugstate )
    {
        fprintf(stderr, "load_ro>> entering load_ro\n");
        fprintf(stderr, "\tgen_cur_ro\n");
        fflush(stderr);
    }
    gen_cur_ro(w, i_next);

    if( debugstate )
    {
        fprintf(stderr, "\tcreating short waveforms\n");
        fflush(stderr);
    }
    /* creating short waveforms*/
    if( w->gx_res != 0 )
    {
        target = loggrd.tx;
        for( j = 0; j < w->gx_res; j++ )
        {
            Gx[j] = (2 * (short)(w->gx_cur[j] / target * (float)MAX_PG_WAMP / 2.0 + 0.5)) & ~WEOS_BIT;
        }
    }

    if( w->gy_res != 0 )
    {
        target = loggrd.ty;
        for( j = 0; j < w->gy_res; j++ )
        {
            Gy[j] = (2 * (short)(w->gy_cur[j] / target * (float)MAX_PG_WAMP / 2.0 + 0.5)) & ~WEOS_BIT;
        }
    }

    if( w->gz_res != 0 )
    {
        target = loggrd.tz;
        for( j = 0; j < w->gz_res; j++ )
        {
            Gz[j] = (2 * (short)(w->gz_cur[j] / target * (float)MAX_PG_WAMP / 2.0 + 0.5)) & ~WEOS_BIT;
        }
    }

    if( debugstate )
    {
        fprintf(stderr, "\tdouble buffering and moving into memory\n");
        fflush(stderr);
    }
    /* double buffering */
    if( w_psd->counter % 2 == 0 )
    {
        if( w->gx_res != 0 )
        {

            Gx[w->gx_res - 1] |= WEOS_BIT;
            movewaveimm(Gx, &(w_psd->gxw_buf1), 0, w->gx_res, TOHARDWARE);
            setwave(w_psd->gxw_buf1.wave_addr, &(w_psd->gxw), 0);
        }
        if( w->gy_res != 0 )
        {

            Gy[w->gy_res - 1] |= WEOS_BIT;
            movewaveimm(Gy, &(w_psd->gyw_buf1), 0, w->gy_res, TOHARDWARE);
            setwave(w_psd->gyw_buf1.wave_addr, &(w_psd->gyw), 0);

        }
        if( w->gz_res != 0 )
        {

            Gz[w->gz_res - 1] |= WEOS_BIT;
            movewaveimm(Gz, &(w_psd->gzw_buf1), 0, w->gz_res, TOHARDWARE);
            setwave(w_psd->gzw_buf1.wave_addr, &(w_psd->gzw), 0);

        }
    }
    else
    {
        if( w->gx_res != 0 )
        {
            Gx[w->gx_res - 1] |= WEOS_BIT;
            movewaveimm(Gx, &(w_psd->gxw_buf2), 0, w->gx_res, TOHARDWARE);
            setwave(w_psd->gxw_buf2.wave_addr, &(w_psd->gxw), 0);

        }
        if( w->gy_res != 0 )
        {

            Gy[w->gy_res - 1] |= WEOS_BIT;
            movewaveimm(Gy, &(w_psd->gyw_buf2), 0, w->gy_res, TOHARDWARE);
            setwave(w_psd->gyw_buf2.wave_addr, &(w_psd->gyw), 0);

        }
        if( w->gz_res != 0 )
        {

            Gz[w->gz_res - 1] |= WEOS_BIT;
            movewaveimm(Gz, &(w_psd->gzw_buf2), 0, w->gz_res, TOHARDWARE);
            setwave(w_psd->gzw_buf2.wave_addr, &(w_psd->gzw), 0);
        }
    }
    if( debugstate )
    {
        fprintf(stderr, "\tdone\n");
        fflush(stderr);
    }
    w_psd->counter++;
}
;

void gen_ro_psd_2( generic_ro * w,
                   generic_ro_psd * w_psd,
                   char * suffix,
                   int starttime,
                   int filternum )
{

    float target;
    char * txt;
    char * txt2;
    int j;
    short * ts;
    float rdx;
    float x; 

    txt = (char*)AllocNode(100 * sizeof(char));
    txt2 = (char*)AllocNode(100 * sizeof(char));

    /* generating the readout gradients */
    if( w->gx_res != 0 )
    {
        target = loggrd.tx;

        w_psd->a_gxw = target;
        w_psd->ia_gxw = w_psd->a_gxw / target * MAX_PG_IAMP;
        w_psd->pw_gxw = (int)(w->gx_res * 4);

        init_wf_pulse(&(w_psd->gxw));
        sprintf(txt, "gxw");
        strcat(txt, suffix);

        pulsename(&(w_psd->gxw), txt);
        createreserve(&(w_psd->gxw), XGRAD, w->gx_res);
        createinstr(&(w_psd->gxw), RUP_GRD(starttime), w->gx_res * 4, max_pg_iamp);

        init_wf_pulse(&(w_psd->gxw_buf1));
        sprintf(txt, "gxw_buf1");
        strcat(txt, suffix);
        pulsename(&(w_psd->gxw_buf1), txt);
        createreserve(&(w_psd->gxw_buf1), XGRAD, w->gx_res);

        init_wf_pulse(&(w_psd->gxw_buf2));
        sprintf(txt, "gxw_buf2");
        strcat(txt, suffix);
        pulsename(&(w_psd->gxw_buf2), txt);
        createreserve(&(w_psd->gxw_buf2), XGRAD, w->gx_res);

    }

    if( w->gy_res != 0 )
    {
        target = loggrd.ty;

        w_psd->a_gyw = target;
        w_psd->ia_gyw = w_psd->a_gyw / target * MAX_PG_IAMP;
        w_psd->pw_gyw = (int)(w->gy_res * 4);

        init_wf_pulse(&(w_psd->gyw));
        sprintf(txt, "gyw");
        strcat(txt, suffix);
        pulsename(&(w_psd->gyw), txt);
        createreserve(&(w_psd->gyw), YGRAD, w->gy_res);
        createinstr(&(w_psd->gyw), RUP_GRD(starttime), w->gy_res * 4, max_pg_iamp);

        init_wf_pulse(&(w_psd->gyw_buf1));
        sprintf(txt, "gyw_buf1");
        strcat(txt, suffix);
        pulsename(&(w_psd->gyw_buf1), txt);
        createreserve(&(w_psd->gyw_buf1), YGRAD, w->gy_res);

        init_wf_pulse(&(w_psd->gyw_buf2));
        sprintf(txt, "gyw_buf2");
        strcat(txt, suffix);
        pulsename(&(w_psd->gyw_buf2), txt);
        createreserve(&(w_psd->gyw_buf2), YGRAD, w->gy_res);
    }

    if( w->gz_res != 0 )
    {
        target = loggrd.tz;

        w_psd->a_gzw = target;
        w_psd->ia_gzw = w_psd->a_gzw / target * MAX_PG_IAMP;
        w_psd->pw_gzw = (int)(w->gz_res * 4);

        init_wf_pulse(&(w_psd->gzw));
        sprintf(txt, "gzw");
        strcat(txt, suffix);
        pulsename(&(w_psd->gzw), txt);
        createreserve(&(w_psd->gzw), ZGRAD, w->gz_res);
        createinstr(&(w_psd->gzw), RUP_GRD(starttime), w->gz_res * 4, max_pg_iamp);

        init_wf_pulse(&(w_psd->gzw_buf1));
        sprintf(txt, "gzw_buf1");
        strcat(txt, suffix);
        pulsename(&(w_psd->gzw_buf1), txt);
        createreserve(&(w_psd->gzw_buf1), ZGRAD, w->gz_res);

        init_wf_pulse(&(w_psd->gzw_buf2));
        sprintf(txt, "gzw_buf2");
        strcat(txt, suffix);
        pulsename(&(w_psd->gzw_buf2), txt);
        createreserve(&(w_psd->gzw_buf2), ZGRAD, w->gz_res);
    }

    /* load initial trajectory */
    w_psd->counter = 0;
    load_ro(w, w_psd, w->i_ctr);

    /* readout (ADC) excludes the dephaser part of w.gx */
    init_wf_pulse(&(w_psd->echo));
    sprintf(txt, "echo");
    strcat(txt, suffix);
    pulsename(&(w_psd->echo), txt);
    acqq(&(w_psd->echo), (long)(starttime + GRAD_UPDATE_TIME * w->gx_start + daqdel), DEFAULTPOS, DEFAULTPOS, filternum, DABNORM);


    /* Fov offset through THETA wave (from G.Glover)  */
    ts = AllocNode(w->frsize*sizeof(short));
    /*thetrecintl = (WF_PULSE *) AllocNode(nl*sizeof(WF_PULSE)); /*no need for pointers beacuse all kx gradients for interleaves are identical */

    init_wf_pulse(&(w_psd->theta));
    sprintf(txt, "theta");
    strcat(txt, suffix);
    pulsename(&w_psd->theta,txt);
    createreserve(&w_psd->theta, THETA, w->frsize);
    createinstr(&w_psd->theta, (long)(starttime + GRAD_UPDATE_TIME * w->gx_start + daqdel), w->frsize*GRAD_UPDATE_TIME, max_pg_iamp);

    rdx = rsp_info[0].rsprloc; /*shift of slice package along readout*/
    target = loggrd.tx;
    /*fprintf(stderr,"targetx = %f \n",target);*/
    movewaveimm(Gx, &(w_psd->gxw), 0, w->gx_res, FROMHARDWARE); /*get instruction wave for readout gradient (waveform scaling reflects w.gmax)*/
    /*x = 2.*FS_PI*w->gmax*GAM*GRAD_UPDATE_TIME*1e-6/(10.0*max_pg_wamp); original*/
    x = 2.*FS_PI*target*GAM*GRAD_UPDATE_TIME*1e-6/(10.0*max_pg_wamp); /*Gx is already scaled */
    /* The demodulation waveform necessary for readout (inegrating over Gx) */
    ts[0] = 0;
    for (j = 1; j < w->frsize; j++)  {
          ts[j] = (short) (ts[j-1] + x*Gx[j+w->gx_start]*rdx) & ~WEOS_BIT;
        }
    ts[w->frsize - 1] |= WEOS_BIT;
    movewaveimm(ts, &(w_psd->theta), (int) 0, w->frsize, TOHARDWARE);
    FreeNode(ts);


#ifdef SIM
#ifdef IPG
    init_wf_pulse(&(w_psd->echo_sim));
    sprintf(txt,"echo_sim");
    strcat(txt, suffix);
    sprintf(txt2,"%d",0); /*echo number */
    strcat(txt, txt2);
    pulsename(&(w_psd->echo_sim),txt);
    /*createconst(&(w_psd->echo_sim),
        OMEGA,
        RUP_GRD(w->frsize*4),
        MAX_PG_WAMP);
    fprintf(stderr,"w->frsize %i \n",w->frsize*4);
    createinstr(&(w_psd->echo_sim),
        RUP_GRD(starttime+4*w->gx_start+daqdel),
        RUP_GRD(w->frsize*4),
        MAX_PG_IAMP);*/
#endif
#endif

    if( debugstate )
        write_log("gen_ro_psd >> finished\n");

    FreeNode(txt);
    FreeNode(txt2);

}
;
