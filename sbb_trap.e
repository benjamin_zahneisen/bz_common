/*
 *  sbb_trap.e
 *
 *  This file contains all the external functions
 *  defined in sbb_trap.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include "sbb_trap.h"


/********************************************************/
/* use local ipgexport definitions in sequence          */
/********************************************************/
@host _ipgexport


/********************************************************/
/* use local ipgexport definitions in sequence          */
/********************************************************/
@cv _cv


/********************************************************/
/* use local ipgexport definitions in sequence          */
/********************************************************/
@host _host


STATUS sbb_trap_init(sbb_trap* psbb){

    /*control variables */
    psbb->ia_flat       = 0;
    psbb->ia_attack     = 0;
    psbb->ia_decay      = 0;
    psbb->a_flat        = 0.0;
    psbb->a_attack      = 0.0;
    psbb->a_decay       = 0.0;
    psbb->momentum      = 0; /*gradient moment [G/cm s]*/

    /*SAR */
    /*int GRAD_SLOT;*/ /*slot in rfpulse[] structure*/
    psbb->axis = ZGRAD; /*GRADX, GRADY, GRADZ */
    psbb->type = TRAP_ALL; /*TRAP_ALL, TRAP_ALL_SLOPED*/
    psbb->Ninstances = 0;;

    /*timing*/
    int start_time; /*all other timing is relative to the sbb start time */
    int total_time; /*duration of sbb */
    int psd_grd_wait;
    int pw_attack;
    int pw_decay;
    int pw_flat;

    initWF_PULSE(&psbb->wf_attack);
    initWF_PULSE(&psbb->wf_decay);
    initWF_PULSE(&psbb->wf_flat);

    return SUCCESS;
};

/*helpfer function to initialize WF_PULSE*/
#ifndef initWF_PULSE_h
#define initWF_PULSE_h
void initWF_PULSE(WF_PULSE* pwf){
    pwf->pulsename = "<uninitialized>";
    pwf->ninsts = 0;
    pwf->wave_addr = -1;
    pwf->board_type = PSDCERD;
    pwf->reusep =  PSDREUSEP;
    pwf->tag =  SSPUNKN;
    pwf->addtag =  0;
    pwf->id =  0;
    pwf->ctrlfield =  0;
    pwf->inst_hdr_tail =  NULL;
    pwf->inst_hdr_head =  NULL;
    pwf->wavegen_type =TYPXGRAD;
    pwf->type =  TYPBITS;
    pwf->resolution = 0;
    pwf->assoc_pulse = NULL;
    pwf->ext =  NULL;
};
#endif

STATUS sbb_trap_cveval(sbb_trap* psbb,float momentum){

    /* slice select dephaser & rephaser instruction amplitudes.
     amppwgrad() calculates fastest trapezoid for desired area. */
    float target;
    int rtime,ftime;
    gettarget(&target, ZGRAD, &loggrd);
    getramptime(&rtime, &ftime, ZGRAD, &loggrd);


    if( amppwgrad(momentum, target, 0.0, 0.0, rtime, MIN_PLATEAU_TIME, &psbb->a_flat, &psbb->pw_attack, &psbb->pw_flat,
                  &psbb->pw_decay)
        == FAILURE)
        return FAILURE;
    psbb->ia_flat = (psbb->a_flat / target) * MAX_PG_IAMP;

    /*set timing of sbb */
    psbb->total_time =  psbb->pw_attack
                        + psbb->pw_flat
                        + psbb->pw_decay;

    return SUCCESS;
};


@pg _pg

STATUS sbb_trap_pulsegen(sbb_trap* psbb,CHAR *sbb_name){

    fprintf(stderr, "calling sbb_trap_pulsegen(). \n" );

    int ia_start = 0;
    int ia_end = 0;

    trapezoid(psbb->axis,sbb_name, &psbb->wf_flat, &psbb->wf_attack, &psbb->wf_decay,
              psbb->pw_flat, psbb->pw_attack, psbb->pw_decay, psbb->ia_flat,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    return SUCCESS;
};



@rsp _rsp
STATUS sbb_trap_psdinit(sbb_trap* psbb){

    setiamp(psbb->ia_flat, &psbb->wf_flat,0);

    return SUCCESS;
};

STATUS sbb_trap_setAmplitude(sbb_trap* psbb,int amplitude,long index){

    if(psbb->type == TRAP_ALL){
        setiamp(amplitude, &psbb->wf_attack,index);
        setiamp(amplitude, &psbb->wf_flat,index);
        setiamp(amplitude, &psbb->wf_decay,index);
    }

    return SUCCESS;
}
