/*
 *  sbb_diff_grad.e = plays out a gradient pulse on all three axis
 *
 *  This file contains all the external functions
 *  defined in sbb_diff_grad.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2016
 *
 */

/**********************************************/
@global _global
#include "sbb_spoil_grad.h"




/**********************************************/
@host _ipgexport


/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
#include "sbb_spoil_grad.h"

void sbb_spoil_grad_init(sbb_spoil_grad* p){
	
	initWF_PULSE(&p->gxa);
	initWF_PULSE(&p->gx);
	initWF_PULSE(&p->gxd);

	initWF_PULSE(&p->gya);
	initWF_PULSE(&p->gy);
	initWF_PULSE(&p->gyd);

	initWF_PULSE(&p->gza);
	initWF_PULSE(&p->gz);
	initWF_PULSE(&p->gzd);

	/*initWF_PULSE(&p->blank_rho);
	initWF_PULSE(&p->blank_theta);
	initWF_PULSE(&p->blank_omega);
	initWF_PULSE(&p->blank_ssp);*/
    
	p->start_time = 0;
	p->Ninstances = 0;
	p->use_maxloggrad=1;
	p->debug = 0;


};



STATUS sbb_spoil_grad_cveval(sbb_spoil_grad* psbb,float revolutions)
{
    fprintf(stderr, "\n welcome to sbb_diff_grad_cveval()\n");


    float target;
    int rtime,ftime;
    gettarget(&target,(WF_PROCESSOR) XGRAD, &loggrd);
	/*fprintf(stderr, "target: %f [G/cm] \n",target);*/
    getramptime(&rtime, &ftime,(WF_PROCESSOR) psbb->axis, &loggrd);

    /*if( amppwgrad(moment,loggrd.tx, 0.0, 0.0, loggrd.xrt, MIN_PLATEAU_TIME, &psbb->a_gx, &psbb->pw_gxa, &psbb->pw_gx,
                  &psbb->pw_gxd)
        == FAILURE)
        return FAILURE;/

	/*  spoilers to return the readout grads to a constant state  */
	psbb->pw_gx = 1ms;
	psbb->a_gx = 0.500 * loggrd.xfs;
	psbb->pw_gxa = cfrmp2xfs;
	psbb->pw_gxd = cfrmp2xfs;
		
	/*y and z have identical timting*/
	psbb->pw_gya = psbb->pw_gxa;
	psbb->pw_gy = psbb->pw_gx;
	psbb->pw_gyd = psbb->pw_gxd;
	psbb->pw_gza = psbb->pw_gxa;
	psbb->pw_gz = psbb->pw_gx;
	psbb->pw_gzd = psbb->pw_gxd; 
	
	psbb->a_gy = psbb->a_gx;
	psbb->a_gz = psbb->a_gx;	

	psbb->ia_gx = (psbb->a_gx / loggrd.tx) * MAX_PG_IAMP;
	psbb->ia_gy = (psbb->a_gy / loggrd.ty) * MAX_PG_IAMP;	
	psbb->ia_gz = (psbb->a_gz / loggrd.tz) * MAX_PG_IAMP;

    /*set timing of sbb */
    psbb->total_time =  psbb->pw_gx
                        + psbb->pw_gxa
                        + psbb->pw_gxd;
  

    /*reset number of instances with every call of cveval*/
    psbb->Ninstances = 0;
    

    return SUCCESS;
}

STATUS sbb_spoil_grad_predownload(void){



    return SUCCESS;
};



/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg



STATUS sbb_spoil_grad_pulsegen(sbb_spoil_grad* psbb,CHAR *sbb_name,int start_time)
{
    /*start with slice selection gradients first */
    int ia_start = 0;
    int ia_end = 0;
    char name[40];
	
    psbb->start_time = start_time;
    
    /*X-gradient */
    sprintf(name,"%s_%s",sbb_name,"gx");
    trapezoid(XGRAD,name, &psbb->gx, &psbb->gxa, &psbb->gxd,
              psbb->pw_gx, psbb->pw_gxa, psbb->pw_gxd, psbb->ia_gx,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    /*Y-gradient*/
    sprintf(name,"%s_%s",sbb_name,"gy");
    trapezoid(YGRAD,name, &psbb->gy, &psbb->gya, &psbb->gyd,
              psbb->pw_gy, psbb->pw_gya, psbb->pw_gyd, psbb->ia_gy,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);
			  
	/*Z-gradient*/
    sprintf(name,"%s_%s",sbb_name,"gz");
    trapezoid(ZGRAD,name, &psbb->gz, &psbb->gza, &psbb->gzd,
              psbb->pw_gz, psbb->pw_gza, psbb->pw_gzd, psbb->ia_gz,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);


    /*blank pulse instructions */
    /*int pw_wait;
    pw_wait = psbb->pw_gxa + psbb->pw_gx + psbb->pw_gxd;
    sprintf(name,"%s_%s",sbb_name,"bl_rho");
    pulsename(&psbb->blank_rho,name);
    createconst(&psbb->blank_rho,RHO,pw_wait,0); 
    createinstr( &psbb->blank_rho,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_theta");
    pulsename(&psbb->blank_theta,name);
    createconst(&psbb->blank_theta,THETA,pw_wait,0); 
    createinstr( &psbb->blank_theta,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_omega");
    pulsename(&psbb->blank_omega,name);
    createconst(&psbb->blank_omega,OMEGA,pw_wait,0); 
    createinstr( &psbb->blank_omega,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_ssp");
    pulsename(&psbb->blank_ssp,name);
    createconst(&psbb->blank_ssp,SSP,pw_wait,0); 
    createinstr( &psbb->blank_ssp,psbb->start_time,pw_wait,0);*/

    /*keep track of how many times this pulse is played out (=called during pulsegen) */
    psbb->Ninstances++;
	
    if(psbb->debug > 0){
	    fprintf(stderr, "sbb_spoil_grad_pulsegen() finished. \n" );
    }

    return SUCCESS;
};



/* end @pg rfov_funcs.e **********************/


/***********************************/
@rsp _rsp
void sbb_spoil_grad_psdinit(sbb_spoil_grad* psbb){
	
	/*initialize all instances*/
	setiampt(psbb->ia_gx, &psbb->gx,INSTRALL);/*use setiampt() instead of setiamp() if you want to change the trapezoid amplitude */
	setiampt(psbb->ia_gy, &psbb->gy,INSTRALL);
	setiampt(psbb->ia_gz, &psbb->gz,INSTRALL);

    if(psbb->debug > 0){
	fprintf(stderr, "sbb_spoil_grad_psdinit(): done.\n");
    }

};



