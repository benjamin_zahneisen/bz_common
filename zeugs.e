/* ****************************************
   MYSCAN
   myscan sets up the scan_info table for a hypothetical scan.
   It is controlled by the cv opslquant, and opslthick, and opfov. 
   ************************************** */
void
myscan( void )
{
    int i,j;
    int num_slice;
    float z_delta;		/* change in z_loc between slices */
    float r_delta;		/* change in r_loc between slices */
    double alpha, beta, gamma; /* rotation angles about x, y, z respectively */
    
    num_slice = exist(opslquant);
    
    r_delta = exist(opfov)/num_slice;
    z_delta = exist(opslthick)+exist(opslspace);
    
    scan_info[0].optloc = 0.5*z_delta*(num_slice-1);
    if (myrloc!=0.0)
        scan_info[0].oprloc = myrloc;
    else 
        scan_info[0].oprloc = 0;
    
    for (i=1;i<9;i++)
        scan_info[0].oprot[i]=0.0;
    
    switch (exist(opplane)) {
    case PSD_AXIAL:
        scan_info[0].oprot[0] = 1.0;
        scan_info[0].oprot[4] = 1.0;
        scan_info[0].oprot[8] = 1.0;
        break;
    case PSD_SAG:
        scan_info[0].oprot[2] = 1.0;
        scan_info[0].oprot[4] = 1.0;
        scan_info[0].oprot[6] = 1.0;
        break;
    case PSD_COR:
        scan_info[0].oprot[1] = 1.0;
        scan_info[0].oprot[5] = 1.0;
        scan_info[0].oprot[6] = 1.0;
        break;
    case PSD_OBL:
        alpha = PI/4.0;  /* rotation about x (applied first) */
        beta = PI/8.0;   /* rotation about y (applied 2nd) */
        gamma = PI/4.0;  /* rotation about z (applied 3rd) */
        scan_info[0].oprot[0] = cos(gamma)*cos(beta);
        scan_info[0].oprot[1] = cos(gamma)*sin(beta)*sin(alpha) -
                                       sin(gamma)*cos(alpha);
        scan_info[0].oprot[2] = cos(gamma)*sin(beta)*cos(alpha) +
                                       sin(gamma)*sin(alpha);
        scan_info[0].oprot[3] = sin(gamma)*cos(beta);
        scan_info[0].oprot[4] = sin(gamma)*sin(beta)*sin(alpha) +
                                       cos(gamma)*cos(alpha);
        scan_info[0].oprot[5] = sin(gamma)*sin(beta)*cos(alpha) -
                                       cos(gamma)*sin(alpha);
        scan_info[0].oprot[6] = -sin(beta);
        scan_info[0].oprot[7] = cos(beta)*sin(alpha);
        scan_info[0].oprot[8] = cos(beta)*cos(alpha);
        break;
    }
  
    for(i=1;i<num_slice;i++) {
        scan_info[i].optloc = scan_info[i-1].optloc - z_delta;
        scan_info[i].oprloc = i*r_delta;
        for(j=0;j<9;j++)
            scan_info[i].oprot[j] = scan_info[0].oprot[j];
    }
    
    return;
    
}