#ifndef bz_funcs_h
#define bz_funcs_h


/*my k-space coverage*/
void myPhaseTable(short* viewtable,int N){

    float dK = 2.0/N;
    int i;
    float Kstart = -1.0; /*corresponds to Kmax */
    if(N%2 == 1)
        Kstart+=(dK/2.0); /*add half a k-space increment. We want to hit k=0*/

    for(i=0;i<N;i++){
        viewtable[i+1] = (short) (2*floor((Kstart + dK*i)*MAX_PG_IAMP/2.0 +0.5));
        /*fprintf(stderr,"my viewtable %i \n",viewtable[i+1] );*/
    }

    /*if N==1 ==> no z-encoding gradients*/
    if(N==1)
        viewtable[1] = 0;

    /*first entry of viewtable must be zero */
    viewtable[0] = 0;
}

/*get k-space in actual units */
int getKspace(float* K,float FOV,int N){

    /* float* K  = return k-space array [1/cm] (O) */
    /* float FOV = field-of-view [cm] */
    /* int N = number of samples */
    /*returns K-space center idx*/

    float dK;
    float Kmax;
    int K0_idx;
    dK = 1/FOV;
    Kmax = dK*(float)N / 2.0; /*[1/cm]*/

    /*make k  vector */
    int n;
    for(n=0;n<N;n++){
        K[n] = -Kmax + n*dK;
        if (N%2 == 1)
            K[n]+= dK/2.0; /* this ensures that there is a K=0 sample */
    }

    if( (N ==1) || (FOV < 0) ){
        K[0] = 0.0;
        dK  = 0.0;
        Kmax = 0.0;
        K0_idx = 0;
    }
    else{
        K0_idx = ceil(N/2.0)-1;
    }

    return K0_idx;
}


/*get max momentum for given k-space*/
void getMaxKMomentum(float* momentum,float FOV,int N){

    float Kmax;
    Kmax = (10.0/FOV)*N/2.0; /*[1/cm]*/
    /* The 1.0e6/GAM factor is included to convert cm^-1 */
      /* units to G-us/cm units - 1.0e6 usec/sec / 4257 1/(sec-Gauss)*/

    *momentum = (1000.0*1000.0)*(Kmax/GAM); /*[1/cm]/[Hz/G] = [G/cm * microses] = grad momentum */

    if(N==1){
        Kmax = (10.0/256)*64/2.0;

        *momentum = (1000.0*1000.0)*(Kmax/GAM);
    }

}

/********************************************************************/
/* RF spoiling                                                      */
/* ntab = how many rf-pulses (I)                                    */
/* rfphase = array of rf-phase (32767) (O)                             */
/********************************************************************/
void get_spgr_phases(int ntab,int* rfphase ){

    int IA = 141; /*  24 bit overflow  */
    int IC = 28411;
    int IM = 134456;
    int i, jran;
    float x;

    /*parameter choices */
    int seed = 16807; /*with{   0,16807,,,"Spoiled Grass seed value","seed"};*/
    int spgr_flag =1;

    if(spgr_flag<=2) {

        switch (spgr_flag) {
            case 0: /* no spoiling  */
                for (i=0;i<ntab;i++)
                    rfphase[i] = 0;
                break;

            case 1: /*  GEMS algorithm  */
                rfphase[0] = 0;
                for (i=1;i<ntab;i++)
                    rfphase[i] = ((int)((float)rfphase[i-1] + (float)i*seed + 3L*FS_PI) %
                        FS_2PI)-FS_PI;
                break;

            case 2: /* random number generator (Num. Recipes, p.209  */
                jran = seed;
                for (i=0;i<ntab;i++)
                {
                    jran = (jran*IA + IC) % IM;
                    rfphase[i] = (int)((float)(FS_2PI + 1)*(float)jran/(float)IM) - FS_PI;
                }
                break;
            default:
            break;
        } /* switch  */
    }
    else /*  make quadratic phase schedule  */
    {
        for (i=0;i<ntab;i++)
        {
            x = spgr_flag*(i - ntab/2);
            rfphase[i] = (int)(x*x) % FS_2PI - FS_PI;
        }
    }

    return;
} /*  end get_spgr_phase  */

#endif
