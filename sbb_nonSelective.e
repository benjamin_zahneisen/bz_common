/*
 *  exc_sbb.e
 *
 *  This file contains all the external functions
 *  defined in exc_sbb.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include <mplx_rfpulse.h>
#include "sbb_nonSelective.h"




/**********************************************/
@host _ipgexport



/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
/*#include <mplx_rfpulse.c>*/

void sbb_nonSelective_init(sbb_nonSelective* p){
    p->prfpulse = NULL;

    initWF_PULSE(&p->rf);
    initWF_PULSE(&p->rf_theta);

    initWF_PULSE(&p->gzcrushla);
    initWF_PULSE(&p->gzcrushl);
    initWF_PULSE(&p->gzcrushld);
    initWF_PULSE(&p->gzcrushra);
    initWF_PULSE(&p->gzcrushr);
    initWF_PULSE(&p->gzcrushrd);

    initWF_PULSE(&p->gzcrushlXa);
    initWF_PULSE(&p->gzcrushlX);
    initWF_PULSE(&p->gzcrushlXd);
    initWF_PULSE(&p->gzcrushrXa);
    initWF_PULSE(&p->gzcrushrX);
    initWF_PULSE(&p->gzcrushrXd);

    initWF_PULSE(&p->gzcrushlYa);
    initWF_PULSE(&p->gzcrushlY);
    initWF_PULSE(&p->gzcrushlYd);
    initWF_PULSE(&p->gzcrushrYa);
    initWF_PULSE(&p->gzcrushrY);
    initWF_PULSE(&p->gzcrushrYd);

    /*control variables */
    p->ia_rfExt         = MAX_PG_IAMP;
    p->ia_rfExtTheta    = MAX_PG_IAMP;

    p->slcThickness     = 0;
    p->axis             = ZGRAD;

    p->alpha            = 180.0;  /*flip angle in deg */
    p->RF_SLOT          = 1; /*slot in rfpulse[] structure*/
    p->exciter          = TYPRHO1; /*which exciter RHO1 or RHO2*/
    p->Ninstances       = 0;
    p->stretchFactor    = 1.0;

    /*timing*/
    p->start_time       = 0;
    p->total_time       = 5000;
    p->psd_rf_wait      = 0; /*(long) get_rf_dly();*/
    p->psd_grd_wait     = 0; /*(long) get_grad_dly();*/ /*52 seems reasonable*/
    p->pw_rfExt         = 0;

    /* psd_grd_wait:  delay between waveform appearing on gradient sequencer and production of physical gradient */
    /* psd_rf_wait = delay between gradient and RF output */
};

/*helpfer function to initialize WF_PULSE*/
#ifndef initWF_PULSE_h
#define initWF_PULSE_h
void initWF_PULSE(WF_PULSE* pwf){
    pwf->pulsename = "<uninitialized>";
    pwf->ninsts = 0;
    pwf->wave_addr = -1;
    pwf->board_type = PSDCERD;
    pwf->reusep =  PSDREUSEP;
    pwf->tag =  SSPUNKN;
    pwf->addtag =  0;
    pwf->id =  0;
    pwf->ctrlfield =  0;
    pwf->inst_hdr_tail =  NULL;
    pwf->inst_hdr_head =  NULL;
    pwf->wavegen_type =TYPXGRAD;
    pwf->type =  TYPBITS;
    pwf->resolution = 0;
    pwf->assoc_pulse = NULL;
    pwf->ext =  NULL;
};
#endif

STATUS sbb_nonSelective_cveval(sbb_nonSelective* psbb)
{

    if(psbb->prfpulse == NULL){
        fprintf(stderr, "sbb_nonSelective_cveval() error: No rfpulse specified! \n");
        return FAILURE;
     }

    float target;
    int rtime,ftime;
    float revgzcrush;
    float areagzcrush;

    gettarget(&target,(WF_PROCESSOR) psbb->axis, &loggrd);
    getramptime(&rtime, &ftime,(WF_PROCESSOR) psbb->axis, &loggrd);


    /* crusher gradients */
    revgzcrush = 8.0;
    areagzcrush = 2 * revgzcrush * 10000000.0 / GAM / (float)50; /*set it to 120mm slabs */



    if( amppwgrad(areagzcrush, target, 0.0, 0.0, rtime, MIN_PLATEAU_TIME, &psbb->a_gcrush, &psbb->pw_gcrushla, &psbb->pw_gcrushl,
                  &psbb->pw_gcrushld)
        == FAILURE)
        return FAILURE;


    psbb->ia_gcrush = (psbb->a_gcrush / loggrd.tz_xz) * MAX_PG_IAMP;

    /*copy values for right crusher */
    psbb->pw_gcrushr = psbb->pw_gcrushl;
    psbb->pw_gcrushra = psbb->pw_gcrushla;
    psbb->pw_gcrushrd = psbb->pw_gcrushld;


    /*set the pulse width*/
    psbb->pw_rfExt = (psbb->prfpulse->resolution) * (psbb->prfpulse->dwell) ;
    /*fprintf(stderr, "psbb->pw_rfExt %i \n",psbb->pw_rfExt);*/

    /*set timing of sbb */
    psbb->total_time =  psbb->pw_rfExt
                        +200 /*add 100 microsec pause before and after rf */
                        + 2*(psbb->pw_gcrushla + psbb->pw_gcrushl + psbb->pw_gcrushld);


    /*psbb->time_from_RFcenter =  psbb->pw_gzrf/2
                                + psbb->pw_gzrfa
                                + (psbb->pw_gcrushla + psbb->pw_gcrushl + psbb->pw_gcrushld);*/

    /*reset number of instances with every call of cveval*/
    psbb->Ninstances = 0;


    return SUCCESS;
}

STATUS sbb_nonSelective_predownload(void){



    return SUCCESS;
};

STATUS sbb_nonSelective_link_rfpulse(sbb_nonSelective* psbb,int slot){

    /*do we rely on rfpulse as a global variable ?? */

    rfpulse[slot].pw =(int *)  &(psbb->pw_rfExt);   /* int *pw;  pulse width */
    rfpulse[slot].amp = (float *) &(psbb->a_rfExt);  /* float *amp;      amplitude */
    rfpulse[slot].act_fa = (float *) &(psbb->alpha);   /*float *act_fa;   desired flip angle */
    rfpulse[slot].res = (int *) &(psbb->prfpulse->resolution);  /* pulse resolution *//* vmx 3/13/95 YI changed from short to int */
    rfpulse[slot].exciter = (int *) &(psbb->exciter);  /* waveform generator (e.g., TYPRHO1 or TYPRHO2) */

    /*SAR relevant parameters from rf-pulse */
    rfpulse[slot].abswidth      = psbb->prfpulse->SAR.abs_width; /* calculated RF absolute width */
    rfpulse[slot].effwidth      = psbb->prfpulse->SAR.eff_width; /* calculated RF effective width */
    rfpulse[slot].area          = psbb->prfpulse->SAR.area;     /* zero momemt of pulse */
    rfpulse[slot].dtycyc        = psbb->prfpulse->SAR.dty_cycle;   /* max % of pulse width above 23% and in any 1  lobe */
    rfpulse[slot].maxpw         = psbb->prfpulse->SAR.max_pw;    /* max % of time rf pulse is above 0 amp */
    rfpulse[slot].num           = 1;        /* quantity of this type of RF pulse */
    rfpulse[slot].max_b1        = psbb->prfpulse->SAR.max_b1;   /* peak B1 in G under standard conditions */
    rfpulse[slot].max_int_b1_sq = psbb->prfpulse->SAR.max_integral_b1_sqr;   /* peak integral B1^2 in GG-msec under standard conditions */
    rfpulse[slot].max_rms_b1    = psbb->prfpulse->SAR.max_rms_b1;      /* peak RMS B1 in G under standard conditions */
    rfpulse[slot].nom_fa        = psbb->prfpulse->SAR.nom_fa;   /* flip angle on resonance under standard conditions. */

    rfpulse[slot].nom_pw = psbb->prfpulse->nom_dwell*psbb->prfpulse->resolution;   /* pulse duration under standard conditions */
    rfpulse[slot].nom_bw =psbb->prfpulse->bw;   /* pulse bandwidth under standard conditions */

    rfpulse[slot].activity  = PSD_MPS2_ON+PSD_APS2_ON+PSD_SCAN_ON; /* bitmask showing when pulse is used */
    rfpulse[slot].reference = (char ) 0; /* flag for pulse used in TG setting */
    rfpulse[slot].isodelay  = 0; /* time in usec from the zero phase reference of pulse to its end*/
    rfpulse[slot].scale     = 0.0;    /* duty cycle scale factor */
    rfpulse[slot].extgradfile = FALSE; /* TRUE = rfpulse uses external gradient waveform file */


    return SUCCESS;
};

/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg

#include "mplx_rfpulse.c"


STATUS sbb_nonSelective_pulsegen(sbb_nonSelective* psbb,CHAR *sbb_name)
{

    /*multiplexPulse_print(psbb->prfpulse);*/

    if(psbb->prfpulse == NULL){
        fprintf(stderr, "sbb_nonSelective_pulsegen() error: No rfpulse specified! \n");
        return FAILURE;
     }

    /*start with slice selection gradients first */
    int ia_start = 0;
    int ia_end = 0;
    int posRF;
    int posRightCrusher;
    char name[128];

    sprintf(name,"%s_%s",sbb_name,"gzcrushl");
    /*left crusher gradient */
    trapezoid(ZGRAD, name, &psbb->gzcrushl, &psbb->gzcrushla, &psbb->gzcrushld,
              psbb->pw_gcrushl, psbb->pw_gcrushla, psbb->pw_gcrushld, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);
    
    sprintf(name,"%s_%s",sbb_name,"gxcrushl");
    trapezoid(XGRAD,name, &psbb->gzcrushlX, &psbb->gzcrushlXa, &psbb->gzcrushlXd,
              psbb->pw_gcrushl, psbb->pw_gcrushla, psbb->pw_gcrushld, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    posRF = (psbb->pw_gcrushla + psbb->pw_gcrushl + psbb->pw_gcrushld) + 100;



    posRightCrusher = posRF + (psbb->pw_rfExt ) + 100;
    sprintf(name,"%s_%s",sbb_name,"gzcrushr");
    trapezoid(ZGRAD,name, &psbb->gzcrushr, &psbb->gzcrushra, &psbb->gzcrushrd,
              psbb->pw_gcrushr, psbb->pw_gcrushra, psbb->pw_gcrushrd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,posRightCrusher + psbb->start_time, TRAP_ALL, &loggrd);
    
    sprintf(name,"%s_%s",sbb_name,"gxcrushr");
    trapezoid(XGRAD,name, &psbb->gzcrushrX, &psbb->gzcrushrXa, &psbb->gzcrushrXd,
              psbb->pw_gcrushr, psbb->pw_gcrushra, psbb->pw_gcrushrd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,posRightCrusher + psbb->start_time, TRAP_ALL, &loggrd);


    /*set pulse name */
    sprintf(name,"%s_%s",sbb_name,"rfmag");
    pulsename(&psbb->rf,name);
    sprintf(name,"%s_%s",sbb_name,"rftheta");
    pulsename(&psbb->rf_theta,name);

    /*reserves space in instruction memory */
    createreserve(&psbb->rf,RHO,psbb->prfpulse->resolution);
    createreserve(&psbb->rf_theta,THETA,psbb->prfpulse->resolution);

    /*convert waveforms in psbb from int to short*/
    short* wave_mag;
    short* wave_phase;
    wave_mag = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    wave_phase = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    int t;
    for(t=0;t<psbb->prfpulse->resolution;t++){
        wave_mag[t] = (short) psbb->prfpulse->iwave_mag[t];
        wave_phase[t] = (short) psbb->prfpulse->iwave_phase[t];
    }

    /*assign local wave to hardware */
    movewaveimm(wave_mag,&psbb->rf,0,psbb->prfpulse->resolution,TOHARDWARE);
    movewaveimm(wave_phase,&psbb->rf_theta,0,psbb->prfpulse->resolution,TOHARDWARE);

    /*create instruction */
    psbb->ia_rfExtTheta = MAX_PG_IAMP;
    psbb->ia_rfExt = MAX_PG_IAMP*psbb->prfpulse->a_RF; /*get instruction amplitude after B1 scaling*/

    long start_rf;
    start_rf =  psbb->start_time
                + (psbb->pw_gcrushla + psbb->pw_gcrushl + psbb->pw_gcrushld) /*left crusher */
                + 100;

    createinstr( &psbb->rf,start_rf,(long)psbb->pw_rfExt,(long)psbb->ia_rfExt);
    createinstr( &psbb->rf_theta,start_rf,(long)psbb->pw_rfExt,(long)psbb->ia_rfExtTheta);

    /*this ensures that the last point of the waveform is odd, I guess */
    setweos(EOS_DEAD,&psbb->rf,psbb->prfpulse->resolution-1);
    setweos(EOS_DEAD,&psbb->rf_theta,psbb->prfpulse->resolution-1);

    addrfbits(&psbb->rf,0,start_rf, psbb->pw_rfExt);

    /*keep track of how many times this pulse is played out (=called during pulsegen) */
    psbb->Ninstances++;
    fprintf(stderr, "Ninstances %i. \n", psbb->Ninstances);


    fprintf(stderr, "sbb_nonSelective_pulsegen() finished. \n" );

    return SUCCESS;
};




/***********************************/
@rsp _rsp
void sbb_nonSelective_psdinit(sbb_nonSelective* psbb){

    fprintf(stderr, "sbb_nonSelective_psdinit(): entered.\n");

    /*loop over all instances*/
    int n;
    for(n=0;n<psbb->Ninstances;n++){
        /*set frequency to zero */
        setfrequency(0.0, &psbb->rf, n);

        /*set amplitudes */
        setiamp(psbb->ia_rfExt, &psbb->rf, n);
        setiamp(MAX_PG_IAMP, &psbb->rf_theta, n);

        /*crusher gradients*/
        setiamp(psbb->ia_gcrush, &psbb->gzcrushl,n);
        setiamp(psbb->ia_gcrush, &psbb->gzcrushr,n);
        setiamp(psbb->ia_gcrush, &psbb->gzcrushlX,n);
        setiamp(psbb->ia_gcrush, &psbb->gzcrushrX,n);
    }

};

void sbb_nonSelective_setfrequency(sbb_nonSelective* psbb,long freq){

    #ifdef SIM
        fprintf(stderr, "sbb_nonSelective_setfrequency(): entering.\n");
    #endif

    /*loop over all instances*/
    long n;
    for(n=0;n < psbb->Ninstances;n++){
        setfrequency(freq, &psbb->rf, n);
    }


};


void sbb_nonSelective_setphase(sbb_nonSelective* psbb,double phase){
    /*phase value in radians  (e.g. pi/2)*/

    setphase(phase, &psbb->rf, INSTRALL);
};

