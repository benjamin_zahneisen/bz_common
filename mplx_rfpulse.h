#ifndef _MPLX_RFPULSE
#define _MPLX_RFPULSE

/*#define _DEBUG_MPLX*/


#ifndef M_PI
#define M_PI 3.141592653589793
#endif
#ifndef GAMMAP
#define GAMMAP      42.5771
#endif
#ifndef SQUARE
#define SQUARE(x)   (pow((double)(x),2.0))
#endif
#ifndef ABS
#define ABS(x) ( (x>0) ? x : -x)
#endif
/*#include "stdlib.h"*/
/*#include "math.h"
#include "time.h"
#include "string.h"
#include "stdio.h"*/

/*#include "support_func.host.h"*/


#define MAX_MPLX_RESOLUTION 2048
#define MAX_MPLX_SLICES 9
#define DWELL_RF 2 /*time between two rf samples [microsec] */
#ifndef PI
    #define PI 3.14159
#endif

/*Eric Wong's Phase optimization, see ISMRM 2012 #2209
 this reduces the multiband peak voltage from linear with N to sqrt(N) */

/*float afWONG_PHASES[MAX_MPLX_SLICES][MAX_MPLX_SLICES] =
{
     {0, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 0.730, 4.602, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 3.875, 5.940, 6.197, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 3.778, 5.335, 0.872, 0.471, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 2.005, 1.674, 5.012, 5.736, 4.123, 0,     0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 3.002, 5.998, 5.909, 2.624, 2.527, 2.440, 0,     0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 1.036, 3.414, 3.778, 3.215, 1.756, 4.555, 2.467, 0,     0,     0,     0,     0,     0,     0,     0,    },
     {0, 1.250, 1.783, 3.558, 0.739, 3.319, 1.296, 0.521, 5.332, 0,     0,     0,     0,     0,     0,     0,    },
     {0, 4.418, 2.360, 0.677, 2.253, 3.472, 3.040, 3.974, 1.192, 2.510, 0,     0,     0,     0,     0,     0,    },
     {0, 5.041, 4.285, 3.001, 5.765, 4.295, 0.056, 4.213, 6.040, 1.078, 2.759, 0,     0,     0,     0,     0,    },
     {0, 2.755, 5.491, 4.447, 0.231, 2.499, 3.539, 2.931, 2.759, 5.376, 4.554, 3.479, 0,     0,     0,     0,    },
     {0, 0.603, 0.009, 4.179, 4.361, 4.837, 0.816, 5.995, 4.150, 0.417, 1.520, 4.517, 1.729, 0,     0,     0,    },
     {0, 3.997, 0.830, 5.712, 3.838, 0.084, 1.685, 5.328, 0.237, 0.506, 1.356, 4.025, 4.483, 4.084, 0,     0,    },
     {0, 4.126, 2.266, 0.957, 4.603, 0.815, 3.475, 0.977, 1.449, 1.192, 0.148, 0.939, 2.531, 3.612, 4.801, 0,    },
     {0, 4.359, 3.510, 4.410, 1.750, 3.357, 2.061, 5.948, 3.000, 2.822, 0.627, 2.768, 3.875, 4.173, 4.224, 5.941 }
};
*/

float afWONG_PHASES[MAX_MPLX_SLICES][MAX_MPLX_SLICES] =
{
     {0, 0,     0,     0,     0,     0,     0,     0        },
     {0, 2.1,     0,     0,     0,     0,     0,     0       },
     {0, 0.730, 4.602, 0,     0,     0,     0,     0       },
     {0, 3.875, 5.940, 6.197, 0,     0,     0,     0        },
     {0, 3.778, 5.335, 0.872, 0.471, 0,     0,     0        },
     {0, 2.005, 1.674, 5.012, 5.736, 4.123, 0,     0        },
     {0, 3.002, 5.998, 5.909, 2.624, 2.527, 2.440, 0         },
     {0, 1.036, 3.414, 3.778, 3.215, 1.756, 4.555, 2.467     },

};



typedef struct rfstat_info {
        float nom_fa; /*we assume that this does not change */
        int nom_pw; /*pulse width microsec */
        int number; /*resolution */
        int temp_pw;
        /*these get calculated */
        float area;
        float abs_width;
        float eff_width;
        float max_point;
        float dty_cycle;
        float max_pw;
        float sum_aj;
        float ai_sqr;
        float max_b1;
        float max_integral_b1_sqr;
        float max_rms_b1;
        float pw_sec;

} rfstat_info;
#define RFSTAT_INIT {90.0,3200,800,0,0,0,0,0,0,0,0,0,0,0,0};

typedef struct mplx_pulse {
        int resolution;
        int singleBandWaveOrg_mag[MAX_MPLX_RESOLUTION];
        int singleBandWaveOrg_phase[MAX_MPLX_RESOLUTION];
        float bw; /*bandwidth [Hz] after stretching*/
        float nom_bw; /*nominal bandwidth*/
        float slcSelectAmp; /* slice select gradient amplitude of the single band pulse [G/cm] */
        float slcThickness;
        float slcScaling; /*factor by which the effective slice thickness will be reduced */
        float maxB1_org;/*amplitude of single band pulse [G] */
        float maxB1; /*actual amplitude of multi-band pulse */
        int dwell; /*actual sample spacing [micros]*/
        int nom_dwell;/*nominal sample spacing [micros]*/
        float stretchFactor; /*stretch or compress pulse widths*/
        int nonSelective; /*no slice selection gradient needed if =1*/
        int pw; /*pulse width = dwell*resolution [microsec];*/
        int nom_pw; /*pulse width under standard conditions*/
        float alpha; /*flip angle in degrees*/
        float nom_alpha; /*flip angle under standard conditions*/
        float a_RF; /*amplitude of the rf pulse between 0 and 1 after rf-scaling */
        int rfslot; /*position if rfpulse array*/
        int exciter; /*which exciter RHO1 or RHO2*/

        int iwave_mag[MAX_MPLX_RESOLUTION];
        int iwave_phase[MAX_MPLX_RESOLUTION];

        int mplxFactor;
        float sliceSeparation; /*cm*/
        float sliceLocations[MAX_MPLX_SLICES]; /*absolute locations of bands relative to isocenter [cm]*/
        float isoSliceLocations[MAX_MPLX_SLICES]; /*locations of bands around isocenter [cm]*/
        int isMultiplexed;
        int isInitialized;
        char name[80];
        int useWongPhases;
        float FOV; /*FOV = mplxFactor*sliceSeparation [cm] */
        float K; /*associated K-space value [1/cm] for POMP rf-encoding */
        int debug; /*print out debug statements*/

        /*SAR relevant parameters */
        rfstat_info SAR;

} mplx_pulse;


/*function declarations */
int multiplexPulse_init(mplx_pulse* p);
int multiplexPulse_initWave(mplx_pulse* p,short* wave, short* wave_theta,int resolution);
int multiplexPulse_initWaveParams(mplx_pulse* p,float maxB1,float nom_bw,float nom_fa,int nom_pw,int res);
int multiplexPulse_initWaveFromFile(mplx_pulse* p,char* fname,char* fname2, int res);
int multiplexPulse_stretchWave(mplx_pulse* p,float stretch);
int multiplexPulse_initPompArray(mplx_pulse* p,mplx_pulse* p_array,int length);
int multiplexPulse_modulate(mplx_pulse* p,int mb,float K,float globalPhase);
STATUS multiplexPulse_link_rfpulse(mplx_pulse* p,RF_PULSE* rfpulse,int slot);
int multiplexPulse_rfstat(mplx_pulse* p);
int multiplexPulse_print(mplx_pulse* p);
int multiplexPulse_export(mplx_pulse* p);
/*int multiplexPulse_logmessage( char *logfilename,char *logstring,int rewrite );*/
int multiplex_waveform(float* mplxMag,float* mplxPhase,float* single_mag,float* single_phase,float* freq,float* phaseOffset, int mb,int tpoints,float dwell);
int multiplex_sliceLocs(mplx_pulse* p,SCAN_INFO* pscan_info,int nsltot,int mb_factor);
int multiplex_calcSlcSelectAmp(mplx_pulse* p,float slthick, float scaleFactor);

/*helper functions*/
int float2short(float maxValFloat,float* f,int* i,int res);
int short2float(int* i, float* f, int res);
short reverseShort(short s);
int importExtPulse(short* wave,int resolution,char* fname);

#endif
