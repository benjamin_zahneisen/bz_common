/*
 *  sbb_diff_grad.e = plays out a gradient pulse on all three axis
 *
 *  This file contains all the external functions
 *  defined in sbb_diff_grad.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2016
 *
 */

/**********************************************/
@global _global
#include "sbb_diff_grad.h"




/**********************************************/
@host _ipgexport


/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
#include "sbb_diff_grad.h"

void sbb_diff_grad_init(sbb_diff_grad* p){
	
	initWF_PULSE(&p->gxa);
	initWF_PULSE(&p->gx);
	initWF_PULSE(&p->gxd);

	initWF_PULSE(&p->gya);
	initWF_PULSE(&p->gy);
	initWF_PULSE(&p->gyd);

	initWF_PULSE(&p->gza);
	initWF_PULSE(&p->gz);
	initWF_PULSE(&p->gzd);

	initWF_PULSE(&p->blank_rho);
	initWF_PULSE(&p->blank_theta);
	initWF_PULSE(&p->blank_omega);
	initWF_PULSE(&p->blank_ssp);
    
	p->start_time = 0;
	p->Ninstances = 0;
	p->use_maxloggrad=1;
	p->debug = 0;


};



STATUS sbb_diff_grad_cveval(sbb_diff_grad* psbb,float moment)
{
    fprintf(stderr, "\n welcome to sbb_diff_grad_cveval()\n");


    float target;
    int rtime,ftime;
    gettarget(&target,XGRAD, &loggrd);
	/*fprintf(stderr, "target: %f [G/cm] \n",target);*/
    getramptime(&rtime, &ftime,(WF_PROCESSOR) psbb->axis, &loggrd);

    if( amppwgrad(moment,loggrd.tx, 0.0, 0.0, loggrd.xrt, MIN_PLATEAU_TIME, &psbb->a_gx, &psbb->pw_gxa, &psbb->pw_gx,
                  &psbb->pw_gxd)
        == FAILURE)
        return FAILURE;
		
	/*y and z have identical timting*/
	psbb->pw_gya = psbb->pw_gxa;
	psbb->pw_gy = psbb->pw_gx;
	psbb->pw_gyd = psbb->pw_gxd;
	psbb->pw_gza = psbb->pw_gxa;
	psbb->pw_gz = psbb->pw_gx;
	psbb->pw_gzd = psbb->pw_gxd; 
	
	psbb->a_gy = psbb->a_gx;
	psbb->a_gz = psbb->a_gx;	

	psbb->ia_gx = (psbb->a_gx / loggrd.tx) * MAX_PG_IAMP;
	psbb->ia_gy = (psbb->a_gy / loggrd.ty) * MAX_PG_IAMP;	
	psbb->ia_gz = (psbb->a_gz / loggrd.tz) * MAX_PG_IAMP;

    /*set timing of sbb */
    psbb->total_time =  psbb->pw_gx
                        + psbb->pw_gxa
                        + psbb->pw_gxd;
  

    /*reset number of instances with every call of cveval*/
    psbb->Ninstances = 0;
    
    /*fprintf(stderr, "psbb->a_gx: %f [G/cm] \n",psbb->a_gx);
    fprintf(stderr, "psbb->pw_gx: %i [G/cm] \n",psbb->pw_gx);*/

    return SUCCESS;
}

STATUS sbb_diff_grad_predownload(void){



    return SUCCESS;
};



/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg



STATUS sbb_diff_grad_pulsegen(sbb_diff_grad* psbb,CHAR *sbb_name,int start_time)
{
    /*start with slice selection gradients first */
    int ia_start = 0;
    int ia_end = 0;
    char name[40];
	
    psbb->start_time = start_time;
    
    /*X-gradient */
    sprintf(name,"%s_%s",sbb_name,"gx");
    trapezoid(XGRAD,name, &psbb->gx, &psbb->gxa, &psbb->gxd,
              psbb->pw_gx, psbb->pw_gxa, psbb->pw_gxd, psbb->ia_gx,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    /*Y-gradient*/
    sprintf(name,"%s_%s",sbb_name,"gy");
    trapezoid(YGRAD,name, &psbb->gy, &psbb->gya, &psbb->gyd,
              psbb->pw_gy, psbb->pw_gya, psbb->pw_gyd, psbb->ia_gy,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);
			  
	/*Z-gradient*/
    sprintf(name,"%s_%s",sbb_name,"gz");
    trapezoid(ZGRAD,name, &psbb->gz, &psbb->gza, &psbb->gzd,
              psbb->pw_gz, psbb->pw_gza, psbb->pw_gzd, psbb->ia_gz,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);


    /*blank pulse instructions */
    int pw_wait;
    pw_wait = psbb->pw_gxa + psbb->pw_gx + psbb->pw_gxd;
    sprintf(name,"%s_%s",sbb_name,"bl_rho");
    pulsename(&psbb->blank_rho,name);
    createconst(&psbb->blank_rho,RHO,pw_wait,0); 
    createinstr( &psbb->blank_rho,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_theta");
    pulsename(&psbb->blank_theta,name);
    createconst(&psbb->blank_theta,THETA,pw_wait,0); 
    createinstr( &psbb->blank_theta,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_omega");
    pulsename(&psbb->blank_omega,name);
    createconst(&psbb->blank_omega,OMEGA,pw_wait,0); 
    createinstr( &psbb->blank_omega,psbb->start_time,pw_wait,0);

    sprintf(name,"%s_%s",sbb_name,"bl_ssp");
    pulsename(&psbb->blank_ssp,name);
    createconst(&psbb->blank_ssp,SSP,pw_wait,0); 
    createinstr( &psbb->blank_ssp,psbb->start_time,pw_wait,0);

    /*keep track of how many times this pulse is played out (=called during pulsegen) */
    psbb->Ninstances++;
	
    if(psbb->debug > 0){
	    fprintf(stderr, "sbb_diff_grad_pulsegen() finished. \n" );
    }

    return SUCCESS;
};



/* end @pg rfov_funcs.e **********************/


/***********************************/
@rsp _rsp
void sbb_diff_grad_psdinit(sbb_diff_grad* psbb){
	
	/*initialize all instances*/
	setiampt(psbb->ia_gx, &psbb->gx,INSTRALL);/*use setiampt() instead of setiamp() if you want to change the trapezoid amplitude */
	setiampt(psbb->ia_gy, &psbb->gy,INSTRALL);
	setiampt(psbb->ia_gz, &psbb->gz,INSTRALL);

    if(psbb->debug > 0){
	fprintf(stderr, "sbb_diff_grad_psdinit(): done.\n");
    }

};

/*shrink duration of diffusion gradients to 12 microsecs */
void sbb_diff_grad_collapse(sbb_diff_grad* psbb,int reverse){


  if(reverse == 0){
      sbb_diff_grad_step(psbb,0.0,0.0,0.0,-1);/*set all amplitudes to zero*/

      setperiod((int)GRAD_UPDATE_TIME, &psbb->gxa, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gx, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gxd, 0);

      setperiod((int)GRAD_UPDATE_TIME, &psbb->gya, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gy, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gyd, 0);

      setperiod((int)GRAD_UPDATE_TIME, &psbb->gza, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gz, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->gzd, 0);


      setperiod((int)GRAD_UPDATE_TIME, &psbb->blank_rho, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->blank_theta, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->blank_omega, 0);
      setperiod((int)GRAD_UPDATE_TIME, &psbb->blank_ssp, 0);
  }
  else{

      setperiod((long) psbb->pw_gxa, &psbb->gxa, 0);
      fprintf(stderr,"psbb->pw_gxa = %i psbb->pw_gxa \n",psbb->pw_gxa);
      setperiod(psbb->pw_gx, &psbb->gx, 0);
      setperiod(psbb->pw_gxd, &psbb->gxd, 0);

      setperiod(psbb->pw_gxa, &psbb->gya, 0);
      setperiod(psbb->pw_gx, &psbb->gy, 0);
      setperiod(psbb->pw_gxd, &psbb->gyd, 0);

      setperiod(psbb->pw_gxa, &psbb->gza, 0);
      setperiod(psbb->pw_gx, &psbb->gz, 0);
      setperiod(psbb->pw_gxd, &psbb->gzd, 0);

      int pw_wait;
      pw_wait = psbb->pw_gxa + psbb->pw_gx + psbb->pw_gxd;

      setperiod(pw_wait, &psbb->blank_rho, 0);
      setperiod(pw_wait, &psbb->blank_theta, 0);
      setperiod(pw_wait, &psbb->blank_omega, 0);
      setperiod(pw_wait, &psbb->blank_ssp, 0);


  }





};


void sbb_diff_grad_step(sbb_diff_grad* psbb,float d1, float d2, float d3,int instance){
	
	if(instance > (psbb->Ninstances-1)){
		fprintf(stderr, "sbb_diff_grad_step(): too few instances.\n");
		return;
	}
	if((ABS(d1) > 1.0001) || (ABS(d2)>1.0001) || (ABS(d3)>1.0001) ){
		fprintf(stderr, "sbb_diff_grad_step(): scaling out of range.\n");
		return;
	}
		
	int iax,iay,iaz;
	iax = (int) psbb->ia_gx*d1;
	iay = (int) psbb->ia_gy*d2;
	iaz = (int) psbb->ia_gz*d3;
	/*    ia_incdifx = (int)(a_gxdl1*TENSOR_AGP[0][dshot]*(float)max_pg_iamp/loggrd.tx);*/
	
	if(instance == -1){
		setiampt(iax, &psbb->gx,INSTRALL); /*use setiampt() instead of setiamp() if you want to change the trapezoid amplitude */
		setiampt(iay, &psbb->gy,INSTRALL);
		setiampt(iaz, &psbb->gz,INSTRALL);		
	}
	else{
		setiampt(iax, &psbb->gx,instance);	
		setiampt(iay, &psbb->gy,instance);
		setiampt(iaz, &psbb->gz,instance);
	}
	
};

/* end @rsp rfov_funcs.e ********************/
