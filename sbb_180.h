/*
 *  exc_sbb.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in exc_sbb.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Jan/2015
 *
 */

#ifndef sbb_180_h
#define sbb_180_h

#define SBB180_DEBUG
/*
 * @global functions
 */


#include "mplx_rfpulse.h"

/*enable/disable support for POMP rf-encoding of multiple slices*/
#define POMP

/*rf excitation sequence building block */
typedef struct sbb_refoc{
        mplx_pulse* prfpulse;
        mplx_pulse* prfpulse_pomp[MAX_MPLX_SLICES];
        WF_PULSE rf;
        WF_PULSE rf_theta;
        WF_PULSE gzrfa;
        WF_PULSE gzrf;
        WF_PULSE gzrfd;

	/*left crusher Z*/
        WF_PULSE gzcrushla;
        WF_PULSE gzcrushl;
        WF_PULSE gzcrushld;

	/*right crusher Z*/
        WF_PULSE gzcrushra;
        WF_PULSE gzcrushr;
        WF_PULSE gzcrushrd;

	/*left crusher X*/
        WF_PULSE gxcrushla;
        WF_PULSE gxcrushl;
        WF_PULSE gxcrushld;

	/*right crusher X*/
        WF_PULSE gxcrushra;
        WF_PULSE gxcrushr;
        WF_PULSE gxcrushrd;

	/*left crusher Y*/
        WF_PULSE gycrushla;
        WF_PULSE gycrushl;
        WF_PULSE gycrushld;

	/*right crusher Y*/
        WF_PULSE gycrushra;
        WF_PULSE gycrushr;
        WF_PULSE gycrushrd;

        /*dynamic arrays for rf-pulses*/
        WF_PULSE* pomp_wf;
        WF_PULSE* pomp_wf_theta;
        WF_HW_WAVEFORM_PTR* wave_ptr;
        WF_HW_WAVEFORM_PTR* wave_theta_ptr;
        
	int Nwaves;/*number of available waves*/


        /*control variables */
        int ia_rfExt;
        float a_rfExt;
        int ia_rfExtTheta;
        float a_rfExtTheta;
        float a_gzrf;
        int ia_gzrf;
        float a_gcrush;
        int ia_gcrush;
        float slcThickness;
        int axis; /* 0=X;1=Y;2=Z*/
	int debug;

        /*SAR */
        float alpha; /*flip angle in deg */
        int RF_SLOT; /*slot in rfpulse[] structure*/
        int exciter; /*which exciter RHO1 or RHO2*/
        int Ninstances; /*how often do we employ that pulse per entry point */
        float B1scaling[MAX_MPLX_SLICES];
        float stretchFactor; /*multiple nominal pulse width by this factor */

        /*timing*/
        int start_time; /*all other timing is relative to the sbb start time */
        int total_time; /*duration of sbb */
        int time_from_RFcenter; /*sbb duration measured from peak of rf pulse */
        int psd_rf_wait;
        int psd_grd_wait;
        int pw_rfExt;
        int pw_gzrf;
        int pw_gzrfa;
        int pw_gzrfd;
      
	/*timing of crusher gradients all identical*/
	float revgzcrush;
        int pw_gcrusha;
        int pw_gcrush;
        int pw_gcrushd;

} sbb_refoc;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_refoc_cveval(sbb_refoc* psbb);
STATUS sbb_refoc_predownload(void);

/*
 * @pg functions
 */
STATUS sbb_refoc_pulsegen(sbb_refoc* psbb,CHAR *sbb_name,mplx_pulse* prf,int start_time);

/*load rf waveform into memory but do not load actual instructions*/
STATUS sbb_refoc_pulsegen_pomp(sbb_refoc* psbb,int Nwaves,mplx_pulse* prf);


/*************************/
/* @rsp functions */
/*************************/
void sbb_refoc_psdinit(sbb_refoc* psbb);
void sbb_refoc_setfrequency(sbb_refoc* psbb,long freq);
void sbb_refoc_setwave(sbb_refoc* psbb,int num);
void sbb_refoc_crusherTheme(sbb_refoc* psbb,int crushertheme);

#endif 
