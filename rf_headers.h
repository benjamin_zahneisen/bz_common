#ifndef rf_headers_h
#define rf_headers_h

/*3.2ms sinc pulse (bz_sinc.rho + bz_sinc.theta) */
#define SAR_ABS_RFEXT_SINC 0.3176 /* abswidth */
#define SAR_PRFEXT_SINC 0.2179 /* effwidth */
#define SAR_ARFEXT_SINC 0.3176 /* area */
#define SAR_DTYCYC_RFEXT_SINC 1.0
#define SAR_MAXPW_RFEXT_SINC 1.0 /* maxpw = dutycycle ??? */
#define MAX_B1_RFEXT_90_SINC 0.0723022
#define MAX_INT_B1_SQ_RFEXT_90_SINC 0.0036451
#define MAX_RMS_B1_RFEXT_90_SINC 0.03375
#define NOM_BW_RFEXT_SINC 1250
#define NOM_FA_RFEXT_SINC 90.0
#define NOM_RES_RFEXT_SINC 1600
#define NOM_PW_RFEXT_SINC 3200


/*3.2ms SLR pulse (bz_sinc2.rho for DV26+) */
#define SAR_ABS_RFEXT_SINC2 0.3176 /* abswidth */
#define SAR_PRFEXT_SINC2 0.207718 /* effwidth */
#define SAR_ARFEXT_SINC2 0.3176 /* area */
#define SAR_DTYCYC_RFEXT_SINC2 1.0
#define SAR_MAXPW_RFEXT_SINC2 1.0 /* maxpw = dutycycle ??? */
#define MAX_B1_RFEXT_90_SINC2 0.0765957
#define MAX_INT_B1_SQ_RFEXT_90_SINC2 0.0036451
#define MAX_RMS_B1_RFEXT_90_SINC2 0.03375
#define NOM_BW_RFEXT_SINC2 1250
#define NOM_FA_RFEXT_SINC2 90.0
#define NOM_RES_RFEXT_SINC2 320
#define NOM_PW_RFEXT_SINC2 3200

/*3.2ms SLR pulse ("rfse1b4.rho") the rest is defines in sar_pm.h */
#define NOM_PW_SE1B4_RF2 3200
#define NOM_FA_SE1B4_RF2 180


/* Some spsp RF1 definitions */
#define PSD_SE_RF1_PW   5ms    /* Pulse width */
#define PSD_SE_RF1_fPW  5.0ms
#define PSD_SE_RF1_LEFT  3100us
#define PSD_SE_RF1_RIGHT 1900us
#define PSD_SE_RF1_R     250   /* Resolution of FL90mc pulse */
#define PSD_GR_RF1_PW   3200us    /* Pulse width */
#define PSD_GR_RF1_fPW  3200.0us
#define PSD_GR_RF1_LEFT  1600us
#define PSD_GR_RF1_RIGHT 1600us
#define PSD_GR_RF1_R     400   /* Resolution of GR30l pulse */

#endif
