#include "mplx_rfpulse.h"
#ifndef mplx_rfpulse_c
#define mplx_rfpulse_c

/*BZ */
/*All these functions are intented to be standalone functions except for the link to the RF_PULSE array.*/

/*************************************************/
/* INIT: set structure to a known state          */
/*************************************************/
int multiplexPulse_init(mplx_pulse* p){

    if(!(p->isInitialized == 1)){
    p->resolution       = MAX_MPLX_RESOLUTION;
    p->mplxFactor       = 1;
    p->slcSelectAmp     = 1;
    p->slcThickness     = 5;
    p->slcScaling       = 1.0;
    p->bw               = 1.0;
    p->nom_bw           = 1.0;
    p->maxB1_org        = 0.0;
    p->maxB1            = 0.0;
    p->dwell            = DWELL_RF;
    p->nom_dwell        = DWELL_RF;
    p->stretchFactor    = 1.0;
    p->nonSelective     = 0;
    p->pw               = 0;
    p->nom_pw           = 0;
    p->alpha            = 0.0;
    p->nom_alpha        = 0.0;
    p->a_RF            = 0.0;
    p->rfslot           = 0;
    p->exciter          = TYPRHO1;
    p->sliceSeparation  = 0.0;
    p->isMultiplexed    = 0;
    p->useWongPhases    = 1;
    p->FOV              = -1;
    p->K                = 0.0;
    p->debug            = 0;
    p->SAR.nom_fa       = 0.0;
    p->SAR.nom_pw       = 0.0;
    /*Don't forget to change initPompArray too */

    /*set all wave arrays to zero */
    int t;
    for(t=0;t < MAX_MPLX_RESOLUTION;t++){
        /*p->wave_form_mag[t]      = 0;*/
        /*p->wave_form_phase[t]    = 0;*/
    }

    /*set all slice locations to zero */
    for(t=0;t < MAX_MPLX_SLICES;t++){
        p->sliceLocations[t]      = 0.0;
        p->isoSliceLocations[t]   = 0.0;
    }

    sprintf(p->name,"RFn");

    p->isInitialized = 1;
    }

    return 1;
};

/*************************************************/
/* INIT: set initial RF waveform and convert/store it as int array */
/*************************************************/
int multiplexPulse_initWave(mplx_pulse* p,short* wave, short* wave_theta, int resolution){


    if(resolution <= MAX_MPLX_RESOLUTION){
        p->resolution = resolution;
        int i;
        for(i=0;i<resolution;i++){
            p->singleBandWaveOrg_mag[i] = (int) wave[i];
            p->singleBandWaveOrg_phase[i] = (int) wave_theta[i];
        }
    }else{
        /*multiplexPulse_logmessage("/usr/g/bin/basic.log"," mulitplexPulse_init() error: resolution > MAX_MPLX_RESOLUTION ", 0);*/
        return 0;
    }


    if(p->debug)
      fprintf(stderr, "multiplexPulse_initWave() \n");


    p->isInitialized = 1;
    return 1;
};


/*************************************************/
/* read waveform from 5x GE rf-pulse format      */
/* wave is stored as short 32 shorts header + 2 shorts at end*/
/*************************************************/
int importExtPulse(short* wave,int resolution,char* fname){

    FILE* filePtr;
    filePtr = fopen(fname,"rl");
    if(filePtr == NULL){
        char message[256];
        sprintf(message,"no open file = %s \n",fname);
        write_log(message);
        return 0;

    }
    fprintf(stderr, "sizeof(short) =  %d \n",sizeof(short));
    fseek(filePtr,32*sizeof(short),SEEK_SET);
    fread(wave,sizeof(short),resolution,filePtr);

    fclose(filePtr);

    /*convert to little endian*/
    int t;
    for(t=0;t<resolution;t++){
        wave[t] = reverseShort(wave[t]);
    }

    return SUCCESS;

}

/*************************************************/
/* get the waveform from a file                  */
/*************************************************/
int multiplexPulse_initWaveFromFile(mplx_pulse* p,char* fname,char* fname2, int res){

    int t;
    short* wave_space;
    short* wave_space_theta;

    wave_space = (short *)AllocNode(res*sizeof(short));
    wave_space_theta = (short *)AllocNode(res*sizeof(short));

    /*initialize pulse object*/
    multiplexPulse_init(p);

    write_log("multiplexPulse_initWaveFromFile: after init\n");

    if(strcmp(fname2,"none")==0){

        /*fprintf(stderr, "loading wave from %s \n",fname);*/
        /*uextwave(wave_space,res,fname);*/ /*wave_space = +- 32767*/
        importExtPulse(wave_space,res,fname);



        /*re-generate theta wave for negative lobes of pulse*/
        for(t=0;t<res;t++){
            fprintf(stderr, "wave_space[t] %d \n",reverseShort(wave_space[t]));
            if(wave_space[t] < 0 ){
                wave_space[t] = ABS(wave_space[t]);
                wave_space_theta[t] = -MAX_PG_WAMP;
            }
            else{
                wave_space_theta[t] = 0;
            }
        /*fprintf(stderr,"wave [%i]: %i \n",t,wave_space[t]);
        fprintf(stderr,"wave_theta[%i]: %i \n",t,wave_space_theta[t]);*/
        }
    }
    else{
        /*fprintf(stderr, "loading mag wave from %s \n",fname);*/
        char message[256];
        sprintf(message,"multiplexPulse_initWaveFromFile: fname = %s \n",fname);
        write_log(message);
        /*uextwave(wave_space,res,fname);*/ /*wave_space = +- 32767*/
        /*fprintf(stderr, "loading theta wave from %s \n",fname2);*/
        /*uextwave(wave_space_theta,res,fname2);*/ /*wave_space = +- 32767*/
        importExtPulse(wave_space,res,fname);
        importExtPulse(wave_space_theta,res,fname2);
    }

    write_log("multiplexPulse_initWaveFromFile: after uextwave\n");

    /*call low level function */
    multiplexPulse_initWave(p,wave_space,wave_space_theta,res);

    /*set the resolution */
    p->resolution = res;


    /*free memory */
    FreeNode(wave_space);
    FreeNode(wave_space_theta);

    return SUCCESS;
};

/********************************************************/
/* set rf-pulse header variables */
/********************************************************/
int multiplexPulse_initWaveParams(mplx_pulse* p,float maxB1,float nom_bw,float nom_fa,int nom_pw, int res){

    p->maxB1_org = maxB1;
    p->maxB1     = maxB1;
    if(nom_bw < 0){
        p->nonSelective = 1;
        p->nom_bw = -1.0;
        p->bw = -1.0;
    }
    else{
        p->bw = nom_bw;
        p->nom_bw = nom_bw;
        p->nonSelective = 0;
    }
    p->SAR.nom_fa = nom_fa;/*degree */
    p->nom_alpha = nom_fa;
    p->alpha = nom_fa;
    p->nom_pw = nom_pw;
    p->SAR.nom_pw = nom_pw; /*microseconds */
    p->nom_dwell = nom_pw/res;
    p->dwell = p->nom_dwell;
    p->pw = nom_pw; /*[microsec]*/

    return SUCCESS;
};

/********************************************************/
/* stretch/compress pulse width and adapt bw, dwell,... */
/********************************************************/
int multiplexPulse_stretchWave(mplx_pulse* p,float stretch){


    /*actually stretch the waveform array (linear interpolation?) */
    int t;
    int newres;

    short* wave_space_new;
    short* wave_space_theta_new;
    short* wave_space_old;
    short* wave_space_theta_old;

    newres = (int) p->resolution*stretch;
    /*fprintf(stderr,"newres: %i \n",newres);*/

    if(newres > MAX_MPLX_RESOLUTION)
        return FAILURE;

    wave_space_new = (short *)AllocNode(newres*sizeof(short));
    wave_space_theta_new = (short *)AllocNode(newres*sizeof(short));
    wave_space_old = (short *)AllocNode(p->resolution*sizeof(short));
    wave_space_theta_old = (short *)AllocNode(p->resolution*sizeof(short));

    for(t=0;t<p->resolution;t++){
        wave_space_old[t] = (short) p->singleBandWaveOrg_mag[t];
        wave_space_theta_old[t] = (short) p->singleBandWaveOrg_phase[t];
        /*fprintf(stderr,"wave_old[%i]: %i \n",t,wave_space_old[t]);
        fprintf(stderr,"wave_theta_old[%i]: %i \n",t,wave_space_theta_old[t]);*/
    }

    stretchpulse(p->resolution,newres,wave_space_old,wave_space_new);
    stretchpulse(p->resolution,newres,wave_space_theta_old,wave_space_theta_new);

    p->resolution = newres;
    for(t=0;t<newres;t++){
        /*fprintf(stderr,"wave[%i]: %i \n",t,wave_space_new[t]);
        fprintf(stderr,"wave_theta[%i]: %i \n",t,wave_space_theta_new[t]);*/

        p->singleBandWaveOrg_mag[t] = (int) wave_space_new[t];
        p->singleBandWaveOrg_phase[t] = (int) wave_space_theta_new[t];
    }

    FreeNode(wave_space_new);
    FreeNode(wave_space_theta_new);


    /*update pulse parameter*/
    p->stretchFactor = stretch;
    /*p->dwell = (int)(p->nom_dwell*stretch);*/
    p->pw = (int) (p->nom_pw*stretch);

    if(p->nonSelective==0)
    p->bw = p->nom_bw/stretch;

    return SUCCESS;
};

/********************************************************/
/* copy all constant parameters to array of pomp pulses */
/********************************************************/
int multiplexPulse_initPompArray(mplx_pulse* p,mplx_pulse* p_array,int length){

    if(length > MAX_MPLX_SLICES)
        return FAILURE;

    int n,s;
    for(n=0;n<length;n++){

        p_array[n].resolution = p->resolution;

        p_array[n].bw = p->bw; /*bandwidth [Hz]*/
        p_array[n].nom_bw = p->nom_bw; /*bandwidth [Hz]*/
        p_array[n].slcSelectAmp = p->slcSelectAmp; /*amplitude of the single band pulse [G/cm] */
        p_array[n].slcThickness = p->slcThickness;
        p_array[n].slcScaling = p->slcScaling; /*factor by which the effective slice thickness will be reduced */
        p_array[n].maxB1= p->maxB1;
        p_array[n].maxB1_org= p->maxB1_org;
        p_array[n].dwell = p->dwell; /*sample spacing [micros]*/
        p_array[n].nom_dwell = p->nom_dwell; /*nominal sample spacing [micros]*/
        p_array[n].pw = p->pw;
        p_array[n].nom_pw = p->nom_pw;
        p_array[n].alpha = p->alpha;
        p_array[n].nom_alpha = p->nom_alpha;
        p_array[n].a_RF = p->a_RF;
        p_array[n].exciter = p->exciter;
        p_array[n].stretchFactor = p->stretchFactor;
        p_array[n].mplxFactor = p->mplxFactor;
        p_array[n].sliceSeparation = p->sliceSeparation; /*mm*/
        for(s=0;s<MAX_MPLX_SLICES;s++){
            p_array[n].sliceLocations[s] = p->sliceLocations[s]; /*absolute locations of bands relative to isocenter [cm]*/
            p_array[n].isoSliceLocations[s] = p->isoSliceLocations[s];; /*locations of bands around isocenter [cm]*/
        }
        p_array[n].isMultiplexed = p->isMultiplexed;
        p_array[n].isInitialized = p->isInitialized;
        strcpy(p_array[n].name,"pomp");
        p_array[n].useWongPhases = p->useWongPhases;
        p_array[n].FOV = p->FOV; /*FOV = mplxFactor*sliceSeparation [cm] */
        p_array[n].K = 0.0; /*associated K-space value [1/cm] for POMP rf-encoding */

        for(s=0;s<p->resolution;s++){
            p_array[n].singleBandWaveOrg_mag[s] = p->singleBandWaveOrg_mag[s];
            p_array[n].singleBandWaveOrg_phase[s] = p->singleBandWaveOrg_phase[s];
        }

    }
    
    if(p->debug)
      fprintf(stderr, "multiplexPulse_initPompArray() done. \n");
    
    return SUCCESS;
};



/********************************************************/
/* get the necesaary slice selection gradient. slcScaling reduces the effective slice thickness*/
/********************************************************/
/*int multiplex_calcSlcSelectAmp(mplx_pulse* p,float slthick, float scaleFactor){

   if(ampslice(&p->slcSelectAmp,p->bw,slthick,scaleFactor,TYPDEF) == FAILURE)
        return FAILURE;

    return SUCCESS;
};
*/

/****************************************************************************/
/* POMP: multiplexing with dedicated phase shifts for individual slices     */
/* mb        : number of slices                                             */
/* slcLoc    : slice locations are taken from mplx_pulse.sliceLocations     */
/* K         : associated K-space value [1/cm] for RF-encoding              */
/* globPhase : adds global phase to the pulse (e.g. fulfill CPMG)           */
/****************************************************************************/
int multiplexPulse_modulate(mplx_pulse* p,int mb,float K,float globalPhase){

    /*fprintf(stderr, "entering multiplex_modulate() \n");*/

    float gambar = 4257; /* [Hz/G ] */
    /*reserve space for duplicated pulses */
    float* singleBand_mag;
    float* singleBand_phase;
    float* multiBand_mag;
    float* multiBand_phase;
    singleBand_mag = (float *)AllocNode(p->resolution*sizeof(float));
    singleBand_phase = (float *)AllocNode(p->resolution*sizeof(float));
    multiBand_mag = (float *)AllocNode(p->resolution*sizeof(float));
    multiBand_phase = (float *)AllocNode(p->resolution*sizeof(float));

    float freq[MAX_MPLX_SLICES];
    float slcPhase[MAX_MPLX_SLICES];
    int t,r;
    float maxVal = 0;


    /*p->orgwave_mag is in short +-32767 */
    for(t = 0;t < p->resolution;t++){
        singleBand_mag[t] = (float)p->singleBandWaveOrg_mag[t]/(float)MAX_PG_WAMP; /*[-1 1] now */
        singleBand_phase[t] = ( (float) p->singleBandWaveOrg_phase[t] / (float)MAX_PG_WAMP )*PI; /*[-pi pi] now */
    }

    /*get frequency and phase offsets*/
    for(r=0;r<mb;r++){

        /*convert to offresonance frequency in Hertz */
        freq[r] = gambar*p->sliceLocations[r]*(p->slcSelectAmp * p->slcScaling); /* [Hz/G] * [cm] * [G/cm] = Hz */

        /*additional phase due to RF encoding for a given K */
        slcPhase[r] = p->isoSliceLocations[r]*K*2*PI; /*[cm]*[1/cm]*2*PI = rad; */

        /*slice independent global phase (maintain CPMG) */
       slcPhase[r] += globalPhase;

        if (p->useWongPhases == 1) /*get optimal phases to reduce peakB1*/
            slcPhase[r] += afWONG_PHASES[mb-1][r];

        #ifdef _DEBUG_MPLX
            fprintf(stderr,"K-space value : %f [1/cm] \n",K);
            fprintf(stderr,"slice location [%i] : %f [cm] \n",r,p->sliceLocations[r]);
            fprintf(stderr,"slice freq [%i] : %f [Hz] \n",r,freq[r]);
            fprintf(stderr,"slice phase [%i] : %f [rad] \n",r,slcPhase[r]);
        #endif
    }


    /*do the actual multiplexing */
    float dwell;
    dwell = ((float)p->dwell)/(1000.0*1000.0); /*[s]*/
    multiplex_waveform( multiBand_mag,
                        multiBand_phase,
                        singleBand_mag,
                        singleBand_phase,
                        freq,
                        slcPhase,
                        mb,
                        p->resolution,
                        dwell);

    /*get new maximum value of waveform */
    for(t = 0;t < p->resolution;t++){
        if (multiBand_mag[t] > maxVal)
            maxVal = multiBand_mag[t];
    }

    /*bring back waveform to [-1 1] in order to match new maxB1 */
    for(t = 0;t < p->resolution;t++){
        multiBand_mag[t] /= maxVal;
    }

    /*convert wave_form back to short and +32767 */
    float tmp;
    for(t = 0;t < p->resolution;t++){
        tmp = 2*floor(multiBand_mag[t]*(MAX_PG_WAMP-1)/2.0+0.5);
        p->iwave_mag[t] = (short) tmp;
        tmp = 2*floor((multiBand_phase[t]/(PI))*(MAX_PG_WAMP-1)/2.0+0.5);
        p->iwave_phase[t] = (short)tmp;

    }
    p->iwave_mag[p->resolution-1]=WEOS_BIT;
    p->iwave_phase[p->resolution-1]=WEOS_BIT;
    /*float2short(1.0,multiBand_mag,p->iwave_mag,p->resolution);*/
    /*float2short(PI,multiBand_phase,p->iwave_phase,p->resolution);*/

    /*export variables */
    p->isMultiplexed = 1;
    p->mplxFactor = mb;
    if(mb > 1)
        p->sliceSeparation = p->sliceLocations[1] - p->sliceLocations[0];
    else
        p->sliceSeparation = -1.0;

    p->K = K;
    p->maxB1 = maxVal*p->maxB1_org; /*max(single_band) = 1 ! */

    /*multiplexPulse_print(p);*/
    FreeNode(singleBand_mag);
    FreeNode(singleBand_phase);

    return SUCCESS;
}

/****************************************************/
/* multiplex_waveform;                              */
/* float* mplxMag = return multiplexed waveform (magnitude) (O) */
/* float* mplxPhase = return phase (O) */
/* float* single_mag = single band input waveform (I) */
/* freq = array of frequency shifts for each band [Hz] (I) */
/* float* phaseOffset = global phase offsets for each slice [rad] (I) */
/* int mb = how many multiplexed slices (I) */
/* int tpoints = time points in waveform (I) */
/* dwell = sample spacing [s] (I) */
/*************************************************/
int multiplex_waveform(float* mplxMag,float* mplxPhase,float* single_mag,float* single_phase,float* freq,float* phaseOffset, int mb,int tpoints,float dwell){


    if(mb>MAX_MPLX_SLICES)
        return FAILURE;
    if(tpoints > MAX_MPLX_RESOLUTION)
        return FAILURE;

    int t,k;
    float* wave_re;
    float* wave_imag;
    wave_re = (float *)AllocNode(tpoints*sizeof(float));
    wave_imag = (float *)AllocNode(tpoints*sizeof(float));
    float phaseMod;

    /*set waves to zero */
    for(t = 0;t < tpoints;t++){
        wave_re[t]   = 0.0;
        wave_imag[t] = 0.0;
    }

    /*calculate and apply modulation */
    for(t = 0;t < tpoints;t++){
        for(k=0;k<mb;k++){

            /*off resonance phase ramp along each pulse in radians */
            phaseMod = (2*PI*freq[k]*t*dwell) + phaseOffset[k];

            /*modulate and add up rf pulses */
            wave_re[t]   += (single_mag[t] * cos(single_phase[t] +  phaseMod));
            wave_imag[t] += (single_mag[t] * sin(single_phase[t] +  phaseMod));
        }

        /*convert to magnitude and phase*/
        mplxMag[t] = sqrt(wave_re[t]*wave_re[t] + wave_imag[t]*wave_imag[t]);
        mplxPhase[t] = atan2(wave_imag[t],wave_re[t]); /*return value -pi <ret <=pi */

    }

    /*free memory */
    FreeNode(wave_re);
    FreeNode(wave_imag);

    return SUCCESS;
}


/****************************************************************************/
/*link rf pulse to RFPULSE structure for B1-scaling in predownload          */
/****************************************************************************/
STATUS multiplexPulse_link_rfpulse(mplx_pulse* p,RF_PULSE* rfpulse,int slot){

    /*do we rely on rfpulse as a global variable ?? Not any more. */

    /*set rf-slot */
    p->rfslot = slot;

    rfpulse[slot].pw =(int *)  &(p->pw);   /* int *pw;  pulse width */
    rfpulse[slot].amp = (float *) &(p->a_RF);  /* float *amp;      amplitude */
    rfpulse[slot].act_fa = (float *) &(p->alpha);  /*float *act_fa;   desired flip angle */
    rfpulse[slot].res = (int *) &(p->resolution); /* pulse resolution *//* vmx 3/13/95 YI changed from short to int */
    rfpulse[slot].exciter = (int *) &(p->exciter); /* waveform generator (e.g., TYPRHO1 or TYPRHO2) */

    /*SAR relevant parameters from rf-pulse */
    rfpulse[slot].abswidth      = p->SAR.abs_width; /* calculated RF absolute width */
    rfpulse[slot].effwidth      = p->SAR.eff_width; /* calculated RF effective width */
    rfpulse[slot].area          = p->SAR.area;     /* zero momemt of pulse */
    rfpulse[slot].dtycyc        = p->SAR.dty_cycle;   /* max % of pulse width above 23% and in any 1  lobe */
    rfpulse[slot].maxpw         = p->SAR.max_pw;    /* max % of time rf pulse is above 0 amp */
    rfpulse[slot].num           = 1;        /* quantity of this type of RF pulse */
    rfpulse[slot].max_b1        = p->maxB1;   /* peak B1 in G under standard conditions */
    rfpulse[slot].max_int_b1_sq = p->SAR.max_integral_b1_sqr;   /* peak integral B1^2 in GG-msec under standard conditions */
    rfpulse[slot].max_rms_b1    = p->SAR.max_rms_b1;      /* peak RMS B1 in G under standard conditions */
    rfpulse[slot].nom_fa        = p->nom_alpha;   /* flip angle on resonance under standard conditions. */

    rfpulse[slot].nom_pw = (float) p->nom_pw;   /* pulse duration under standard conditions */
    rfpulse[slot].nom_bw = p->nom_bw;   /* pulse bandwidth under standard conditions */

    rfpulse[slot].activity  = PSD_MPS2_ON+PSD_APS2_ON+PSD_SCAN_ON; /* bitmask showing when pulse is used  PSD_PULSE_OFF*/
    rfpulse[slot].reference = (char ) 0; /* flag for pulse used in TG setting */
    rfpulse[slot].isodelay  = 0; /* time in usec from the zero phase reference of pulse to its end*/
    rfpulse[slot].scale     = 0.0;    /* duty cycle scale factor */
    rfpulse[slot].extgradfile = TRUE; /* TRUE = rfpulse uses external gradient waveform file */


    return SUCCESS;
};


/*************************************************/
/* RFSTAT: reverse engineering ! */
/*************************************************/
int multiplexPulse_rfstat(mplx_pulse* p){

    float nom_fa; /*we assume that this does not change */
    int nom_pw; /*pulse width microsec */
    int number; /*resolution */
    int i;
    /*int temp_pw;*/
    double area;
    double abs_width;
    double eff_width;
    double max_point;
    double dty_cycle;
    double max_pw;
    double sum_aj;
    double ai_sqr;
    double max_b1;
    double max_integral_b1_sqr;
    double max_rms_b1;
    double pw_sec;
    /*double sinc_cyc;*/

    sum_aj = ai_sqr = max_b1 = max_integral_b1_sqr = max_rms_b1 = 0;
    max_point = area = abs_width = eff_width = 0;
    dty_cycle = max_pw =  1.0;

    /*get information from mplx_pulse structure */
    number = p->resolution;
    nom_pw = p->nom_pw; /*[microsec]*/
    nom_fa = p->nom_alpha;

    float* wave_form_mag;
    wave_form_mag = (float *)AllocNode(p->resolution*sizeof(float));

    /*transform pulse to [-1, 1] range in floats*/
    short2float(p->iwave_mag, wave_form_mag, p->resolution);


    /*perform the integration */
    for (i = 0; i < p->resolution; i++)
    {
        sum_aj += (double) wave_form_mag[i];
        ai_sqr += SQUARE(wave_form_mag[i]);
        abs_width += fabs((double) wave_form_mag[i]);
        if (fabs((double) wave_form_mag[i]) > max_point)
        {
            max_point = fabs((double) wave_form_mag[i]);
        }
    }

    /*scaling */
    area = sum_aj/(p->resolution);
    /*area = sum_aj/(p->resolution*MAX_PG_AMP); since our waveform is between [-1 1], just get rid of MAX_PG_AMP*/
    abs_width /= (p->resolution);
    eff_width = ai_sqr/(p->resolution);
    dty_cycle /= p->resolution;
    max_pw    /= p->resolution;

    #ifdef DEBUG_RFSTAT
    /*fprintf(stderr,"max_point %f  \n",max_point);
    fprintf(stderr,"max_b1 %f [Gauss]  \n",max_point*p->maxB1);
    fprintf(stderr,"area %f [Gauss]  \n",area);
    fprintf(stderr,"abs_width %f [Gauss]  \n",abs_width);
    fprintf(stderr,"eff_width %f [Gauss]  \n",eff_width);*/
    #endif

    /* New features */
    pw_sec = (double) nom_pw / 1.0e+06;
    /*skip these two lines and assume that we already know max b1 */
    /*max_b1 = (double) nom_fa*p->resolution*max_point/((double)360.0*GAMMAP*pw_sec*sum_aj); */
    max_b1 *= 0.01;   /* conversion from (uT) to Gauss */
    max_b1 = max_point*p->maxB1;



    max_integral_b1_sqr = SQUARE(nom_fa)*number*ai_sqr/(SQUARE(360.0*GAMMAP*sum_aj)*pw_sec);
    max_integral_b1_sqr *= 0.1;  /* conversion from uT to G */
    /*fprintf(stderr," Rho alone maximum integral B1^2 is %f \n", max_integral_b1_sqr);*/
    max_integral_b1_sqr = SQUARE(max_b1) * pw_sec * eff_width*1000;

    max_rms_b1 = nom_fa*sqrt((double)number*ai_sqr)/(360.0*GAMMAP*sum_aj*pw_sec);
    max_rms_b1 *= 0.01; /* conversion from uT to G */
    /*fprintf(stderr," Rho alone maximum RMS B1 is %g \n", max_rms_b1);*/
    max_rms_b1 = max_b1*sqrt(ai_sqr/(max_point*max_point*(double)number));

    /* Output information */
    #ifdef DEBUG_RFSTAT
    fprintf(stderr," Resolution Points: %5d\n",p->resolution);
    fprintf(stderr," The area is %7.4f \n", area);
    fprintf(stderr," The absolute width is %7.4f \n", abs_width);
    fprintf(stderr," The effective width is %7.4f \n", eff_width);
    fprintf(stderr," The duty cycle is %7.4f \n", dty_cycle);
    fprintf(stderr," The maximum pulse width is %7.4f \n", max_pw);
    fprintf(stderr," The maximum B1 is %g \n", max_b1);
    fprintf(stderr," The maximum integral B1^2 is %g \n", max_integral_b1_sqr);
    fprintf(stderr," The maximum RMS B1 is %g \n", max_rms_b1);
    #endif


    p->SAR.area = (float) area;
    p->SAR.abs_width = (float) abs_width;
    p->SAR.eff_width = (float) eff_width;
    p->SAR.dty_cycle = 1.0;
    p->SAR.max_pw = 1.0;
    p->SAR.max_b1 = (float) max_b1;
    p->SAR.max_integral_b1_sqr = (float) max_integral_b1_sqr;
    p->SAR.max_rms_b1 = (float) max_rms_b1;

    return SUCCESS;
}

/***************************************************/
/* SLICELOCS: reorder slice ordering & acquisition
 * --------------------------------------------------
 * mplx_pulse* p->sliceLocations = slice locations for lowest mb-group [cm] (O)
 * SCAN_INFO pscan_info = prescribed imaging volume (I)
 * nsltot = total number of slices in volume (I)
 * mb_factor = THE multiband factor (I)
 */
/***************************************************/
int multiplex_sliceLocs(mplx_pulse* p,SCAN_INFO* pscan_info,int nsltot,int mb_factor){

    int deltaSlice;
    float cog = 0.0; /*center-of-gravity of slice group */
    int l;
    int writeFile = 1;

    /*default settings for single slice excitation*/
    if(nsltot == 1){
        p->sliceLocations[0] =  (pscan_info[0].optloc)/10.0;
        p->isoSliceLocations[0] = 0.0;
        p->sliceSeparation = 0.0;
        p->FOV = -1.0;

        if(mb_factor == 1){
            /*fprintf(stderr, ".sliceSeparation SUCCESS [cm]:  %f \n", p->sliceSeparation);*/
            return SUCCESS; /*everthing is fine*/}
        else
            return FAILURE; /*mb excitation for single slice not supported */
    }

    /*make sure mulit-band groups fit into total volume */
    if((nsltot % mb_factor == 0) && (nsltot >= mb_factor)){

        /*start with lowest slice and increase by mb-factor*/
        deltaSlice = nsltot/mb_factor; /*this is also the number of shots */

        for(l=0;l<mb_factor;l++){
            p->sliceLocations[l] = (pscan_info[l*deltaSlice].optloc)/10.0; /*scan_info = slice loc along z in logical frame [mm]*/
            cog+=p->sliceLocations[l];
        }

        /*get set of slice locations around the isocenter  */
        cog/=mb_factor;
        for(l=0;l<mb_factor;l++){
            p->isoSliceLocations[l] = p->sliceLocations[l] - cog;
        }

        /*calculate rest of mplx-geometry */
        if (mb_factor > 1){
            p->sliceSeparation = ABS(p->sliceLocations[1] - p->sliceLocations[0]);
            p->FOV = mb_factor*p->sliceSeparation;
        }

    }else {
        return FAILURE;
    }


    /*calculate rest of mplx-geometry */
    if (mb_factor > 1){
        p->sliceSeparation = ABS(p->sliceLocations[1] - p->sliceLocations[0]);
        p->FOV = mb_factor*p->sliceSeparation;
    }
    /*returns lowest slice in slice group. In case of nsltot == 1 should return a slice in the isocenter */
    else{
        p->sliceLocations[0] =  (pscan_info[0].optloc)/10.0;
        p->isoSliceLocations[0] = 0.0;
        p->sliceSeparation = 0.0;
        p->FOV = -1.0;
    }


    /*write the slice information to a file */
    if(writeFile){
        FILE* asciiwavefile;
        char fname[80];
        sprintf(fname,"%s","mplx_sliceLocs.txt");
        asciiwavefile = fopen(fname,"w");
        for(l=0;l<mb_factor;l++){
            fprintf(asciiwavefile, "slcLocMplx[%i]: %f [cm]\n",l,p->sliceLocations[l]);
            fprintf(asciiwavefile, "slcLocMplx_iso[%i]: %f [cm]\n",l,p->isoSliceLocations[l]);
        }
        for(l=0;l<nsltot;l++){
            fprintf(asciiwavefile, "allSlices[%i]: %f [cm] \n",l,(pscan_info[l].optloc)/10.0);
        }

        if (asciiwavefile != NULL) {
            fclose(asciiwavefile);
        }
    }

    return SUCCESS;
}

/*************************************************/
/* PRINT: display pulse information              */
/*************************************************/
int multiplexPulse_print(mplx_pulse* p){
    fprintf(stderr, "//**********************************// : \n");
    fprintf(stderr, "mplx_pulse info (%s): \n",p->name);
    fprintf(stderr, ".resolution: %i \n", p->resolution);
    fprintf(stderr, ".slcSelectAmp [G/cm]:  %f \n", p->slcSelectAmp);
    fprintf(stderr, ".mplxFactor:  %i \n", p->mplxFactor);
    /*fprintf(stderr, ".sliceSeparation [cm]:  %f \n", p->sliceSeparation);
    fprintf(stderr, ".isoSliceLocations[0]: %f \n", p->isoSliceLocations[0]);
    fprintf(stderr, ".SliceLocations[0]: %f \n", p->sliceLocations[0]);*/
    fprintf(stderr, ".maxB1 [G]:  %f \n", p->maxB1);
    fprintf(stderr, ".nom_pw [us]:  %i \n", p->nom_pw);
    fprintf(stderr, ".pw [us]:  %i \n", p->pw);
    fprintf(stderr, ".nom_bw [Hz]:  %f \n", p->nom_bw);
    fprintf(stderr, ".bw [Hz]:  %f \n", p->bw);
    fprintf(stderr, "//**********************************// : \n");

    return 1;
}

/*************************************************/
/* EXPORT: write the multiplex waveform to a file*/
/*************************************************/
int multiplexPulse_export(mplx_pulse* p){

    FILE *asciiwavefile;

    char fname[80];
    sprintf(fname,"%s.%s",p->name,"txt");
    asciiwavefile = fopen(fname,"w");

    if (asciiwavefile == NULL) {
       fprintf(stderr,"ERROR: unable to open ASCII waveform file\n");
       fflush(stderr);
    }

    if (asciiwavefile != NULL) {

        /*print header information */
        fprintf(asciiwavefile, "mplx_pulse info (%s): \n",p->name);
        fprintf(asciiwavefile, ".slcSelectAmp [G/cm]: %f \n", p->slcSelectAmp);
        fprintf(asciiwavefile, ".mplxFactor: %i \n", p->mplxFactor);
        fprintf(asciiwavefile, ".sliceSeparation [cm]: %f \n", p->sliceSeparation);
        fprintf(asciiwavefile, ".K [1/cm]:  %f \n", p->K);
        fprintf(asciiwavefile, ".useWongPhases: %i \n", p->useWongPhases);
        fprintf(asciiwavefile, ".maxB1 [G]:  %f \n", p->maxB1);

        /* print actual wave */
        fprintf(asciiwavefile,"%s %s %s\n","t", "abs","phase");
        int t;
        for(t=0;t<p->resolution;t++){
            fprintf(asciiwavefile,"%d %d %d\n",t, p->iwave_mag[t], p->iwave_phase[t]);
            fflush(asciiwavefile);
        }

    }

    if (asciiwavefile != NULL) {
       fclose(asciiwavefile);
    }

    return 1;
};

/*************************************************/
/* helper functions                              */
/*************************************************/
int float2short(float maxValFloat, float* f,int* i,int res){

    float tmp;
    int t;
    for(t = 0;t < res;t++){
        tmp = 2*floor((f[t]/(maxValFloat))*(MAX_PG_WAMP)/2.0+0.5);
        i[t] = (short) tmp;
    }
    return SUCCESS;
}


int short2float(int* i, float* f, int res){

    /* Aufgemerkt!*/
    /* MAX_PG_IAMP =  32767*/
    /* MAX_PG_WAMP =  32766*/

    int t;
    /*p->orgwave_mag is in short +-32766 */
    for(t = 0;t < res;t++){
    f[t] = (float)i[t]/(float)MAX_PG_WAMP; /*[-1 1] now */
    }
    return SUCCESS;
}

/*reverse Byte order*/
short reverseShort(short s){

    unsigned char c1,c2;

    c1 = s & 255;
    c2 = (s >> 8) & 255;

    return (c1 << 8) + c2;

}

/*dump rfpulse-info */
void dump_rfpulse(RF_PULSE* rfpulse,int slot){

    fprintf(stderr, "//-------rfpulse[slot]------// : \n");
    fprintf(stderr, ".slot: %i \n",slot);
    fprintf(stderr, ".resolution:  %i \n", *(rfpulse[slot].res));
    fprintf(stderr, ".pw:  %i \n", *(rfpulse[slot].pw));
    fprintf(stderr, ".nom_pw:  %f \n", rfpulse[slot].nom_pw);
    fprintf(stderr, ".amp:  %f \n", *(rfpulse[slot].amp));
    fprintf(stderr, ".nom_fa:  %f \n", rfpulse[slot].nom_fa);
    fprintf(stderr, ".act_fa:  %f \n", *(rfpulse[slot].act_fa));
    fprintf(stderr, ".maxB1:  %f \n", rfpulse[slot].max_b1);
    fprintf(stderr, ".nom_bw:  %f \n", rfpulse[slot].nom_bw);
    fprintf(stderr, ".extgradfile:  %i \n", rfpulse[slot].extgradfile);
    fprintf(stderr, ".isodelay:  %i \n", rfpulse[slot].isodelay);
    

}

#endif
