/*
 *  sbb_wait.e
 *
 *  This file contains all the external functions
 *  defined in exc_sbb.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include "sbb_wait.h"



/**********************************************/
@host _ipgexport



/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
/*#include <mplx_rfpulse.c>*/

void sbb_wait_init(sbb_wait* p){

    initWF_PULSE(&p->rho);
    initWF_PULSE(&p->theta);
    initWF_PULSE(&p->omega);
    initWF_PULSE(&p->ssp);
    initWF_PULSE(&p->gx);
    initWF_PULSE(&p->gy);
    initWF_PULSE(&p->gz);

    /*timing*/
    p->start_time       = 0;
    p->total_time       = 1000;
    p->max_duration		= 1000000;
};

/*helpfer function to initialize WF_PULSE*/
#ifndef initWF_PULSE_h
#define initWF_PULSE_h
void initWF_PULSE(WF_PULSE* pwf){
    pwf->pulsename = "<uninitialized>";
    pwf->ninsts = 0;
    pwf->wave_addr = -1;
    pwf->board_type = PSDCERD;
    pwf->reusep =  PSDREUSEP;
    pwf->tag =  SSPUNKN;
    pwf->addtag =  0;
    pwf->id =  0;
    pwf->ctrlfield =  0;
    pwf->inst_hdr_tail =  NULL;
    pwf->inst_hdr_head =  NULL;
    pwf->wavegen_type =TYPXGRAD;
    pwf->type =  TYPBITS;
    pwf->resolution = 0;
    pwf->assoc_pulse = NULL;
    pwf->ext =  NULL;
};
#endif

STATUS sbb_wait_cveval(sbb_wait* psbb)
{

    return SUCCESS;
}

STATUS sbb_wait_predownload(void){


    return SUCCESS;
};




/*************************************************/
@pg _pg

STATUS sbb_wait_pulsegen(sbb_wait* psbb,CHAR *sbb_name,int start_time,int duration)
{
  
    psbb->start_time = start_time;
    if(duration < GRAD_UPDATE_TIME)
	psbb->total_time = GRAD_UPDATE_TIME;
    else
      psbb->total_time = RUP_GRD(duration);

    pulsename(&psbb->rho,sbb_name);
    createconst(&psbb->rho,RHO,psbb->total_time,(short)0); 
    createinstr( &psbb->rho,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->theta,sbb_name);
    createconst(&psbb->theta,THETA,psbb->total_time,(short)0); 
    createinstr( &psbb->theta,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->omega,sbb_name);
    createconst(&psbb->omega,OMEGA,psbb->total_time,(short)0); 
    createinstr( &psbb->omega,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->ssp,sbb_name);
    createconst(&psbb->ssp,SSP,psbb->total_time,(short)0); 
    createinstr( &psbb->ssp,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->gx,sbb_name);
    createconst(&psbb->gx,XGRAD,psbb->total_time,(short)0); 
    createinstr( &psbb->gx,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->gy,sbb_name);
    createconst(&psbb->gy,YGRAD,psbb->total_time,(short)0); 
    createinstr( &psbb->gy,(long)(psbb->start_time),psbb->total_time,0);

    pulsename(&psbb->gz,sbb_name);
    createconst(&psbb->gz,ZGRAD,psbb->total_time,(short)0); 
    createinstr( &psbb->gz,(long)(psbb->start_time),psbb->total_time,0);

    return SUCCESS;
};




/***********************************/
@rsp _rsp
void sbb_wait_psdinit(sbb_wait* psbb){


};




void sbb_wait_setperiod(sbb_wait* psbb,long duration){

  /*some checks*/
  /*if(duration > psbb->max_time){
    duration = (long)psbb>max_time;
  }
  if(duration == 0){
    duration = GRAD_UPDATE_TIME
  }*/
    if(duration < GRAD_UPDATE_TIME)
      psbb->total_time = GRAD_UPDATE_TIME;
    else
      psbb->total_time = RUP_GRD(duration);
    
    setperiod(psbb->total_time, &psbb->rho, 0);
    setperiod(psbb->total_time, &psbb->theta, 0);
    setperiod(psbb->total_time, &psbb->omega, 0);
    setperiod(psbb->total_time, &psbb->ssp, 0);
    setperiod(psbb->total_time, &psbb->gx, 0);
    setperiod(psbb->total_time, &psbb->gy, 0);
    setperiod(psbb->total_time, &psbb->gz, 0);

  /*return SUCCESS;*/

};

