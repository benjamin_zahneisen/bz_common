/*
 *  exc_sbb.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in exc_sbb.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Jan/2015
 *
 */

#ifndef exc_sbb_h
#define exc_sbb_h

/* @global functions*/
#ifndef MAX
  #define MAX(x,y) ( (x>y) ? x : y )
#endif
#ifndef MIN
  #define MIN(x,y) ( (x<y) ? x : y )
#endif

#define CEIL(x) ( ((x-(int)x) > 0) ? ((int)x+1) : ((int)x) )


#include "mplx_rfpulse.h"

#ifdef RTMOCO
#endif

/*enable/disable support for POMP rf-encoding of multiple slices*/
#define POMP

/*rf excitation sequence building block */
typedef struct sbb_exc{
        mplx_pulse* prfpulse;
        mplx_pulse* prfpulse_pomp[MAX_MPLX_SLICES];
        WF_PULSE rf;
        WF_PULSE rf_theta;
        WF_PULSE gzrfa;
        WF_PULSE gzrf;
        WF_PULSE gzrfd;
        WF_PULSE gzrfdepa;
        WF_PULSE gzrfdep;
        WF_PULSE gzrfdepd;
        WF_PULSE gzrfrepa;
        WF_PULSE gzrfrep;
        WF_PULSE gzrfrepd;

        /*dynamic arrays for rf-pulses*/
        #ifdef POMP
        WF_PULSE* pomp_wf;
        WF_PULSE* pomp_wf_theta;
        WF_HW_WAVEFORM_PTR* wave_ptr;
        WF_HW_WAVEFORM_PTR* wave_theta_ptr;
        int Nwaves;
        #endif

        /*control variables */
        int ia_rfExt;
        float a_rfExt;
        int ia_rfExtTheta;
        float a_rfExtTheta;
        float a_gzrf;
        int ia_gzrf;
        float a_grfrep;
        int ia_grfrep;
        float slcThickness;
        int playSlcReph;

        /*SAR */
        float alpha; /*flip angle in deg */
        int RF_SLOT; /*slot in rfpulse[] structure*/
        int exciter; /*which exciter RHO1 or RHO2*/
        int Ninstances; /*how often do we employ that pulse per entry point */
        float B1scaling[MAX_MPLX_SLICES];

        /*timing*/
        int start_time; /*all other timing is relative to the sbb start time */
        int total_time; /*duration of sbb */
        int time_from_RFcenter; /*sbb duration measured from peak of rf pulse */
        int time_to_RFcenter; /*sbb duration measured from peak of rf pulse */
        int psd_rf_wait;
        int psd_grd_wait;
        int pw_rfExt;
        int pw_gzrf;
        int pw_gzrfa;
        int pw_gzrfd;
        int pw_grfrepa;
        int pw_grfrep;
        int pw_grfrepd;

        int instance; /*add number to base name*/
        int debugstate;

} sbb_exc;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_exc_cveval(sbb_exc* psbb,mplx_pulse* prf);
STATUS sbb_exc_predownload(void);
STATUS sbb_exc_link_rfpulse(sbb_exc* psbb,int slot);

/*
 * @pg functions
 */
STATUS sbb_exc_pulsegen(sbb_exc* psbb,CHAR *sbb_name,mplx_pulse* prf,int start_time);
#ifdef POMP
STATUS sbb_exc_pulsegen_pomp(sbb_exc* psbb);
#endif

/*************************/
/* @rsp functions */
/*************************/
void sbb_exc_psdinit(sbb_exc* psbb);
void sbb_exc_setfrequency(sbb_exc* psbb,long freq);
void sbb_exc_setphase(sbb_exc* psbb,double phase);
void sbb_exc_setwave(sbb_exc* psbb,int num);

#endif /*exc_sbb_h */
