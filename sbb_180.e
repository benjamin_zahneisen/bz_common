/*
 *  exc_sbb.e
 *
 *  This file contains all the external functions
 *  defined in exc_sbb.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2015
 *
 */

/**********************************************/
@global _global
#include "mplx_rfpulse.h"
#include "sbb_180.h"




/**********************************************/
@host _ipgexport
mplx_pulse mplxRFPulse;
sbb_refoc SBB180;


/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
#include "sbb_180.h"

void sbb_refoc_init(sbb_refoc* p){
    p->prfpulse = NULL;
    int i;
    for(i=0;i<MAX_MPLX_SLICES;i++){
        p->prfpulse_pomp[i] = NULL;
        p->B1scaling[i] = 1.0;
    }
    initWF_PULSE(&p->rf);
    initWF_PULSE(&p->rf_theta);
    initWF_PULSE(&p->gzrfa);
    initWF_PULSE(&p->gzrf);
    initWF_PULSE(&p->gzrfd);

    initWF_PULSE(&p->gzcrushla);
    initWF_PULSE(&p->gzcrushl);
    initWF_PULSE(&p->gzcrushld);
    initWF_PULSE(&p->gzcrushra);
    initWF_PULSE(&p->gzcrushr);
    initWF_PULSE(&p->gzcrushrd);

    initWF_PULSE(&p->gxcrushla);
    initWF_PULSE(&p->gxcrushl);
    initWF_PULSE(&p->gxcrushld);
    initWF_PULSE(&p->gxcrushra);
    initWF_PULSE(&p->gxcrushr);
    initWF_PULSE(&p->gxcrushrd);

    initWF_PULSE(&p->gycrushla);
    initWF_PULSE(&p->gycrushl);
    initWF_PULSE(&p->gycrushld);
    initWF_PULSE(&p->gycrushra);
    initWF_PULSE(&p->gycrushr);
    initWF_PULSE(&p->gycrushrd);

    /*control variables */
    p->ia_rfExt         = MAX_PG_IAMP/2;
    p->ia_rfExtTheta    = MAX_PG_IAMP/2;
    p->ia_gzrf          = MAX_PG_IAMP/2;
    p->ia_gcrush	= MAX_PG_IAMP/2;
    p->a_gzrf           = 0;
    p->slcThickness     = 0;
    p->axis             = ZGRAD;
    p->debug            = 0;

    p->alpha            = 180.0;  /*flip angle in deg */
    p->RF_SLOT          = 1; /*slot in rfpulse[] structure*/
    p->exciter          = TYPRHO1; /*which exciter RHO1 or RHO2*/
    p->Ninstances       = 0;
    p->stretchFactor    = 1.0;
    /*pomp and single band waves*/
    p->Nwaves		= 1;

    /*timing*/
    p->start_time       = 0;
    p->total_time       = 5000;
    p->psd_rf_wait      = 0; /*(long) get_rf_dly();*/
    p->psd_grd_wait     = 0; /*(long) get_grad_dly();*/ /*52 seems reasonable*/
    p->pw_rfExt         = 2560;
    p->pw_gzrf          = 2560;
    p->pw_gzrfa         = 300;
    p->pw_gzrfd         = 300;
    p->pw_gcrusha	= 300;
    p->pw_gcrush	= 1280;
    p->pw_gcrushd	= 300;
    p->revgzcrush	= 8.0;

    /* psd_grd_wait:  delay between waveform appearing on gradient sequencer and production of physical gradient */
    /* psd_rf_wait = delay between gradient and RF output */
};

/*helpfer function to initialize WF_PULSE*/
#ifndef initWF_PULSE_h
#define initWF_PULSE_h
void initWF_PULSE(WF_PULSE* pwf){
    pwf->pulsename = "<uninitialized>";
    pwf->ninsts = 0;
    pwf->wave_addr = -1;
    pwf->board_type = PSDCERD;
    pwf->reusep =  PSDREUSEP;
    pwf->tag =  SSPUNKN;
    pwf->addtag =  0;
    pwf->id =  0;
    pwf->ctrlfield =  0;
    pwf->inst_hdr_tail =  NULL;
    pwf->inst_hdr_head =  NULL;
    pwf->wavegen_type =TYPXGRAD;
    pwf->type =  TYPBITS;
    pwf->resolution = 0;
    pwf->assoc_pulse = NULL;
    pwf->ext =  NULL;
};
#endif

STATUS sbb_refoc_cveval(sbb_refoc* psbb)
{
    if(psbb->debug)
      fprintf(stderr, "welcome to sbb_refoc_cveval()\n");

    if(psbb->prfpulse == NULL){
        fprintf(stderr, "sbb_refoc_cveval() error: No rfpulse specified! \n");
        return FAILURE;
     }

    float target;
    int rtime,ftime;
    float areagzcrush;

    gettarget(&target,(WF_PROCESSOR) psbb->axis, &loggrd);
    getramptime(&rtime, &ftime,(WF_PROCESSOR) psbb->axis, &loggrd);

    /*store slice thickness from rf-pulse*/
    psbb->slcThickness = psbb->prfpulse->slcThickness;

    /*get slice selection parameters from rf-pulse*/
    psbb->a_gzrf = psbb->prfpulse->slcSelectAmp*psbb->prfpulse->slcScaling;


    if (optramp(&psbb->pw_gzrfa, psbb->a_gzrf, loggrd.tz, loggrd.zrt, TYPDEF) == FAILURE){
        fprintf(stderr, "psbb->a_gzrf1: %f [G/cm] \n",psbb->a_gzrf);
        fprintf(stderr, "optramp attack in exc_sbb_cveval() failed.\n");
        return FAILURE;
    }

    if (optramp(&psbb->pw_gzrfd, psbb->a_gzrf, loggrd.tz, loggrd.zrt, TYPDEF) == FAILURE){
        fprintf(stderr, "optramp decay in exc_sbb_cveval() failed.\n");
        return FAILURE;
    }

    /*calculate area of slice select gradient */ 
    psbb->ia_gzrf = psbb->a_gzrf * MAX_PG_IAMP / loggrd.tz;
    /*float arearf1 = psbb->a_gzrf * ((float)psbb->pw_rfExt + (float)psbb->pw_gzrfa);*/


    /* crusher gradients */
    areagzcrush = 2 * psbb->revgzcrush * 10000000.0 / GAM / (float)psbb->slcThickness;

    if( amppwgrad(areagzcrush,loggrd.tz_xyz, 0.0, 0.0, loggrd.zrt, MIN_PLATEAU_TIME, &psbb->a_gcrush, &psbb->pw_gcrusha, &psbb->pw_gcrush,
                  &psbb->pw_gcrushd)
        == FAILURE){
        return FAILURE;
    }

    /*let crusher gradient have the same polarity as slice select gradient*/
    if(psbb->a_gzrf < 0.0)
        psbb->a_gcrush = -psbb->a_gcrush;

    psbb->ia_gcrush = (psbb->a_gcrush / loggrd.tz) * MAX_PG_IAMP; /*BZ back to .tz instead of .tz_xyz. not sure why */ 

    /*right crusher same width as left crusher*/


    /*set the pulse width*/
    psbb->pw_rfExt = (psbb->prfpulse->pw);
    psbb->pw_gzrf = psbb->pw_rfExt;

    /*set timing of sbb */
    psbb->total_time =  psbb->pw_gzrfa
                        + psbb->pw_gzrf
                        + psbb->pw_gzrfd
                        + 2*(psbb->pw_gcrusha + psbb->pw_gcrush + psbb->pw_gcrushd);


    psbb->time_from_RFcenter =  psbb->pw_gzrf/2
                                + psbb->pw_gzrfa
                                + (psbb->pw_gcrusha + psbb->pw_gcrush + psbb->pw_gcrushd);

    /*reset number of instances with every call of cveval*/
    psbb->Ninstances = 0;

    return SUCCESS;
}

STATUS sbb_refoc_predownload(void){



    return SUCCESS;
};



/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg

#include "mplx_rfpulse.c"


STATUS sbb_refoc_pulsegen(sbb_refoc* psbb,CHAR *sbb_name,mplx_pulse* prf,int start_time)
{

    /*store pointer(s) to RF array*/
    psbb->prfpulse =  &(prf[0]);

    /*store start time */
    psbb->start_time = start_time;

    if(psbb->prfpulse == NULL){
        fprintf(stderr, "sbb_refoc_pulsegen() error: No rfpulse specified! \n");
        return FAILURE;
     }

    /*start with slice selection gradients  */
    int ia_start = 0;
    int ia_end = 0;
    int posSlcSelect;
    int posRightCrusher;
    char name[22]; /*it seems WTools only support 22 characters !??!*/
    long start_rf;


    /*left crusher gradient */
    sprintf(name,"%s_%s",sbb_name,"gzcrl");
    trapezoid(ZGRAD,name, &psbb->gzcrushl, &psbb->gzcrushla, &psbb->gzcrushld,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,psbb->start_time , TRAP_ALL, &loggrd);
    
    sprintf(name,"%s_%s",sbb_name,"gxcrl");
    trapezoid(XGRAD,name, &psbb->gxcrushl, &psbb->gxcrushla, &psbb->gxcrushld,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    sprintf(name,"%s_%s",sbb_name,"gycrl");
    trapezoid(YGRAD,name, &psbb->gycrushl, &psbb->gycrushla, &psbb->gycrushld,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,psbb->start_time, TRAP_ALL, &loggrd);

    posSlcSelect = (psbb->pw_gcrusha + psbb->pw_gcrush + psbb->pw_gcrushd);

    sprintf(name,"%s_%s",sbb_name,"gz");
    trapezoid(ZGRAD,name, &psbb->gzrf, &psbb->gzrfa, &psbb->gzrfd,
              psbb->pw_gzrf, psbb->pw_gzrfa, psbb->pw_gzrfd, psbb->ia_gzrf,
              ia_start, ia_end,0,0 ,psbb->start_time + posSlcSelect, TRAP_ALL, &loggrd);

    posRightCrusher = posSlcSelect + (psbb->pw_gzrfa + psbb->pw_gzrf + psbb->pw_gzrfd);
    
    sprintf(name,"%s_%s",sbb_name,"gzcrr");
    trapezoid(ZGRAD,name, &psbb->gzcrushr, &psbb->gzcrushra, &psbb->gzcrushrd,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,posRightCrusher + psbb->start_time, TRAP_ALL, &loggrd);

    sprintf(name,"%s_%s",sbb_name,"gxcrr");
    trapezoid(XGRAD,name, &psbb->gxcrushr, &psbb->gxcrushra, &psbb->gxcrushrd,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,posRightCrusher + psbb->start_time, TRAP_ALL, &loggrd);
    
    sprintf(name,"%s_%s",sbb_name,"gycrr");
    trapezoid(YGRAD,name, &psbb->gycrushr, &psbb->gycrushra, &psbb->gycrushrd,
              psbb->pw_gcrush, psbb->pw_gcrusha, psbb->pw_gcrushd, psbb->ia_gcrush,
              ia_start, ia_end,0,0 ,posRightCrusher + psbb->start_time, TRAP_ALL, &loggrd);

    psbb->total_time = posSlcSelect +  (psbb->pw_gzrfa + psbb->pw_gzrf + psbb->pw_gzrfd) + (psbb->pw_gcrusha + psbb->pw_gcrush + psbb->pw_gcrushd); 


    /*rf-pulse and theta wave section*/

    /*set pulse name */
    sprintf(name,"%s_%s",sbb_name,"rfmag");
    pulsename(&psbb->rf,name);
    sprintf(name,"%s_%s",sbb_name,"rftheta");
    pulsename(&psbb->rf_theta,name);

    /*reserves space in instruction memory */
    createreserve(&psbb->rf,RHO,psbb->prfpulse->resolution);
    createreserve(&psbb->rf_theta,THETA,psbb->prfpulse->resolution);

    /*convert waveforms in psbb from int to short*/
    short* wave_mag;
    short* wave_phase;
    wave_mag = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    wave_phase = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
    int t;
    for(t=0;t<psbb->prfpulse->resolution;t++){
        wave_mag[t] = (short) psbb->prfpulse->iwave_mag[t];
        wave_phase[t] = (short) psbb->prfpulse->iwave_phase[t];
    }

    /*assign local wave to hardware */
    movewaveimm(wave_mag,&psbb->rf,0,psbb->prfpulse->resolution,TOHARDWARE);
    movewaveimm(wave_phase,&psbb->rf_theta,0,psbb->prfpulse->resolution,TOHARDWARE);

    /*create instruction */
    psbb->ia_rfExtTheta = MAX_PG_IAMP;
    psbb->ia_rfExt = MAX_PG_IAMP*psbb->prfpulse->a_RF; /*get instruction amplitude after B1 scaling*/

    start_rf =  psbb->start_time
                + (psbb->pw_gcrusha + psbb->pw_gcrush + psbb->pw_gcrushd) /*left crusher */
                + psbb->pw_gzrfa;

    createinstr( &psbb->rf,start_rf,(long)psbb->pw_rfExt,(long)psbb->ia_rfExt);
    createinstr( &psbb->rf_theta,start_rf,(long)psbb->pw_rfExt,(long)psbb->ia_rfExtTheta);

    /*this ensures that the last point of the waveform is odd, I guess */
    setweos(EOS_DEAD,&psbb->rf,psbb->prfpulse->resolution-1);
    setweos(EOS_DEAD,&psbb->rf_theta,psbb->prfpulse->resolution-1);

    addrfbits(&psbb->rf,0,start_rf, psbb->pw_rfExt);

    /*keep track of how many times this pulse is played out (=called during pulsegen) */
    psbb->Ninstances++;

    if(psbb->debug)
      fprintf(stderr, "sbb_refoc_pulsegen() finished. \n" );

    return SUCCESS;
};

/***********************************************************/
/* Loads waveforms into memory. No instructions are called.*/
/***********************************************************/
#ifdef POMP
STATUS sbb_refoc_pulsegen_pomp(sbb_refoc* psbb,int Nwaves,mplx_pulse* prf){
	
	if(psbb->debug){
		fprintf(stderr, "entering sbb_refoc_pulsegen_pomp(). \n" );
	}
	
	/*tell sbb how many waveforms to prepare*/
    psbb->Nwaves = Nwaves;

    /* allocate memory for pomp waveforms */
    psbb->pomp_wf = (WF_PULSE *) AllocNode(psbb->Nwaves*sizeof(WF_PULSE));
    psbb->pomp_wf_theta = (WF_PULSE *) AllocNode(psbb->Nwaves*sizeof(WF_PULSE));
    psbb->wave_ptr = (WF_HW_WAVEFORM_PTR *) AllocNode(psbb->Nwaves*sizeof(WF_HW_WAVEFORM_PTR));
    psbb->wave_theta_ptr = (WF_HW_WAVEFORM_PTR *) AllocNode(psbb->Nwaves*sizeof(WF_HW_WAVEFORM_PTR));
    char pnstr[80];
    short* wave_mag;
    short* wave_phase;
    float maxB1Pomp = 0.0;
    int k,t;

    
    /*store pointers to RF array*/
    psbb->prfpulse =  &(prf[0]);
	for(k=0;k<Nwaves;k++){
        psbb->prfpulse_pomp[k] =  &(prf[k]);
    }

	/*convert waveforms in psbb from int to short*/
     wave_mag = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));
     wave_phase = (short *)AllocNode(psbb->prfpulse->resolution*sizeof(short));


    for (k = 0; k < psbb->Nwaves; k++){

        for(t=0;t<psbb->prfpulse->resolution;t++){
            wave_mag[t] = (short) psbb->prfpulse_pomp[k]->iwave_mag[t];
            wave_phase[t] = (short) psbb->prfpulse_pomp[k]->iwave_phase[t];
			
        }

        /*magnitude */
        sprintf(pnstr,"thrf1sl%d",k);
        pulsename(&psbb->pomp_wf[k], pnstr);
        createreserve(&psbb->pomp_wf[k],TYPRHO1,psbb->prfpulse->resolution);
        movewaveimm(wave_mag,&psbb->pomp_wf[k],0,psbb->prfpulse->resolution,TOHARDWARE);
        psbb->wave_ptr[k] = psbb->pomp_wf[k].wave_addr;

        /*phase */
        sprintf(pnstr,"thrf2sl%d",k);
        pulsename(&psbb->pomp_wf_theta[k], pnstr);
        createreserve(&psbb->pomp_wf_theta[k],THETA,psbb->prfpulse->resolution);
        movewaveimm(wave_phase,&psbb->pomp_wf_theta[k],0,psbb->prfpulse->resolution,TOHARDWARE);
        psbb->wave_theta_ptr[k] = psbb->pomp_wf_theta[k].wave_addr;

        /*for each wave we have to keep track of small changes in actual max B1 value */
        psbb->B1scaling[k] = psbb->prfpulse_pomp[k]->maxB1;
        if(psbb->B1scaling[k] > maxB1Pomp)
            maxB1Pomp = psbb->B1scaling[k];
		if(psbb->debug)	
		fprintf(stderr, "what's in the pulse->maxB1Pomp = %f. \n" ,maxB1Pomp);
	}

    for (k = 0; k < psbb->Nwaves; k++){
        psbb->B1scaling[k] /= maxB1Pomp;
	if(psbb->debug)
	    fprintf(stderr,"B1scaling 180 = %f \n",psbb->B1scaling[k]);
    }

	if(psbb->debug)
		fprintf(stderr, "sbb_refoc_pulsegen_pomp() done. \n" );

    FreeNode(wave_mag);
    FreeNode(wave_phase);

    return SUCCESS;
};
#endif

/* end @pg rfov_funcs.e **********************/


/***********************************/
@rsp _rsp
void sbb_refoc_psdinit(sbb_refoc* psbb){

    fprintf(stderr, "sbb_refoc_psdinit(): entered.\n");

    /*loop over all instances*/
    int n;
    for(n=0;n<psbb->Ninstances;n++){
        /*set frequency to zero */
        setfrequency(0.0, &psbb->rf, n);

        /*set amplitudes */
        setiamp(psbb->ia_rfExt, &psbb->rf, n);
        setiamp(MAX_PG_IAMP, &psbb->rf_theta, n);

        /*slice selection and crusher gradient*/
        setiamp(psbb->ia_gzrf, &psbb->gzrf,n);
        setiamp(psbb->ia_gcrush, &psbb->gzcrushl,n);
        setiamp(psbb->ia_gcrush, &psbb->gzcrushr,n);
        setiamp(psbb->ia_gcrush, &psbb->gxcrushl,n);
        setiamp(psbb->ia_gcrush, &psbb->gxcrushr,n);
        setiamp(psbb->ia_gcrush, &psbb->gycrushl,n);
        setiamp(psbb->ia_gcrush, &psbb->gycrushr,n);
    }

    fprintf(stderr, "sbb_refoc_psdinit(): done.\n");

};

void sbb_refoc_setfrequency(sbb_refoc* psbb,long freq){

    #ifdef SIM
        fprintf(stderr, "sbb_refoc_setfrequency(): entering.\n");
    #endif

    /*loop over all instances*/
    long n;
    for(n=0;n < psbb->Ninstances;n++){
        setfrequency(freq, &psbb->rf, n);
    }


};


void sbb_refoc_setphase(sbb_refoc* psbb,double phase){
    /*phase value in radians  (e.g. pi/2)*/

    setphase(phase, &psbb->rf, INSTRALL);
};

/* Sets different waveforms  */
void sbb_refoc_setwave(sbb_refoc* psbb,int num)
{

    if((num > (MAX_MPLX_SLICES-1)) || (num>=psbb->Nwaves )){
        fprintf(stderr,"No wave available for n=%i.\n",num);
        return;
    }

    if(num < 0){
        /*set wave to default single wave pointer*/
        return;
    }

    /*magnitude */
    int ia;
    setwave(psbb->wave_ptr[num],&(psbb->rf),INSTRALL);
    setweos(EOS_DEAD,&(psbb->rf),psbb->prfpulse->resolution-1);
    ia = psbb->ia_rfExt * (psbb->B1scaling[num]);
    /*fprintf(stderr,"ia B1scaling %i \n",ia);*/
    setiamp(ia, &(psbb->rf), INSTRALL); /*where do we get the correct ampliutde ?*/

    /*phase */
    setwave(psbb->wave_theta_ptr[num],&(psbb->rf_theta),INSTRALL);
    setweos(EOS_DEAD,&(psbb->rf_theta),psbb->prfpulse->resolution-1);
    setiamp(MAX_PG_IAMP, &(psbb->rf_theta), INSTRALL);

}

/**********************************************************/
/* Modifiy crusher gradients to prevent unwanted echoes.  */
/**********************************************************/
void sbb_refoc_crusherTheme(sbb_refoc* psbb,int crushertheme){
  
     int ia;
    
    /*make sure we have more than once instance*/
    if(psbb->Ninstances == 1){
      fprintf(stderr,"Only one 180deg pulse present n=%i.\n",psbb->Ninstances);
      return;
    }	

    if (crushertheme==0){
	/*do nothing */
        return;
    }  


    if (crushertheme==1){/*for dual echo*/
	fprintf(stderr,"Crusher theme %i.\n",crushertheme);
	/* flip sign of x-crusher*/
	setiampt(-psbb->ia_gcrush,&psbb->gxcrushl,1);
	setiampt(-psbb->ia_gcrush,&psbb->gxcrushr,1);
	setiampt(-psbb->ia_gcrush,&psbb->gycrushl,1);
	setiampt(-psbb->ia_gcrush,&psbb->gycrushr,1);
	setiampt(-psbb->ia_gcrush,&psbb->gzcrushl,1);
	setiampt(-psbb->ia_gcrush,&psbb->gzcrushr,1);
	
    }
    
    if (crushertheme==2){
	    /* flip sign of all crusher gradients AND lower amplitude*/
	    ia = (int) (0.75*(psbb->ia_gcrush));
	    setiampt(-ia,&(psbb->gxcrushl),1);
	    setiampt(-ia,&(psbb->gxcrushr),1);
	    setiampt(-ia,&(psbb->gycrushl),1);
	    setiampt(-ia,&(psbb->gycrushr),1);
	    setiampt(-ia,&(psbb->gzcrushl),1);
	    setiampt(-ia,&(psbb->gzcrushr),1);

    }

    return;
};
/* end @rsp rfov_funcs.e ********************/
